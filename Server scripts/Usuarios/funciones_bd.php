<?php
 
class funciones_BD {
 
    private $db;
 
    // constructor
    function __construct() {
        require_once 'connectbd.php';
        // connecting to database
        $this->db = new DB_Connect();
        $this->db->connect();
    }
 
    // destructor
    function __destruct() {
     }
 
    //Añadir nuevo usuario en la BBDD
    public function storeUser($uname, $password) {
        $uuid = uniqid('', true);
        $hash = $this->hashSSHA($password);
        $encrypted_password = $hash["encrypted"]; // encrypted password
        $salt = $hash["salt"]; // salt
        $result = mysql_query("INSERT INTO users(unique_id, username, encrypted_password, salt, created_at) VALUES('$uuid','$uname', '$encrypted_password', '$salt', NOW())");
        // check for successful store
        if ($result) {// get user details 
            $uid = mysql_insert_id(); // last inserted id
            $result = mysql_query("SELECT * FROM users WHERE id = $uid");
            // return user details
            return mysql_fetch_array($result);
        } else {
            return false;
        }
    }

    //Verificar si el usuario ya existe por el username
    public function isuserexist($username) {
        $result = mysql_query("SELECT username from users WHERE username = '$username'");
        $num_rows = mysql_num_rows($result); //numero de filas retornadas
        if ($num_rows > 0) {// el usuario existe 
            return true;
        } else { // no existe
            return false;
        }
    }
 
    public function getUserByEmailAndPassword($username, $password) {
        $result = mysql_query("SELECT * FROM users WHERE username = '$username'") or die(mysql_error());
        // check for result 
        $no_of_rows = mysql_num_rows($result);
        if ($no_of_rows > 0) {
            $result = mysql_fetch_array($result);
            $salt = $result['salt'];
            $encrypted_password = $result['encrypted_password'];
            $hash = $this->checkhashSSHA($salt, $password);
            // check for password equality
            if ($encrypted_password == $hash) { // user authentication details are correct
                return $result;
            }
        } else {// user not found
            return false;
        }
    }
   
     public function hashSSHA($password) {
        $salt = sha1(rand());
        $salt = substr($salt, 0, 10);
        $encrypted = base64_encode(sha1($password . $salt, true) . $salt);
        $hash = array("salt" => $salt, "encrypted" => $encrypted);
        return $hash;
    }

    //encrypting password
    //returns hash string
    public function checkhashSSHA($salt, $password) {
        $hash = base64_encode(sha1($password . $salt, true) . $salt);
        return $hash;
    }
}	
 
?>
