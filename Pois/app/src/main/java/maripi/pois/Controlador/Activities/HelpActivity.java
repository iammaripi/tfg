package maripi.pois.Controlador.Activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import maripi.pois.R;

public class HelpActivity extends AppCompatActivity {

    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.help);

        //Añadir toolbar
        toolbar = (Toolbar) findViewById(R.id.tool);
        setSupportActionBar(toolbar);

    }
}
