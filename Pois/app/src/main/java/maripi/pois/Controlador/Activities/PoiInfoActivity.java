package maripi.pois.Controlador.Activities;

import android.Manifest;
import android.app.ActionBar;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;

import maripi.pois.Modelo.Conexion;
import maripi.pois.Modelo.GestorPois;
import maripi.pois.Modelo.ParserJson;
import maripi.pois.Modelo.Poi;
import maripi.pois.R;

public class PoiInfoActivity extends AppCompatActivity {

    public static final String TAG = "PoiInfoActivity";
    private static final String API_KEY = "&key=AIzaSyAb963lQCvQL7gbZXPRQjBEYL7QAnrNsbs";
    private static final String url = "https://maps.googleapis.com/maps/api/place/details/json?";
    private static final String urlFoto = "https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference=";

    private ProgressBar pb;
    private Poi lugar;
    private Location ubicacion;
    private static String idUser;
    String sufijoURL;
    private GestorPois gestorPois;
    private String respuesta;
    private ImageButton btnAniadir;
    private ImageButton btnLlamar;
    private ImageButton btnWeb;
    private ImageButton btnRuta;
    private String username="";
    boolean aniadido=false;

    //Atributos para el Navigation Drawer
    private android.support.v4.widget.DrawerLayout DrawerLayout;
    private Toolbar toolbar;
    private NavigationView NavigationDrawer;
    private ActionBarDrawerToggle drawerToggle;
    //Fin Atributos para el Navigation Drawer

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.place_info);

        //Añadir toolbar
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        btnAniadir= (ImageButton) findViewById(R.id.btnAgregarPOI);
        btnLlamar= (ImageButton) findViewById(R.id.btnLlamar);
        btnRuta= (ImageButton) findViewById(R.id.btnComoLlegar);
        btnWeb= (ImageButton) findViewById(R.id.btnWeb);

        Bundle bundle=getIntent().getExtras();
        idUser= bundle.getString("idUser");
        username= bundle.getString("username");
        Log.i("Log idUser", "PoiInfoActivity " + idUser);

        if(bundle.getString("actividadOrigen").equals("PoiListActivity")){
            setButtonTint(btnAniadir, ColorStateList.valueOf(0xfff44336));
            aniadido=true;
        }
        else{
            setButtonTint(btnAniadir, ColorStateList.valueOf(0xff99cc00));
            aniadido=false;
        }

        pb = (ProgressBar) findViewById(R.id.pbPlaceInfo);
        pb.setVisibility(View.INVISIBLE);

        //Inicializar gestor para acceder
        gestorPois = new GestorPois();

        getDetallesLugar();

        //Inicializar el Navigation Drawer
        DrawerLayout = (android.support.v4.widget.DrawerLayout) findViewById(R.id.DrawerLayout);
        drawerToggle = setupDrawerToggle();

        DrawerLayout.setDrawerListener(drawerToggle);

        DrawerLayout = (DrawerLayout)findViewById(R.id.DrawerLayout);
        NavigationDrawer = (NavigationView)findViewById(R.id.nvView);

        View navView =  NavigationDrawer.getHeaderView(0);
        TextView nav_user = (TextView)navView.findViewById(R.id.name);
        nav_user.setText(username);

        NavigationDrawer.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        switch (menuItem.getItemId()) {
                            case R.id.nav_profile:
                                Intent intentPerfil = new Intent(PoiInfoActivity.this, UserProfileActivity.class);
                                intentPerfil.putExtra("username", username);
                                intentPerfil.putExtra("idUser", idUser);
                                startActivity(intentPerfil);
                                break;
                            case R.id.nav_busqueda:
                                Intent intentBusqueda = new Intent(PoiInfoActivity.this, SelectSearchActivity.class);
                                intentBusqueda.putExtra("idUser", idUser);
                                intentBusqueda.putExtra("username", username);
                                startActivity(intentBusqueda);
                                break;
                            case R.id.nav_listapois:
                                Intent intentListaPois = new Intent(PoiInfoActivity.this, PoiListActivity.class);
                                intentListaPois.putExtra("idUser", idUser);
                                intentListaPois.putExtra("username", username);
                                startActivity(intentListaPois);
                                break;
                            case R.id.nav_nuevo:
                                Intent intentNuevoPoi = new Intent(PoiInfoActivity.this, SelectNewPoiActivity.class);
                                intentNuevoPoi.putExtra("idUser", idUser);
                                intentNuevoPoi.putExtra("username", username);
                                intentNuevoPoi.putExtra("actividadOrigen","PoiInfoActivity");
                                startActivity(intentNuevoPoi);
                                break;
                            case R.id.nav_off:
                                cerrarSesion();
                                break;
                            case R.id.nav_help:
                                Intent intentAyuda = new Intent(PoiInfoActivity.this, HelpActivity.class);
                                intentAyuda.putExtra("idUser", idUser);
                                intentAyuda.putExtra("username", username);
                                startActivity(intentAyuda);
                                break;
                            default:
                                Log.i("NavigationView", "Ninguna opción pulsada");
                        }
                        menuItem.setChecked(true);
                        DrawerLayout.closeDrawers();
                        return true;
                    }
                });
        //Fin Navigation Drawer
    }//Fin onCreate

    private void comprobarBotones(ImageButton btnLlamar, ImageButton btnRuta, ImageButton btnWeb) {
        if (!lugar.getUrlWeb().equals("Sin web")) {
            setButtonTint(btnWeb, ColorStateList.valueOf(0xff99cc00));
        } else {
            setButtonTint(btnWeb, ColorStateList.valueOf(0xfff44336));
        }
        if (!lugar.getPhone().equals("Sin teléfono")) {
            setButtonTint(btnLlamar, ColorStateList.valueOf(0xff99cc00));
        } else {
            setButtonTint(btnLlamar, ColorStateList.valueOf(0xfff44336));
        }
        if (hayConexion()) {
            getLocation();
            if (ubicacion == null) {
                setButtonTint(btnRuta, ColorStateList.valueOf(0xfff44336));
            } else {
                setButtonTint(btnRuta, ColorStateList.valueOf(0xff99cc00));
            }
        } else {
            setButtonTint(btnRuta, ColorStateList.valueOf(0xfff44336));
        }
    }

    private void getDetallesLugar() {
        String contentSource = getIntent().getStringExtra("placeSource");
        if (contentSource.equals("onlineContent")) {
            if (hayConexion()) {
                String placeID = "placeid=" + getIntent().getStringExtra("placeID");
                String urlpeticion = url + placeID + API_KEY;
                Log.i(TAG, "URL = " + urlpeticion);
                getBusqueda busqueda = new getBusqueda();
                busqueda.execute(urlpeticion);
            } else {
                Toast.makeText(this, "No hay conexión a internet", Toast.LENGTH_LONG).show();
            }
        } else {
            Bundle extras = getIntent().getExtras();
            lugar = new Poi();
            lugar.setUsername(extras.getString("placeUsername"));
            lugar.setPlaceId(extras.getString("placeID"));
            lugar.setName(extras.getString("placeName"));
            lugar.setAddress(extras.getString("placeAddress"));
            lugar.setLatitud(extras.getDouble("placeLatitud"));
            lugar.setLongitud(extras.getDouble("placeLongitud"));
            lugar.setType(extras.getString("placeType"));
            lugar.setPhone(extras.getString("placePhone"));
            lugar.setDescription(extras.getString("placeDescription"));
            lugar.setUrlWeb(extras.getString("placeWeb"));
            mostrarDetalles();
            comprobarBotones(btnLlamar, btnRuta, btnWeb);
        }
    }

    //Método para crear el menú
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_map, menu);
        return true;
    }

    //Método para lanzar los eventos de las opciones del menú
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.search:
                Intent intentBusquedaSitio = new Intent(this, SelectSearchActivity.class);
                intentBusquedaSitio.putExtra("username", username);
                startActivity(intentBusquedaSitio);
                return true;
            case R.id.info:
                AlertDialog alertDialog = new AlertDialog.Builder(this).create();
                alertDialog.setTitle("Información");
                alertDialog.setMessage("Esta aplicación es el resultado del TFG Aplicación móvil " +
                        "de localización de POIs basada en preferencias de usuario y tecnologías " +
                        "de interacción por contacto. Desarrollada por María del Pilar Moreno Duque");
                alertDialog.setButton(DialogInterface.BUTTON_POSITIVE,"OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {}});
                alertDialog.setIcon(R.drawable.ic_info);
                alertDialog.show();
                return true;
            case R.id.salir:
                if (hayConexion()) {
                    Intent intentLogout = new Intent(this, LoginActivity.class);
                    startActivity(intentLogout);
                    finish();
                } else {
                    Toast.makeText(this, "No hay conexión a internet", Toast.LENGTH_LONG).show();
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }//Fin onOptionsItemSelected

    //Método para comprobar los permisos. Obligatorio del API 23 en adelante
    private boolean checkPermission() {
        int gpsPermission = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
        if (gpsPermission == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            String[] permission = {Manifest.permission.ACCESS_FINE_LOCATION};
            ActivityCompat.requestPermissions(this, permission, 0);
            return false;
        }
    }

    //Clase para realizar la conexión de la petición de búsqueda al servicio web
    private class getBusqueda extends AsyncTask<String, String, Poi> {
        @Override
        protected void onPreExecute() {
            pb.setVisibility(View.VISIBLE);
        }

        @Override
        protected Poi doInBackground(String... params) {
            String respuesta = null;
            Conexion conexion = new Conexion();
            try {
                respuesta = conexion.retrieveData(params[0]);
                if (respuesta != null) {
                    Poi lugar = new ParserJson().parserLugar(respuesta);
                    if (lugar != null && lugar.getPhotoReferences() != null) {
                        Log.i(TAG, lugar.getPhotoReferences().toString());
                        lugar = getFotos(lugar);
                    }
                    return lugar;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        //Método para recuperar las fotos de un sitio
        private Poi getFotos(Poi poi) {
            try {
                poi.setPhotos(new ArrayList<Bitmap>());
                for (String reference : poi.getPhotoReferences()) {
                    String urlImagen = urlFoto + reference + API_KEY;
                    Log.i(TAG, urlImagen);
                    InputStream inputStream = (InputStream) new URL(urlImagen).getContent();
                    Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
                    poi.getPhotos().add(bitmap);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return poi;
        }

        @Override
        protected void onPostExecute(Poi poi) {
            if (poi != null) {
                lugar = poi;
                mostrarDetalles();
                comprobarBotones(btnLlamar, btnRuta, btnWeb);
            } else {
                Log.i(TAG, "El sitio no existe");
            }
            pb.setVisibility(View.INVISIBLE);
        }
    }//Fin clase getBusqueda

    //Método para mostrar los detalles de un sitio
    private void mostrarDetalles() {
        TextView abierto = (TextView) findViewById(R.id.txtAbierto);
        TextView nombreLugar = (TextView) findViewById(R.id.txtNombreLugar);
        TextView direccionLugar = (TextView) findViewById(R.id.txtDireccionLugar);
        TextView tipoLugar = (TextView) findViewById(R.id.txtTipoLugar);
        TextView telefonoLugar = (TextView) findViewById(R.id.txtTelefonoLugar);
        TextView descripcionLugar = (TextView) findViewById(R.id.txtDescripcionLugar);
        TextView horario = (TextView) findViewById(R.id.txtHorario);
        TextView reviews = (TextView) findViewById(R.id.txtValoraciones);

        if (lugar.getOpening() != null) {
            if (lugar.getOpening().equalsIgnoreCase("true")) {
                abierto.setText(Html.fromHtml("<b>Abierto ahora:</b> &nbsp; Sí"));
            } else {
                abierto.setText(Html.fromHtml("<b>Abierto ahora:</b> &nbsp; No"));
            }
        }

        nombreLugar.setText(Html.fromHtml("<b>Nombre:</b> &nbsp; " + lugar.getName()));
        direccionLugar.setText(Html.fromHtml("<b>Dirección:</b> &nbsp; " + lugar.getAddress()));
        tipoLugar.setText(Html.fromHtml("<b>Tipo:</b>  &nbsp;" + lugar.getType()));
        telefonoLugar.setText(Html.fromHtml("<b>Teléfono:</b> &nbsp;" + lugar.getPhone()));

        if (lugar.getPhotos() != null) {
            int height = getAverageSize(lugar);
            Log.i(TAG, lugar.getPhotos().size() + "");
            LinearLayout imagenes = (LinearLayout) findViewById(R.id.lstFotos);
            for (Bitmap image : lugar.getPhotos()) {
                ImageView imageView = new ImageView(this);
                imageView.setImageBitmap(image);
                imageView.setLayoutParams(new ActionBar.LayoutParams(height + 250, height));
                imagenes.addView(imageView);
            }
        }
        if (!lugar.getDescription().equals("Sin descripción") && !lugar.getDescription().equals("Sin descripcion")) {
            descripcionLugar.setText(Html.fromHtml("<b>Descripción:</b> &nbsp; " + lugar.getDescription()));
        } else {
            descripcionLugar.setHeight(0);
            descripcionLugar.setWidth(0);
            descripcionLugar.setPadding(0, 0, 0, 0);
        }
        if (lugar.getOpeningHours() != null) {
            horario.setText(Html.fromHtml("<b>Horario de apertura:</b> <br/> <br/>" + lugar.getOpeningHours()));
        }
        if (lugar.getReviews() != null) {
            reviews.setText(Html.fromHtml("<b>Valoraciones:</b> <br/> <br/>" + lugar.getReviews()));
        }

        String urlMapita= "http://maps.googleapis.com/maps/api/staticmap?center=" + lugar.getLatitud() + "," + lugar.getLongitud() + "&zoom=17&scale=false&size=600x300&maptype=roadmap&format=png&visual_refresh=true&markers=size:mid%7Ccolor:0xff0000%7Clabel:1%7C" + lugar.getLatitud() + "," + lugar.getLongitud();
        new DownloadImageTask((ImageView) findViewById(R.id.imageMap)).execute(urlMapita);
    }

    private int getAverageSize(Poi poi) {
        int totalHeight = 0;
        for (Bitmap image : poi.getPhotos()) {
            totalHeight += image.getHeight();
        }
        return (int) totalHeight / poi.getPhotos().size();
    }

    //Método del evento de click del botón de web
    public void btnWebOnClick(View view) {
        if (!lugar.getUrlWeb().equals("Sin web")) {
            Uri URI = Uri.parse(lugar.getUrlWeb());
            Intent intent = new Intent(Intent.ACTION_VIEW, URI);
            startActivity(intent);
        } else {
            Toast.makeText(this, "Sin web", Toast.LENGTH_LONG).show();
        }
    }

    //Método del evento de click del botón para llamar
    public void btnLlamarOnClick(View v) {
        if (!lugar.getPhone().equals("Sin teléfono")) {
            Intent callIntent = new Intent(Intent.ACTION_DIAL);
            callIntent.setData(Uri.parse("tel:" + lugar.getPhone()));
            startActivity(callIntent);
        } else {
            Toast.makeText(this, "Sin teléfono", Toast.LENGTH_LONG).show();
        }
    }

    //Método del evento de click del botón para mostrar la ruta en google maps
    public void btnComoLlegarOnClick(View view) {
        if (hayConexion()) {
            getLocation();
            if (ubicacion == null) {
                Toast.makeText(this, "No es posible ubicarte", Toast.LENGTH_LONG).show();
            } else {
                String origen = "saddr=" + ubicacion.getLatitude() + "," + ubicacion.getLongitude();
                String destino = "&daddr=" + lugar.getAddress();
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://maps.google.com/maps?" + origen + destino));
                intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
                startActivity(intent);
            }
        } else {
            Toast.makeText(this, "No hay conexión a internet", Toast.LENGTH_LONG).show();
        }
    }

    //Método del evento de click del botón para agregar el sitio como POI
    public void btnAgregarPOIOnClick(View view) {
        if (hayConexion()) {
            lugar.setUsername(idUser);
            sufijoURL = "?placeid="+ lugar.getPlaceId()+"&username="+lugar.getUsername()+"&name="+lugar.getName()+"&address="+lugar.getAddress()+"&latitud="+lugar.getLatitud()+"&longitud="+lugar.getLongitud()+"&type="+lugar.getType()+"&description="+lugar.getDescription()+"&phone="+lugar.getPhone()+"&web="+lugar.getUrlWeb()+"&icon="+lugar.getUrlIcon()+"&scope="+lugar.getScope();
            new setPoi().execute();
        } else {
            Toast.makeText(this, "No hay conexión a internet", Toast.LENGTH_LONG).show();
        }
    }

    //Clase para realizar la conexión de la petición de crear un POI al servicio web
    private class setPoi extends AsyncTask<Integer, Integer, Integer> {
        private boolean creado=false;
        @Override
        protected Integer doInBackground(Integer... arg0) {
            respuesta = gestorPois.setPoi(sufijoURL);
            Log.e("status", "respuesta= " + respuesta);
            if (respuesta!=null && respuesta.contains("ok")) {
                creado=true;
            }else{
                creado=false;
            }
            return null;
        }

        @Override
        protected void onPostExecute(Integer result) {
            if(creado){
                Toast.makeText(getBaseContext(), "POI creado", Toast.LENGTH_LONG).show();
            }else{
                if (respuesta.contains("Duplicate")) {
                    Toast.makeText(getBaseContext(), "El POI ya estaba en tu lista", Toast.LENGTH_LONG).show();
                }
                else{
                    Toast.makeText(getBaseContext(), "Error al crear POI", Toast.LENGTH_LONG).show();
                }
            }
        }
    }//Fin clase setPoi

    //Método para obtener la localización del dispositivo
    private void getLocation() {
        if(checkPermission()) {
            LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            final LocationListener locationListener = new LocationListener() {
                public void onLocationChanged(Location location) {
                    ubicacion = location;
                }
                @Override
                public void onStatusChanged(String provider, int status, Bundle extras) {
                    Log.i(TAG, "location status changed");
                }
                @Override
                public void onProviderEnabled(String provider) {
                    Log.i(TAG, "location provider enabled");
                }
                @Override
                public void onProviderDisabled(String provider) {
                    Log.i(TAG, "location provider disabled");
                }
            };
            if (locationManager.getAllProviders().contains(LocationManager.GPS_PROVIDER)
                    && locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 10, locationListener);
            }
            ubicacion = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        }
    }

    //Método para comprobar si hay conexión a internet
    public boolean hayConexion() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getBaseContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnectedOrConnecting()) {
            return true;
        } else {
            return false;
        }
    }

    public static void setButtonTint(ImageButton button, ColorStateList tint) {
        if (Build.VERSION.SDK_INT == Build.VERSION_CODES.LOLLIPOP && button instanceof AppCompatImageButton) {
            ((AppCompatImageButton) button).setSupportBackgroundTintList(tint);
        } else {
            ViewCompat.setBackgroundTintList(button, tint);
        }
    }

    //Clase para rellenar la imagen del mapa al crear un POI manualmente o por NFC
    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }

    public void cerrarSesion(){
        finish();
        Intent intentLogin = new Intent(PoiInfoActivity.this, LoginActivity.class);
        intentLogin.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intentLogin);
        Toast.makeText(PoiInfoActivity.this, "Te has desconectado", Toast.LENGTH_LONG).show();
    }

    //Navigation Drawer
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        drawerToggle.syncState();
    }

    //Navigation Drawer
    private ActionBarDrawerToggle setupDrawerToggle() {
        return new ActionBarDrawerToggle(this, DrawerLayout, toolbar, R.string.drawer_open,  R.string.drawer_close);
    }

    //Navigation Drawer
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        drawerToggle.onConfigurationChanged(newConfig);
    }
}
