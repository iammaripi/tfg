package maripi.pois.Controlador.Activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.List;

import maripi.pois.Controlador.Adapters.PoiAdapter;
import maripi.pois.Modelo.Conexion;
import maripi.pois.Modelo.ParserJson;
import maripi.pois.Modelo.Poi;
import maripi.pois.R;

public class SearchPoiActivity extends AppCompatActivity {
    public static final String TAG = "SearchPoiActivity";
    private static final String API_KEY = "&key=AIzaSyAb963lQCvQL7gbZXPRQjBEYL7QAnrNsbs";
    private static final String url = "https://maps.googleapis.com/maps/api/place/textsearch/json?";

    private static List<Poi> listaLugares;
    private ProgressBar pb;
    private static String idUser;
    private String username="";
    ListView listView;
    ArrayAdapter<String> adaptador;
    TextView txtSinResultados;

    //Atributos para el Navigation Drawer
    private android.support.v4.widget.DrawerLayout DrawerLayout;
    private Toolbar toolbar;
    private NavigationView NavigationDrawer;
    private ActionBarDrawerToggle drawerToggle;
    //Fin Atributos para el Navigation Drawer

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_poi);

        Bundle bundle=getIntent().getExtras();
        idUser= bundle.getString("idUser");
        username= bundle.getString("username");
        Log.i("Log idUser", "SearchPOIActivity " + idUser);

        //Añadir toolbar
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        pb = (ProgressBar) findViewById(R.id.pbSearchPlace);
        pb.setVisibility(View.INVISIBLE);

        final String[] listavacia = new String[]{""};
        adaptador = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, listavacia);
        adaptador.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        listView = (ListView) findViewById(R.id.lstSearchPlace);
        txtSinResultados= (TextView) findViewById(R.id.lblSinResultados);


        //Inicializar el Navigation Drawer
        DrawerLayout = (android.support.v4.widget.DrawerLayout) findViewById(R.id.DrawerLayout);
        drawerToggle = setupDrawerToggle();

        DrawerLayout.setDrawerListener(drawerToggle);

        DrawerLayout = (DrawerLayout)findViewById(R.id.DrawerLayout);
        NavigationDrawer = (NavigationView)findViewById(R.id.nvView);

        View navView =  NavigationDrawer.getHeaderView(0);
        TextView nav_user = (TextView)navView.findViewById(R.id.name);
        nav_user.setText(username);

        NavigationDrawer.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        switch (menuItem.getItemId()) {
                            case R.id.nav_profile:
                                Intent intentPerfil = new Intent(SearchPoiActivity.this, UserProfileActivity.class);
                                intentPerfil.putExtra("username", username);
                                intentPerfil.putExtra("idUser", idUser);
                                startActivity(intentPerfil);
                                break;
                            case R.id.nav_busqueda:
                                Intent intentBusqueda = new Intent(SearchPoiActivity.this, SelectSearchActivity.class);
                                intentBusqueda.putExtra("idUser", idUser);
                                intentBusqueda.putExtra("username", username);
                                startActivity(intentBusqueda);
                                break;
                            case R.id.nav_listapois:
                                Intent intentListaPois = new Intent(SearchPoiActivity.this, PoiListActivity.class);
                                intentListaPois.putExtra("idUser", idUser);
                                intentListaPois.putExtra("username", username);
                                startActivity(intentListaPois);
                                break;
                            case R.id.nav_nuevo:
                                Intent intentNuevoPoi = new Intent(SearchPoiActivity.this, SelectNewPoiActivity.class);
                                intentNuevoPoi.putExtra("idUser", idUser);
                                intentNuevoPoi.putExtra("username", username);
                                startActivity(intentNuevoPoi);
                                break;
                            case R.id.nav_off:
                                cerrarSesion();
                                break;
                            case R.id.nav_help:
                                Intent intentAyuda = new Intent(SearchPoiActivity.this, HelpActivity.class);
                                intentAyuda.putExtra("idUser", idUser);
                                intentAyuda.putExtra("username", username);
                                startActivity(intentAyuda);
                                break;
                            default:
                                Log.i("NavigationView", "Ninguna opción pulsada");
                        }
                        menuItem.setChecked(true);
                        DrawerLayout.closeDrawers();
                        return true;
                    }
                });
        //Fin Navigation Drawer
    }//Fin onCreate

    //Método para volver a listar los lugares de la búsqueda
    protected void onResume() {
        super.onResume();
        if (listaLugares != null) {
            mostrarLista();
        }
    }

    //Método para crear el menú
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search_poi, menu);
        return true;
    }

    //Método para lanzar los eventos de las opciones del menú
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.searchSuggested:
                Intent intentBusquedaSugeridos = new Intent(this, SearchSuggestedActivity.class);
                intentBusquedaSugeridos.putExtra("idUser", idUser);
                intentBusquedaSugeridos.putExtra("username", username);
                startActivity(intentBusquedaSugeridos);
                return true;
            case R.id.home:
                Intent intentHome = new Intent(SearchPoiActivity.this, SelectionActivity.class);
                intentHome.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intentHome.putExtra("idUser", idUser);
                intentHome.putExtra("username", username);
                intentHome.putExtra("actividadOrigen","SearchPoiActivity");
                startActivity(intentHome);
                return true;
            case R.id.info:
                AlertDialog alertDialog = new AlertDialog.Builder(this).create();
                alertDialog.setTitle("Información");
                alertDialog.setMessage("Esta aplicación es el resultado del TFG Aplicación móvil " +
                        "de localización de POIs basada en preferencias de usuario y tecnologías " +
                        "de interacción por contacto. Desarrollada por María del Pilar Moreno Duque");
                alertDialog.setButton(DialogInterface.BUTTON_POSITIVE,"OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {}});
                alertDialog.setIcon(R.drawable.ic_info);
                alertDialog.show();
                return true;
            case R.id.salir:
                if (hayConexion()) {
                    Intent logout = new Intent(this, LoginActivity.class);
                    startActivity(logout);
                    finish();
                } else {
                    Toast.makeText(this, "No hay conexión a internet", Toast.LENGTH_LONG).show();
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }//Fin onOptionsItemSelected


    //Método del evento de click del botón de búsqueda
    public void btnSearchPlaceOnClick(View view) {
        //Se esconde el teclado
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        // Se recoge el término a buscar y se envía la petición
        if (hayConexion()) {
            EditText txtSearchPlace = (EditText) findViewById(R.id.txtSearchPlace);
            if (txtSearchPlace.getText().toString().equals("")) {
                Toast.makeText(this, "Por favor, escriba un lugar o una dirección", Toast.LENGTH_LONG).show();
            } else {
                String peticion = "query=" + txtSearchPlace.getText().toString().replace(" ", "%20");
                String urlPeticion = url + peticion +  API_KEY;
                getBusqueda busqueda = new getBusqueda();
                busqueda.execute(urlPeticion);
            }
        } else {
            Toast.makeText(this, "No hay conexión a internet", Toast.LENGTH_LONG).show();
        }
    }

    //Clase para realizar la conexión de la petición de búsqueda al servicio web
    private class getBusqueda extends AsyncTask<String, String, List<Poi>> {
        @Override
        protected void onPreExecute() {
            pb.setVisibility(View.VISIBLE);
        }

        @Override
        protected List<Poi> doInBackground(String... params) {
            String respuesta = null;
            Conexion conexion = new Conexion();
            try {
                respuesta = conexion.retrieveData(params[0]);
                if (respuesta != null) {
                    List<Poi> lugares = new ParserJson().parserBusquedaPoi(respuesta);
                    return lugares;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(List<Poi> pois) {
            if (pois != null) {
                txtSinResultados.setVisibility(View.INVISIBLE);
                listaLugares = pois;
                mostrarLista();
            } else {
                txtSinResultados.setVisibility(View.VISIBLE);
                listView.setAdapter(adaptador);

                Toast.makeText(getApplicationContext(), "La búsqueda no ha arrojado resultados", Toast.LENGTH_LONG).show();
                Log.i(TAG, "No hay lugares");
            }
            pb.setVisibility(View.INVISIBLE);
        }
    }//Fin clase getBusqueda


    //Método para rellenar la lista con los lugares que han resultado de la búsqueda
    private void mostrarLista() {
        ListAdapter lstLugaresAdapter = new PoiAdapter(this, listaLugares);
        listView.setAdapter(lstLugaresAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Poi poi = (Poi) parent.getItemAtPosition(position);
                Intent intent = new Intent(SearchPoiActivity.this, PoiInfoActivity.class);
                intent.putExtra("placeID", poi.getPlaceId());
                intent.putExtra("placeSource", poi.getScope());
                intent.putExtra("idUser", idUser);
                intent.putExtra("username", username);
                intent.putExtra("actividadOrigen","SearchPoiActivity");
                startActivity(intent);
            }
        });
    }

    //Método para comprobar si hay conexión a internet
    public boolean hayConexion() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getBaseContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnectedOrConnecting()) {
            return true;
        } else {
            return false;
        }
    }

    public void cerrarSesion(){
        finish();
        Intent intentLogin = new Intent(SearchPoiActivity.this, LoginActivity.class);
        intentLogin.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intentLogin);
        Toast.makeText(SearchPoiActivity.this, "Te has desconectado", Toast.LENGTH_LONG).show();
    }

    //Navigation Drawer
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        drawerToggle.syncState();
    }

    //Navigation Drawer
    private ActionBarDrawerToggle setupDrawerToggle() {
        return new ActionBarDrawerToggle(this, DrawerLayout, toolbar, R.string.drawer_open,  R.string.drawer_close);
    }

    //Navigation Drawer
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        drawerToggle.onConfigurationChanged(newConfig);
    }
}
