package maripi.pois.Controlador.Activities;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import maripi.pois.R;

public class IntroActivity extends Activity {
    public static final String TAG = "IntroActivity";
    protected int splashTime = 1000;
    protected Handler exitHandler = null;
    protected Runnable exitRunnable = null;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(TAG, "Cargando Intro");
        setContentView(R.layout.intro);

        try {
            Intent intent = getIntent();
            Bundle extras = intent.getExtras();
            String action = intent.getAction();

            if (Intent.ACTION_SEND.equals(action)) {
                if (extras.containsKey(Intent.EXTRA_STREAM)) {
                    Uri uri = extras.getParcelable(Intent.EXTRA_STREAM);
                    //ApplicationState.setIntentShareUri(uri);
                } else if (extras.containsKey(Intent.EXTRA_TEXT)) {
                    //Do nothing for now;
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        exitRunnable = new Runnable() {
            public void run() {
                exitSplash();
            }
        };

        exitHandler = new Handler();
        exitHandler.postDelayed(exitRunnable, splashTime);

    }

    private void exitSplash()
    {
        finish();
        Log.i(TAG, "Terminando Intro y cargando Login");
        Intent intent = new Intent();
        intent.setClass(IntroActivity.this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
    }

}
