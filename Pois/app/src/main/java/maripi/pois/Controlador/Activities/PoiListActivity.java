package maripi.pois.Controlador.Activities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;

import maripi.pois.Controlador.Adapters.PoiAdapter;
import maripi.pois.Modelo.GestorPois;
import maripi.pois.Modelo.Poi;
import maripi.pois.R;

public class PoiListActivity extends AppCompatActivity {
    private static final String TAG = "PoisListActivity";
    private ListView lstPois;
    private ArrayList<Poi> pois = new ArrayList<>();
    private PoiAdapter adaptadorListaPois;
    private ProgressDialog progressDialog;
    private GestorPois gestorPois;
    private ArrayList<NameValuePair> pares;
    private String respuesta;
    private static String idUser;
    private int poiSeleccionado;
    String sufijoURL;
    private String username="";

    //Atributos para el Navigation Drawer
    private android.support.v4.widget.DrawerLayout DrawerLayout;
    private Toolbar toolbar;
    private NavigationView NavigationDrawer;
    private ActionBarDrawerToggle drawerToggle;
    //Fin Atributos para el Navigation Drawer

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.poislist);

        Toolbar toolbar = (Toolbar) findViewById(R.id.include);
        setSupportActionBar(toolbar);

        Bundle bundle=getIntent().getExtras();
        idUser= bundle.getString("idUser");
        username= bundle.getString("username");

        gestorPois = new GestorPois();

        lstPois = (ListView) findViewById(R.id.lstPois);
        registerForContextMenu(lstPois);

        adaptadorListaPois = new PoiAdapter(this, pois);
        lstPois.setAdapter(adaptadorListaPois);



        progressDialog = ProgressDialog.show(PoiListActivity.this, null, "Obteniendo POIs...", true, false);
        new getPoi().execute();

        lstPois.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View v, int posicion, long id) {
                Poi poi = (Poi) parent.getItemAtPosition(posicion);

                Intent intent = new Intent(PoiListActivity.this, PoiInfoActivity.class);
                intent.putExtra("placeID", poi.getPlaceId());
                intent.putExtra("placeSource", poi.getScope());
                intent.putExtra("idUser", idUser);
                intent.putExtra("username", username);
                intent.putExtra("actividadOrigen","PoiListActivity");

                if (poi.getScope().equals("userContent")) {
                    intent.putExtra("placeUsername", poi.getUsername());
                    intent.putExtra("placeName", poi.getName());
                    intent.putExtra("placeAddress", poi.getAddress());
                    intent.putExtra("placeLatitud", poi.getLatitud());
                    intent.putExtra("placeLongitud", poi.getLongitud());
                    intent.putExtra("placeType", poi.getType());
                    intent.putExtra("placeDescription", poi.getDescription());
                    intent.putExtra("placePhone", poi.getPhone());
                    intent.putExtra("placeWeb", poi.getUrlWeb());
                }
                startActivity(intent);
            }
        });

        //Inicializar el Navigation Drawer
        DrawerLayout = (android.support.v4.widget.DrawerLayout) findViewById(R.id.DrawerLayout);
        drawerToggle = setupDrawerToggle();

        DrawerLayout.setDrawerListener(drawerToggle);

        DrawerLayout = (DrawerLayout)findViewById(R.id.DrawerLayout);
        NavigationDrawer = (NavigationView)findViewById(R.id.nvView);

        View navView =  NavigationDrawer.getHeaderView(0);
        TextView nav_user = (TextView)navView.findViewById(R.id.name);
        nav_user.setText(username);

        NavigationDrawer.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        switch (menuItem.getItemId()) {
                            case R.id.nav_profile:
                                Intent intentPerfil = new Intent(PoiListActivity.this, UserProfileActivity.class);
                                intentPerfil.putExtra("username", username);
                                intentPerfil.putExtra("idUser", idUser);
                                startActivity(intentPerfil);
                                break;
                            case R.id.nav_busqueda:
                                Intent intentBusqueda = new Intent(PoiListActivity.this, SelectSearchActivity.class);
                                intentBusqueda.putExtra("idUser", idUser);
                                intentBusqueda.putExtra("username", username);
                                startActivity(intentBusqueda);
                                break;
                            case R.id.nav_listapois:
                                Intent intentListaPois = new Intent(PoiListActivity.this, PoiListActivity.class);
                                intentListaPois.putExtra("idUser", idUser);
                                intentListaPois.putExtra("username", username);
                                startActivity(intentListaPois);
                                break;
                            case R.id.nav_nuevo:
                                Intent intentNuevoPoi = new Intent(PoiListActivity.this, SelectNewPoiActivity.class);
                                intentNuevoPoi.putExtra("idUser", idUser);
                                intentNuevoPoi.putExtra("username", username);
                                startActivity(intentNuevoPoi);
                                break;
                            case R.id.nav_off:
                                cerrarSesion();
                                break;
                            case R.id.nav_help:
                                Intent intentAyuda = new Intent(PoiListActivity.this, HelpActivity.class);
                                intentAyuda.putExtra("idUser", idUser);
                                intentAyuda.putExtra("username", username);
                                startActivity(intentAyuda);
                                break;
                            default:
                                Log.i("NavigationView", "Ninguna opción pulsada");
                        }
                        menuItem.setChecked(true);
                        DrawerLayout.closeDrawers();
                        return true;
                    }
                });
        //Fin Navigation Drawer
    }

    //Método para crear el menú
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search_cercanos, menu);
        return true;
    }

    //Método para lanzar los eventos de las opciones del menú
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.search:
                Intent intentBusqueda = new Intent(this, SelectSearchActivity.class);
                intentBusqueda.putExtra("idUser", idUser);
                startActivity(intentBusqueda);
                return true;
            case R.id.home:
                Intent intentHome = new Intent(PoiListActivity.this, SelectionActivity.class);
                intentHome.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intentHome.putExtra("idUser", idUser);
                intentHome.putExtra("username", username);
                intentHome.putExtra("actividadOrigen","PoiListActivity2");
                startActivity(intentHome);
                return true;
            case R.id.info:
                AlertDialog alertDialog = new AlertDialog.Builder(this).create();
                alertDialog.setTitle("Información");
                alertDialog.setMessage("Esta aplicación es el resultado del TFG Aplicación móvil " +
                        "de localización de POIs basada en preferencias de usuario y tecnologías " +
                        "de interacción por contacto. Desarrollada por María del Pilar Moreno Duque");
                alertDialog.setButton(DialogInterface.BUTTON_POSITIVE,"OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {}});
                alertDialog.setIcon(R.drawable.ic_info);
                alertDialog.show();
                return true;
            case R.id.salir:
                if (hayConexion()) {
                    Intent logout = new Intent(this, LoginActivity.class);
                    startActivity(logout);
                    finish();
                } else {
                    Toast.makeText(this, "No hay conexión a internet", Toast.LENGTH_LONG).show();
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }//Fin onOptionsItemSelected


    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_contextual, menu);

        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
        poiSeleccionado = info.position;

    }

    public boolean onContextItemSelected (MenuItem item) {
        switch (item.getItemId()) {
            case R.id.verDetalles:
                Poi poi = pois.get(poiSeleccionado);

                Intent intentInfo = new Intent(PoiListActivity.this, PoiInfoActivity.class);
                intentInfo.putExtra("placeID", poi.getPlaceId());
                intentInfo.putExtra("placeSource", poi.getScope());
                intentInfo.putExtra("idUser", idUser);
                intentInfo.putExtra("username", username);
                intentInfo.putExtra("actividadOrigen","PoiListActivity");

                if (poi.getScope().equals("userContent")) {
                    intentInfo.putExtra("placeUsername", poi.getUsername());
                    intentInfo.putExtra("placeName", poi.getName());
                    intentInfo.putExtra("placeAddress", poi.getAddress());
                    intentInfo.putExtra("placeLatitud", poi.getLatitud());
                    intentInfo.putExtra("placeLongitud", poi.getLongitud());
                    intentInfo.putExtra("placeType", poi.getType());
                    intentInfo.putExtra("placeDescription", poi.getDescription());
                    intentInfo.putExtra("placePhone", poi.getPhone());
                    intentInfo.putExtra("placeWeb", poi.getUrlWeb());
                }
                startActivity(intentInfo);
                break;
            case R.id.borrarPoi:
                pares = getId();
                progressDialog = ProgressDialog.show(PoiListActivity.this, null, "Borrando...", true, false);
                sufijoURL = "?"+ pares.get(0).toString();
                new deletePoi().execute();
                break;
        }
        return true;
    }

    protected ArrayList<NameValuePair> getId() {
        ArrayList<NameValuePair> id=new ArrayList<NameValuePair>(1);
        id.add(new BasicNameValuePair("id", Integer.toString(pois.get(poiSeleccionado).getId())));
        return id;
    }

    private class getPoi extends AsyncTask<Integer, Integer, Integer> {

        @Override
        protected Integer doInBackground(Integer... arg0) {
            pois = gestorPois.getPoi(idUser);
            return null;
        }

        @Override
        protected void onPostExecute(Integer result) {
            progressDialog.dismiss();
            if(pois != null ){
                adaptadorListaPois = new PoiAdapter(PoiListActivity.this, pois);
                lstPois.setAdapter(adaptadorListaPois);
                adaptadorListaPois.notifyDataSetChanged();
            }else{
                msg("No hay POIs");
            }
        }
    }

    private class deletePoi extends AsyncTask<Integer, Integer, Integer> {
        private boolean borrado=false;
        @Override
        protected Integer doInBackground(Integer... arg0) {
            respuesta = gestorPois.deletePoi(sufijoURL);
            if (respuesta!=null && respuesta.contains("ok")) {
                borrado=true;
            }else{
                borrado=false;
            }
            return null;
        }

        @Override
        protected void onPostExecute(Integer result) {
            progressDialog.dismiss();
            if(borrado){
                msg("POI borrado");
                progressDialog = ProgressDialog.show(PoiListActivity.this, null, "Obteniendo POIs...", true, false);
                new getPoi().execute();
            }else{
                msg("Error al borrar el POI");
            }

        }
    }

    public void msg(String msg){
        Toast.makeText(getBaseContext(), msg, Toast.LENGTH_SHORT).show();
    }


    public void newPoiOnClick(View view) {
        Intent intentMapa = new Intent(PoiListActivity.this, SelectNewPoiActivity.class);
        intentMapa.putExtra("idUser", idUser);
        intentMapa.putExtra("username", username);
        startActivity(intentMapa);
    }

    //Método para comprobar si hay conexión a internet
    public boolean hayConexion() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getBaseContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnectedOrConnecting()) {
            return true;
        } else {
            return false;
        }
    }

    public void cerrarSesion(){
        finish();
        Intent intentLogin = new Intent(PoiListActivity.this, LoginActivity.class);
        intentLogin.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intentLogin);
        Toast.makeText(PoiListActivity.this, "Te has desconectado", Toast.LENGTH_LONG).show();
    }

    //Navigation Drawer
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        drawerToggle.syncState();
    }

    //Navigation Drawer
    private ActionBarDrawerToggle setupDrawerToggle() {
        return new ActionBarDrawerToggle(this, DrawerLayout, toolbar, R.string.drawer_open,  R.string.drawer_close);
    }

    //Navigation Drawer
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        drawerToggle.onConfigurationChanged(newConfig);
    }
}
