package maripi.pois.Controlador.Activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.AddPlaceRequest;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

import java.io.InputStream;
import java.util.Collections;

import maripi.pois.Modelo.GestorPois;
import maripi.pois.Modelo.Poi;
import maripi.pois.R;

public class MapActivity extends AppCompatActivity implements PlaceSelectionListener, GoogleApiClient.OnConnectionFailedListener{
    public static final String TAG = "MapActivity";
    private static final int PLACE_PICKER_REQUEST = 1;
    private static final int REQUEST_SELECT_PLACE = 1000;
    private static String idUser;
    private LatLng latitudLongitud;
    private GoogleApiClient mGoogleApiClient;
    private GestorPois gestorPois;
    private String respuesta;
    String sufijoURL;
    protected static Intent sCurrentIntent;
    private Place place;
    private static final String urlIcono = "https://maps.gstatic.com/mapfiles/place_api/icons/geocode-71.png";
    private EditText txtNombre;
    private EditText txtTelefono;
    private Button btnAgregar;
    private TextView txtDireccion;
    private TextView txtTipo;
    private TextView txtDescripcion;
    private TextView txtWeb;
    private TextView txtKeywords;
    private TextView lblInstrucciones;
    private Poi poiNFC;
    private String actividadAnterior="";
    private String username="";


    //Atributos para el Navigation Drawer
    private DrawerLayout DrawerLayout;
    private Toolbar toolbar;
    private NavigationView NavigationDrawer;
    private ActionBarDrawerToggle drawerToggle;
    //Fin Atributos para el Navigation Drawer

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.map);

        Bundle bundle=getIntent().getExtras();
        idUser= bundle.getString("idUser");
        username= bundle.getString("username");
        actividadAnterior=bundle.getString("actividadOrigen");
        Log.i("Log idUser", "MapActivity " + idUser);

        //Inicializar gestor para acceder
        gestorPois = new GestorPois();

        //Añadir toolbar
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        //Inicializar el Navigation Drawer
        DrawerLayout = (DrawerLayout) findViewById(R.id.DrawerLayout);
        drawerToggle = setupDrawerToggle();

        DrawerLayout.setDrawerListener(drawerToggle);

        DrawerLayout = (DrawerLayout)findViewById(R.id.DrawerLayout);
        NavigationDrawer = (NavigationView)findViewById(R.id.nvView);

        View navView =  NavigationDrawer.getHeaderView(0);
        TextView nav_user = (TextView)navView.findViewById(R.id.name);
        nav_user.setText(username);

        NavigationDrawer.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        switch (menuItem.getItemId()) {
                            case R.id.nav_profile:
                                Intent intentPerfil = new Intent(MapActivity.this, UserProfileActivity.class);
                                intentPerfil.putExtra("idUser", idUser);
                                intentPerfil.putExtra("username", username);
                                startActivity(intentPerfil);
                                break;
                            case R.id.nav_busqueda:
                                Intent intentBusqueda = new Intent(MapActivity.this, SelectSearchActivity.class);
                                intentBusqueda.putExtra("idUser", idUser);
                                intentBusqueda.putExtra("username", username);
                                startActivity(intentBusqueda);
                                break;
                            case R.id.nav_listapois:
                                Intent intentListaPois = new Intent(MapActivity.this, PoiListActivity.class);
                                intentListaPois.putExtra("idUser", idUser);
                                intentListaPois.putExtra("username", username);
                                startActivity(intentListaPois);
                                break;
                            case R.id.nav_nuevo:
                                Intent intentNuevoPoi = new Intent(MapActivity.this, SelectNewPoiActivity.class);
                                intentNuevoPoi.putExtra("idUser", idUser);
                                intentNuevoPoi.putExtra("username", username);
                                startActivity(intentNuevoPoi);
                                break;
                            case R.id.nav_off:
                                cerrarSesion();
                                break;
                            case R.id.nav_help:
                                Intent intentAyuda = new Intent(MapActivity.this, HelpActivity.class);
                                intentAyuda.putExtra("idUser", idUser);
                                intentAyuda.putExtra("username", username);
                                startActivity(intentAyuda);
                                break;
                            default:
                                Log.i("NavigationView", "Ninguna opción pulsada");
                        }
                        menuItem.setChecked(true);
                        //setTitle(menuItem.getTitle());
                        DrawerLayout.closeDrawers();
                        return true;
                    }
                });
        //Fin Navigation Drawer


        //Crear Cliente de la API de Google
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                //.addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        try {
            int PLACE_PICKER_REQUEST = 1;
            PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
            startActivityForResult(builder.build(MapActivity.this), PLACE_PICKER_REQUEST);
        } catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }//Fin onCreate

    //NFC

    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    //CLASES ASÍNCRONAS EXTRAS

    //Clase para realizar la conexión de la petición de crear un POI al servicio web
    private class setPoi extends AsyncTask<Integer, Integer, Integer> {
        private boolean creado=false;
        @Override
        protected Integer doInBackground(Integer... arg0) {
            respuesta = gestorPois.setPoi(sufijoURL);
            if (respuesta!=null && respuesta.contains("ok")) {
                creado=true;
            }else{
                creado=false;
            }
            return null;
        }

        @Override
        protected void onPostExecute(Integer result) {
            if(creado){
                Toast.makeText(getBaseContext(), "POI creado", Toast.LENGTH_LONG).show();
            }else{
                if (respuesta.contains("Duplicate")) {
                    Toast.makeText(getBaseContext(), "El POI ya estaba en tu lista", Toast.LENGTH_LONG).show();
                }
                else{
                    Toast.makeText(getBaseContext(), "Error al crear POI", Toast.LENGTH_LONG).show();
                }
            }
        }
    }//Fin clase setPoi

    //Clase para rellenar la imagen del mapa al crear un POI manualmente o por NFC
    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }

    //MENUS
    //Método para crear el menú
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search_cercanos, menu);
        return true;
    }

    //Método para lanzar los eventos de las opciones del menú
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (drawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        switch (item.getItemId()) {
            case R.id.search:
                Intent intentBusqueda = new Intent(this, SelectSearchActivity.class);
                MapActivity.sCurrentIntent = intentBusqueda;
                intentBusqueda.putExtra("idUser", idUser);
                intentBusqueda.putExtra("username", username);
                startActivity(intentBusqueda);
                return true;
            case R.id.home:
                Intent intentHome = new Intent(MapActivity.this, SelectionActivity.class);
                intentHome.putExtra("idUser", idUser);
                intentHome.putExtra("username", username);
                intentHome.putExtra("actividadOrigen","MapActivity");
                startActivity(intentHome);
                return true;
            case R.id.info:
                AlertDialog alertDialog = new AlertDialog.Builder(this).create();
                alertDialog.setTitle("Información");
                alertDialog.setMessage("Esta aplicación es el resultado del TFG Aplicación móvil " +
                        "de localización de POIs basada en preferencias de usuario y tecnologías " +
                        "de interacción por contacto. Desarrollada por María del Pilar Moreno Duque");
                alertDialog.setButton(DialogInterface.BUTTON_POSITIVE,"OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {}});
                alertDialog.setIcon(R.drawable.ic_info);
                alertDialog.show();
                return true;
            case R.id.salir:
                if (hayConexion()) {
                    cerrarSesion();
                } else {
                    Toast.makeText(this, "No hay conexión a internet", Toast.LENGTH_LONG).show();
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    //Navigation Drawer
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        drawerToggle.syncState();
    }

    //Navigation Drawer
    private ActionBarDrawerToggle setupDrawerToggle() {
        return new ActionBarDrawerToggle(this, DrawerLayout, toolbar, R.string.drawer_open,  R.string.drawer_close);
    }

    //Navigation Drawer
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        drawerToggle.onConfigurationChanged(newConfig);
    }


    public void imageMapOnClick(View view) {
        if(actividadAnterior.equals("NewPoi")){
            try {
                int PLACE_PICKER_REQUEST = 1;
                PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
                builder.setLatLngBounds(getLatLngBounds(latitudLongitud));
                startActivityForResult(builder.build(MapActivity.this), PLACE_PICKER_REQUEST);
            } catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onPlaceSelected(Place place) {
        //Log
    }

    //Método que se llama al pulsar un lugar
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_PICKER_REQUEST && resultCode == Activity.RESULT_OK) {
            place = PlacePicker.getPlace(this,data);
            final CharSequence name = place.getName();
            final CharSequence address = place.getAddress();
            final CharSequence phone = place.getPhoneNumber();

            latitudLongitud=place.getLatLng();

            actividadAnterior="NewPoi";
            setContentView(R.layout.newpoi);

            //Añadir toolbar
            toolbar = (Toolbar) findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);

            //Elementos del layout
            txtNombre = (EditText) findViewById(R.id.txtNombre);
            txtDireccion = (TextView) findViewById(R.id.txtDireccion);
            txtTipo = (TextView) findViewById(R.id.txtTipo);
            txtTelefono = (EditText) findViewById(R.id.txtTelefono);
            txtDescripcion = (TextView) findViewById(R.id.txtDescrip);
            txtWeb = (TextView) findViewById(R.id.txtWeb);
            txtKeywords = (TextView) findViewById(R.id.txtKeywords);
            btnAgregar = (Button) findViewById(R.id.btnAgregarPOI);


            txtNombre.setText("Mi POI");
            txtDireccion.setText(address);
            txtTipo.setText(R.string.type);
            txtTelefono.setText(phone);
            if(place.getWebsiteUri() != null){
                final String web = place.getWebsiteUri().toString();
                txtWeb.setText(web);
            }

            String urlMapita= "http://maps.googleapis.com/maps/api/staticmap?center=" + latitudLongitud.latitude + "," + latitudLongitud.longitude + "&zoom=17&scale=false&size=600x300&maptype=roadmap&format=png&visual_refresh=true&markers=size:mid%7Ccolor:0xff0000%7Clabel:1%7C" + latitudLongitud.latitude + "," + latitudLongitud.longitude;
            new DownloadImageTask((ImageView) findViewById(R.id.imageMap)).execute(urlMapita);

            btnAgregar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (txtNombre.getText().length() > 0 && txtTelefono.getText().length() > 0) {
                        AddPlaceRequest requestPoi = new AddPlaceRequest(
                                txtNombre.getText().toString(), // Name
                                new LatLng(latitudLongitud.latitude, latitudLongitud.longitude), // Latitude and longitude
                                txtDireccion.getText().toString(), // Address
                                Collections.singletonList(Place.TYPE_POINT_OF_INTEREST), // Place types
                                txtTelefono.getText().toString() // Phone number
                        );

                        Places.GeoDataApi.addPlace(mGoogleApiClient, requestPoi)
                                .setResultCallback(new ResultCallback<PlaceBuffer>() {
                                    @Override
                                    public void onResult(PlaceBuffer places) {
                                        Log.i("Prueba definitiva", "Resultado de añadir lugar: " + places.getStatus().toString());
                                        Log.i("Prueba definitiva", "Lugar añadido: " + places.get(0).getName().toString());
                                        Log.i("Prueba definitiva", "Lugar añadido, ID: " + places.get(0).getId());
                                        guardarPOIenBBDD(places.get(0).getId(), places.get(0).getName().toString() ,
                                                places.get(0).getAddress().toString(),places.get(0).getLatLng().latitude,
                                                places.get(0).getLatLng().longitude, "TYPE_POINT_OF_INTEREST","Sin web" ,urlIcono,
                                                "Sin descripcion", places.get(0).getPhoneNumber().toString(), "false");
                                        places.release();
                                    }
                                });
                        try {
                            int PLACE_PICKER_REQUEST = 1;
                            PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
                            startActivityForResult(builder.build(MapActivity.this), PLACE_PICKER_REQUEST);
                            builder.setLatLngBounds(getLatLngBounds(latitudLongitud));
                        } catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException e) {
                            e.printStackTrace();
                        }
                    } else {
                        Toast.makeText(getBaseContext(), "Por favor, escriba tanto el nombre como el teléfono.", Toast.LENGTH_SHORT).show();
                    }
                }

            });

        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }


    @Override
    public void onError(Status status) {
        Log.e(TAG, "onError: Status = " + status.toString());
        Toast.makeText(this, "Poi selection failed: " + status.getStatusMessage(),
                Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    protected void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();
    }


    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.e(TAG, "La conexión con la API de Google Places falló con código de error: " +
                connectionResult.getErrorCode());
        Toast.makeText(this, "La conexión con la API de Google Places falló con código de error:" +
                connectionResult.getErrorCode(), Toast.LENGTH_LONG).show();
    }

    //UTIL
    public void guardarPOIenBBDD(String id, String name ,String address, Double latitud, Double longitud, String type, String web, String urlIcon, String description, String phone, String opening){
        if (hayConexion()) {
            Poi poiNuevo = new Poi(id, idUser, name, address, type, urlIcon, description, phone, opening);

            poiNuevo.setScope("userContent");
            Toast.makeText(this, "Nuevo POI guardado", Toast.LENGTH_LONG).show();

            poiNuevo.setUsername(idUser);
            poiNuevo.setLatitud(latitud);
            poiNuevo.setLongitud(longitud);
            poiNuevo.setUrlWeb(web);
            sufijoURL = "?placeid="+ poiNuevo.getPlaceId()+"&username="+poiNuevo.getUsername()
                    +"&name="+poiNuevo.getName()+"&address="+poiNuevo.getAddress()+"&latitud="+
                    poiNuevo.getLatitud()+"&longitud="+poiNuevo.getLongitud()+"&type="+poiNuevo.getType()
                    +"&description="+poiNuevo.getDescription()+"&phone="+poiNuevo.getPhone()
                    +"&web="+poiNuevo.getUrlWeb()+"&icon="+poiNuevo.getUrlIcon()+"&scope="+poiNuevo.getScope();
            Log.i("MAP", "sufijo: " + sufijoURL);
            new setPoi().execute();
        } else {
            Toast.makeText(this, "No hay conexión a internet", Toast.LENGTH_LONG).show();
        }
    }

    //Método para comprobar si hay conexión a internet
    public boolean hayConexion() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getBaseContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnectedOrConnecting()) {
            return true;
        } else {
            return false;
        }
    }

    public LatLngBounds getLatLngBounds(LatLng center) {
        LatLngBounds.Builder builder = LatLngBounds.builder();
        builder.include(center);
        return builder.build();
    }

    public void cerrarSesion(){
        finish();
        Intent intentLogin = new Intent(MapActivity.this, LoginActivity.class);
        intentLogin.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intentLogin);
        Toast.makeText(MapActivity.this, "Te has desconectado", Toast.LENGTH_LONG).show();
    }

    public void btnNewNFCOnClick(View view) {
        Intent intentNuevoPoi = new Intent(MapActivity.this, NFCActivity.class);
        intentNuevoPoi.putExtra("idUser", idUser);
        intentNuevoPoi.putExtra("username", username);
        startActivity(intentNuevoPoi);
    }

    public void btnNewMapaOnClick(View view) {
        Intent intentNuevoPoi = new Intent(MapActivity.this, MapActivity.class);
        intentNuevoPoi.putExtra("idUser", idUser);
        intentNuevoPoi.putExtra("username", username);
        startActivity(intentNuevoPoi);
    }

    //Método para cerrar sesión en caso de que la pantalla anterior fuese el login
    @Override
    public void onBackPressed() {
        Intent intent = new Intent(MapActivity.this, SelectionActivity.class);
        intent.putExtra("idUser", idUser);
        intent.putExtra("username", username);
        startActivity(intent);
    }

}