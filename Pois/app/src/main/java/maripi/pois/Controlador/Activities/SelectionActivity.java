package maripi.pois.Controlador.Activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import maripi.pois.Modelo.GestorUsuario;
import maripi.pois.Modelo.Preferences;
import maripi.pois.R;

public class SelectionActivity extends AppCompatActivity {
    private static String idUser;
    private String username="";
    private GestorUsuario gestorUsuario;
    private ArrayList<Preferences> preferencias = new ArrayList<>();
    TextView nav_user;

    //Atributos para el Navigation Drawer
    private DrawerLayout DrawerLayout;
    private Toolbar toolbar;
    private NavigationView NavigationDrawer;
    private ActionBarDrawerToggle drawerToggle;
    //Fin Atributos para el Navigation Drawer

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.selection);

        Bundle bundle=getIntent().getExtras();
        idUser= bundle.getString("idUser");
        username= bundle.getString("username");

        //Añadir toolbar
        toolbar = (Toolbar) findViewById(R.id.tool);
        setSupportActionBar(toolbar);


        gestorUsuario = new GestorUsuario();

        //Inicializar el Navigation Drawer
        DrawerLayout = (DrawerLayout) findViewById(R.id.DrawerLayout);
        drawerToggle = setupDrawerToggle();

        DrawerLayout.setDrawerListener(drawerToggle);

        DrawerLayout = (DrawerLayout)findViewById(R.id.DrawerLayout);
        NavigationDrawer = (NavigationView)findViewById(R.id.nvView);

        View navView =  NavigationDrawer.getHeaderView(0);

        new getPreferences().execute();
        nav_user = (TextView)navView.findViewById(R.id.name);

        NavigationDrawer.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        switch (menuItem.getItemId()) {
                            case R.id.nav_profile:
                                Intent intentPerfil = new Intent(SelectionActivity.this, UserProfileActivity.class);
                                intentPerfil.putExtra("idUser", idUser);
                                intentPerfil.putExtra("username", username);
                                startActivity(intentPerfil);
                                break;
                            case R.id.nav_busqueda:
                                Intent intentBusqueda = new Intent(SelectionActivity.this, SelectSearchActivity.class);
                                intentBusqueda.putExtra("idUser", idUser);
                                intentBusqueda.putExtra("username", username);
                                startActivity(intentBusqueda);
                                break;
                            case R.id.nav_listapois:
                                Intent intentListaPois = new Intent(SelectionActivity.this, PoiListActivity.class);
                                intentListaPois.putExtra("idUser", idUser);
                                intentListaPois.putExtra("username", username);
                                startActivity(intentListaPois);
                                break;
                            case R.id.nav_nuevo:
                                Intent intentNuevoPoi = new Intent(SelectionActivity.this, SelectNewPoiActivity.class);
                                intentNuevoPoi.putExtra("idUser", idUser);
                                intentNuevoPoi.putExtra("username", username);
                                startActivity(intentNuevoPoi);
                                break;
                            case R.id.nav_off:
                                cerrarSesion();
                                break;
                            case R.id.nav_help:
                                Intent intentAyuda = new Intent(SelectionActivity.this, HelpActivity.class);
                                intentAyuda.putExtra("idUser", idUser);
                                intentAyuda.putExtra("username", username);
                                startActivity(intentAyuda);
                                break;
                            default:
                                Log.i("NavigationView", "Ninguna opción pulsada");
                        }
                        menuItem.setChecked(true);
                        DrawerLayout.closeDrawers();
                        return true;
                    }
                });
        //Fin Navigation Drawer
    }

    //Método para cerrar sesión en caso de que la pantalla anterior fuese el login
    @Override
    public void onBackPressed() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(SelectionActivity.this);
        alertDialog.setTitle("Cerrar sesión");
        alertDialog.setMessage("¿Seguro que quieres cerrar sesión?");
        alertDialog.setIcon(R.drawable.ic_salir);

        alertDialog.setPositiveButton("Si", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                cerrarSesion();
            }
        });
        alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        alertDialog.show();
    }

    public void misPoisOnClick(View view) {
        Intent intentListaPois = new Intent(SelectionActivity.this, PoiListActivity.class);
        intentListaPois.putExtra("idUser", idUser);
        intentListaPois.putExtra("username", username);
        startActivity(intentListaPois);
    }

    public void buscarPoisOnClick(View view) {
        Intent intentBusqueda = new Intent(SelectionActivity.this, SelectSearchActivity.class);
        intentBusqueda.putExtra("idUser", idUser);
        intentBusqueda.putExtra("username", username);
        startActivity(intentBusqueda);
    }

    public void nuevoPoiOnClick(View view) {
        Intent intentNuevoPoi = new Intent(SelectionActivity.this, SelectNewPoiActivity.class);
        intentNuevoPoi.putExtra("idUser", idUser);
        intentNuevoPoi.putExtra("username", username);
        startActivity(intentNuevoPoi);
    }

    public void cerrarSesion(){
        finish();
        Intent intentLogin = new Intent(SelectionActivity.this, LoginActivity.class);
        intentLogin.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intentLogin);
        Toast.makeText(SelectionActivity.this, "Te has desconectado", Toast.LENGTH_LONG).show();
    }

    //Navigation Drawer
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        drawerToggle.syncState();
    }

    //Navigation Drawer
    private ActionBarDrawerToggle setupDrawerToggle() {
        return new ActionBarDrawerToggle(this, DrawerLayout, toolbar, R.string.drawer_open,  R.string.drawer_close);
    }

    //Navigation Drawer
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        drawerToggle.onConfigurationChanged(newConfig);
    }

    //Clase para realizar la conexión de la petición de las preferencias del usuario
    private class getPreferences extends AsyncTask<Integer, Integer, Integer> {
        @Override
        protected Integer doInBackground(Integer... arg0) {
            preferencias = gestorUsuario.getPreferences(idUser);
            return null;
        }

        @Override
        protected void onPostExecute(Integer result) {
            if(preferencias != null ){
                username=preferencias.get(0).getName();
                nav_user.setText(username);
            }else{
                Log.e("status", "No hay preferecias");
            }
        }
    }
}
