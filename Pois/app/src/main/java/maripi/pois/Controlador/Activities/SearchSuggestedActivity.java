package maripi.pois.Controlador.Activities;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import maripi.pois.Modelo.GestorUsuario;
import maripi.pois.Modelo.Poi;
import maripi.pois.Modelo.Conexion;
import maripi.pois.Modelo.ParserJson;
import maripi.pois.Controlador.Adapters.PoiAdapter;
import maripi.pois.Modelo.Preferences;
import maripi.pois.R;

public class SearchSuggestedActivity extends AppCompatActivity {

    public static final String TAG = "SearchSuggestedActivity";
    private static final String API_KEY = "&key=AIzaSyAb963lQCvQL7gbZXPRQjBEYL7QAnrNsbs";
    private static final String url = "https://maps.googleapis.com/maps/api/place/textsearch/json?";

    private static List<Poi> listaLugares;
    private ProgressBar pb;
    private Location ubicacion;
    private static String idUser;
    private String username="";
    private Spinner spKeywords;
    private String categoriaSeleccionada;
    private GestorUsuario gestorUsuario;
    private ArrayList<Preferences> preferencias = new ArrayList<>();
    private ArrayList<String> listaKeysAli = new ArrayList<>();
    private ArrayList<String> listaKeysAlo = new ArrayList<>();
    private ArrayList<String> listaKeysOtr = new ArrayList<>();
    TextView txtSinResultados;
    ListView listView;
    ArrayAdapter<String> adaptador;
    String open;
    Boolean open2;

    //Atributos para el Navigation Drawer
    private android.support.v4.widget.DrawerLayout DrawerLayout;
    private Toolbar toolbar;
    private NavigationView NavigationDrawer;
    private ActionBarDrawerToggle drawerToggle;
    //Fin Atributos para el Navigation Drawer

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_cercanos);

        //Añadir toolbar
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Bundle bundle=getIntent().getExtras();
        idUser= bundle.getString("idUser");
        username= bundle.getString("username");
        Log.i("Log idUser", "SearchSuggestedActivity " + idUser);

        checkPermission();
        pb = (ProgressBar) findViewById(R.id.pbSearchSuggested);
        pb.setVisibility(View.INVISIBLE);

        gestorUsuario = new GestorUsuario();

        new getOpen().execute();

        spKeywords = (Spinner)findViewById(R.id.spBusqueda);
        final String[] categorias = new String[]{"Ninguna categoría","Alimentación","Alojamiento","Otros"};
        ArrayAdapter<String> adaptador = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, categorias);
        adaptador.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spKeywords.setAdapter(adaptador);

        final String[] listavacia = new String[]{""};
        adaptador = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, listavacia);
        adaptador.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        listView = (ListView) findViewById(R.id.lstSearchSuggested);
        txtSinResultados= (TextView) findViewById(R.id.lblSinResultadosS);

        //Inicializar el Navigation Drawer
        DrawerLayout = (android.support.v4.widget.DrawerLayout) findViewById(R.id.DrawerLayout);
        drawerToggle = setupDrawerToggle();

        DrawerLayout.setDrawerListener(drawerToggle);

        DrawerLayout = (DrawerLayout)findViewById(R.id.DrawerLayout);
        NavigationDrawer = (NavigationView)findViewById(R.id.nvView);

        View navView =  NavigationDrawer.getHeaderView(0);
        TextView nav_user = (TextView)navView.findViewById(R.id.name);
        nav_user.setText(username);

        NavigationDrawer.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        switch (menuItem.getItemId()) {
                            case R.id.nav_profile:
                                Intent intentPerfil = new Intent(SearchSuggestedActivity.this, UserProfileActivity.class);
                                intentPerfil.putExtra("idUser", idUser);
                                intentPerfil.putExtra("username", username);
                                startActivity(intentPerfil);
                                break;
                            case R.id.nav_busqueda:
                                Intent intentBusqueda = new Intent(SearchSuggestedActivity.this, SelectSearchActivity.class);
                                intentBusqueda.putExtra("idUser", idUser);
                                intentBusqueda.putExtra("username", username);
                                startActivity(intentBusqueda);
                                break;
                            case R.id.nav_listapois:
                                Intent intentListaPois = new Intent(SearchSuggestedActivity.this, PoiListActivity.class);
                                intentListaPois.putExtra("idUser", idUser);
                                intentListaPois.putExtra("username", username);
                                startActivity(intentListaPois);
                                break;
                            case R.id.nav_nuevo:
                                Intent intentNuevoPoi = new Intent(SearchSuggestedActivity.this, SelectNewPoiActivity.class);
                                intentNuevoPoi.putExtra("idUser", idUser);
                                intentNuevoPoi.putExtra("username", username);
                                startActivity(intentNuevoPoi);
                                break;
                            case R.id.nav_off:
                                cerrarSesion();
                                break;
                            case R.id.nav_help:
                                Intent intentAyuda = new Intent(SearchSuggestedActivity.this, HelpActivity.class);
                                intentAyuda.putExtra("idUser", idUser);
                                intentAyuda.putExtra("username", username);
                                startActivity(intentAyuda);
                                break;
                            default:
                                Log.i("NavigationView", "Ninguna opción pulsada");
                        }
                        menuItem.setChecked(true);
                        DrawerLayout.closeDrawers();
                        return true;
                    }
                });
        //Fin Navigation Drawer

        spKeywords.setOnItemSelectedListener(
                new AdapterView.OnItemSelectedListener() {
                    public void onItemSelected(AdapterView<?> parent, android.view.View v, int position, long id) {
                        categoriaSeleccionada = spKeywords.getSelectedItem().toString();
                        new getKeysfromCategory().execute();
                    }

                    public void onNothingSelected(AdapterView<?> parent) {
                        Log.i("Busqueda", "Select: nada");
                    }
                });
    }//Fin onCreate

    //Clase para realizar la conexión de la petición de las palabras clave de una categoria
    private class getKeysfromCategory extends AsyncTask<Integer, Integer, Integer> {
        @Override
        protected Integer doInBackground(Integer... arg0) {
            preferencias = gestorUsuario.getPreferences(idUser);
            return null;
        }

        @Override
        protected void onPostExecute(Integer result) {
            if(preferencias != null ){
                rellenarKeys(preferencias, categoriaSeleccionada);
            }else{
                Log.e("status", "No hay keys");
            }
        }
    }//Fin clase getKeysfromCategory

    public void rellenarKeys(ArrayList<Preferences> preferencias, String categoriaSeleccionada){
        EditText txtPalabrasClave = (EditText) findViewById(R.id.txtSearhSuggested);
        if(categoriaSeleccionada.equals("Alimentación")){
            if(!preferencias.get(0).getAlimentacion().isEmpty()){
                String stringAli=" ";
                listaKeysAli=preferencias.get(0).getAlimentacion();
                for(int i=0; i<listaKeysAli.size();i++){
                     stringAli=stringAli+" " +listaKeysAli.get(i);
                }
                String st=txtPalabrasClave.getText().toString() + stringAli;
                txtPalabrasClave.setText(st);
                Toast.makeText(getApplicationContext(), "Palabras clave de Alimentación cargadas", Toast.LENGTH_LONG).show();
            }else{
                Toast.makeText(getApplicationContext(), "No tienes palabras clave almacenadas en esa categoría", Toast.LENGTH_LONG).show();
            }
        }else if(categoriaSeleccionada.equals("Alojamiento")){
            if(!preferencias.get(0).getAlojamiento().isEmpty()){
                String stringAlo=" ";
                listaKeysAlo=preferencias.get(0).getAlojamiento();
                for(int i=0; i<listaKeysAlo.size();i++){
                    stringAlo=stringAlo+" " +listaKeysAlo.get(i);
                }
                String st=txtPalabrasClave.getText().toString() + stringAlo;
                txtPalabrasClave.setText(st);
                Toast.makeText(getApplicationContext(), "Palabras clave de Alojamiento cargadas", Toast.LENGTH_LONG).show();
            }else{
                Toast.makeText(getApplicationContext(), "No tienes palabras clave almacenadas en esa categoría", Toast.LENGTH_LONG).show();
            }
        }else if(categoriaSeleccionada.equals("Otros")){
            if(!preferencias.get(0).getOtros().isEmpty()){
                String stringOtr=" ";
                listaKeysOtr=preferencias.get(0).getOtros();
                for(int i=0; i<listaKeysOtr.size();i++){
                    stringOtr=stringOtr+" " +listaKeysOtr.get(i);
                }
                String st=txtPalabrasClave.getText().toString() + stringOtr;
                txtPalabrasClave.setText(st);
                Toast.makeText(getApplicationContext(), "Palabras clave de Otros cargadas", Toast.LENGTH_LONG).show();
            }else{
                Toast.makeText(getApplicationContext(), "No tienes palabras clave almacenadas en esa categoría", Toast.LENGTH_LONG).show();
            }
        }else if(categoriaSeleccionada.equals("Ninguna categoría")){
            txtPalabrasClave.setText("");
        }
    }

    //Método para volver a listar los lugares de la búsqueda
    @Override
    protected void onResume() {
        super.onResume();
        if (listaLugares != null) {
            mostrarLista();
        }
    }

    //Método para crear el menú
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search_cercanos, menu);
        return true;
    }

    //Método para lanzar los eventos de las opciones del menú
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.search:
                Intent intentBusquedaSitio = new Intent(this, SearchPoiActivity.class);
                intentBusquedaSitio.putExtra("idUser", idUser);
                intentBusquedaSitio.putExtra("username", username);
                startActivity(intentBusquedaSitio);
                return true;
            case R.id.home:
                Intent intentHome = new Intent(SearchSuggestedActivity.this, SelectionActivity.class);
                intentHome.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intentHome.putExtra("idUser", idUser);
                intentHome.putExtra("username", username);
                intentHome.putExtra("actividadOrigen","SearchSuggestedActivity");
                startActivity(intentHome);
                return true;
            case R.id.info:
                AlertDialog alertDialog = new AlertDialog.Builder(this).create();
                alertDialog.setTitle("Información");
                alertDialog.setMessage("Esta aplicación es el resultado del TFG Aplicación móvil " +
                        "de localización de POIs basada en preferencias de usuario y tecnologías " +
                        "de interacción por contacto. Desarrollada por María del Pilar Moreno Duque");
                alertDialog.setButton(DialogInterface.BUTTON_POSITIVE,"OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {}});
                alertDialog.setIcon(R.drawable.ic_info);
                alertDialog.show();
                return true;
            case R.id.salir:
                if (hayConexion()) {
                    Intent logout = new Intent(this, LoginActivity.class);
                    startActivity(logout);
                    finish();
                } else {
                    Toast.makeText(this, "No hay conexión a internet", Toast.LENGTH_LONG).show();
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }//Fin onOptionsItemSelected



    //Método para comprobar los permisos. Obligatorio del API 23 en adelante
    private boolean checkPermission() {
        int gpsPermission = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
        if (gpsPermission == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            String[] permission = {Manifest.permission.ACCESS_FINE_LOCATION};
            ActivityCompat.requestPermissions(this, permission, 0);
            return false;
        }
    }

    //Método del evento de click del botón de búsqueda
    public void searchButtonOnClick(View view) {
        //Se esconde el teclado
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        // Se recoge la localización, el término a buscar y el radio; y se envía la petición
        if (hayConexion()) {
            getLocation();
            if (ubicacion != null) {
                EditText txtPalabrasClave = (EditText) findViewById(R.id.txtSearhSuggested);
                EditText txtRadio = (EditText) findViewById(R.id.txtSearchRadius);
                if (txtPalabrasClave.getText().toString().equals("") || txtRadio.getText().toString().equals("") || !txtRadio.getText().toString().matches("\\d+")) {
                    Toast.makeText(this, "Datos no válidos", Toast.LENGTH_LONG).show();
                } else {
                    String locationText = "&location=" + ubicacion.getLatitude() + "," + ubicacion.getLongitude();
                    int radioKm = (int) (Double.parseDouble(txtRadio.getText().toString()) * 1000);
                    String radio = "&radius=" + radioKm;
                    String palabrasClave = "&query=" + txtPalabrasClave.getText().toString().replace(" ", "+");
                    String opennow="";
                    String urlpeticion;
                    if(open2){
                        opennow= "&opennow=true";
                        urlpeticion = url + locationText + radio + opennow + palabrasClave + API_KEY;
                    }else{
                        urlpeticion = url + locationText + radio + palabrasClave + API_KEY;
                    }
                    Log.i(TAG, "URL = " + urlpeticion);
                    getBusqueda busqueda = new getBusqueda();
                    busqueda.execute(urlpeticion);
                }
            } else {
                Toast.makeText(this, "No es posible ubicarte", Toast.LENGTH_LONG).show();
            }
        } else {
            Toast.makeText(this, "No hay conexión a internet", Toast.LENGTH_LONG).show();
        }
    }

    //Método para obtener la localización del dispositivo
    private void getLocation() {
        if(checkPermission()) {
            LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            final LocationListener locationListener = new LocationListener() {
                public void onLocationChanged(Location location) {
                    ubicacion = location;
                }
                @Override
                public void onStatusChanged(String provider, int status, Bundle extras) {
                    Log.i(TAG, "location status changed");
                }
                @Override
                public void onProviderEnabled(String provider) {
                    Log.i(TAG, "location provider enabled");
                }
                @Override
                public void onProviderDisabled(String provider) {
                    Log.i(TAG, "location provider disabled");
                }
            };
            if (locationManager.getAllProviders().contains(LocationManager.NETWORK_PROVIDER)
                    && locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 5000, 10, locationListener);
            }
            ubicacion = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
        }
    }

    //Clase para realizar la conexión de la petición de búsqueda al servicio web
    private class getBusqueda extends AsyncTask<String, String, List<Poi>> {
        @Override
        protected void onPreExecute() {
            pb.setVisibility(View.VISIBLE);
        }

        @Override
        protected List<Poi> doInBackground(String... params) {
            String respuesta = null;
            Conexion conexion = new Conexion();
            try {
                respuesta = conexion.retrieveData(params[0]);
                if (respuesta != null) {
                    List<Poi> lugares = new ParserJson().parserBusquedaSugeridos(respuesta);
                    return lugares;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(List<Poi> pois) {
            if (pois != null) {
                txtSinResultados.setVisibility(View.INVISIBLE);
                listaLugares = pois;
                mostrarLista();
            } else {
                txtSinResultados.setVisibility(View.VISIBLE);
                listView.setAdapter(adaptador);

                Toast.makeText(getApplicationContext(), "La búsqueda no ha arrojado resultados", Toast.LENGTH_LONG).show();
                Log.i(TAG, "No hay lugares");
            }
            pb.setVisibility(View.INVISIBLE);
        }
    }//Fin clase getBusqueda

    //Método para rellenar la lista con los lugares que han resultado de la búsqueda
    private void mostrarLista() {
        ListAdapter lstLugaresAdapter = new PoiAdapter(this, listaLugares);
        listView.setAdapter(lstLugaresAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Poi poi = (Poi) parent.getItemAtPosition(position);
                Intent intent = new Intent(SearchSuggestedActivity.this, PoiInfoActivity.class);
                intent.putExtra("placeID", poi.getPlaceId());
                intent.putExtra("placeSource", poi.getScope());
                intent.putExtra("idUser", idUser);
                intent.putExtra("username", username);
                intent.putExtra("actividadOrigen","SearchSuggestedActivity");
                startActivity(intent);
            }
        });
    }

    //Clase para realizar la conexión de la petición de las preferencias del usuario
    private class getOpen extends AsyncTask<Integer, Integer, Integer> {
        @Override
        protected Integer doInBackground(Integer... arg0) {
            preferencias = gestorUsuario.getPreferences(idUser);
            return null;
        }

        @Override
        protected void onPostExecute(Integer result) {
            if (preferencias != null) {
                open= preferencias.get(0).getIsOpen();
                if(open.equalsIgnoreCase("True")){
                    open2=true;
                }else{
                    open2=false;
                }
            } else {
                Log.e("status", "No hay preferecias");
            }
        }
    }

    //Método para comprobar si hay conexión a internet
    public boolean hayConexion() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getBaseContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnectedOrConnecting()) {
            return true;
        } else {
            return false;
        }
    }

    public void cerrarSesion(){
        finish();
        Intent intentLogin = new Intent(SearchSuggestedActivity.this, LoginActivity.class);
        intentLogin.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intentLogin);
        Toast.makeText(SearchSuggestedActivity.this, "Te has desconectado", Toast.LENGTH_LONG).show();
    }

    //Navigation Drawer
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        drawerToggle.syncState();
    }

    //Navigation Drawer
    private ActionBarDrawerToggle setupDrawerToggle() {
        return new ActionBarDrawerToggle(this, DrawerLayout, toolbar, R.string.drawer_open,  R.string.drawer_close);
    }

    //Navigation Drawer
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        drawerToggle.onConfigurationChanged(newConfig);
    }
}
