package maripi.pois.Controlador.Activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import maripi.pois.Modelo.GestorUsuario;
import maripi.pois.Modelo.Preferences;
import maripi.pois.R;

public class UserProfileActivity extends AppCompatActivity {
    public static final String TAG = "UserProfileActivity";
    private static String idUser;
    private String username="";
    private EditText txtNombre;
    private EditText txtPalabrasClaveAlimentacion;
    private ListView lstKeysAli;
    private EditText txtPalabrasClaveAlojamiento;
    private ListView lstKeysAlo;
    private EditText txtPalabrasClaveOtros;
    private ListView lstKeysOtr;
    private ArrayList<String> listaKeysAli = new ArrayList<>();
    private ArrayList<String> listaKeysAlo = new ArrayList<>();
    private ArrayList<String> listaKeysOtr = new ArrayList<>();
    private Spinner spValoraciones;
    private ArrayList<Preferences> preferencias = new ArrayList<>();
    private GestorUsuario gestorUsuario;
    private Context context;
    private String respuesta;
    private String sufijoURL;
    private String sufijoURL2;
    private String sufijoURL3;
    private String isOpen="";
    private int valoracion;
    private CheckBox cbIsOpen;
    private int keySeleccionado;
    ArrayAdapter<String> adaptadorAli;
    ArrayAdapter<String> adaptadorAlo;
    ArrayAdapter<String> adaptadorOtr;


    //Atributos para el Navigation Drawer
    private android.support.v4.widget.DrawerLayout DrawerLayout;
    private Toolbar toolbar;
    private NavigationView NavigationDrawer;
    private ActionBarDrawerToggle drawerToggle;
    //Fin Atributos para el Navigation Drawer

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.preferences);

        Bundle bundle=getIntent().getExtras();
        idUser= bundle.getString("idUser");
        username= bundle.getString("username");
        Log.i("Log idUser", "UserProfileActivity " + idUser);

        gestorUsuario = new GestorUsuario();
        context=this;

        //Añadir toolbar
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //Elementos del layout
        txtNombre = (EditText) findViewById(R.id.txtNombre);
        cbIsOpen = (CheckBox)findViewById(R.id.cbOpen);

        txtPalabrasClaveAlimentacion = (EditText) findViewById(R.id.txtPalabrasClaveAlimentacion);
        lstKeysAli = (ListView) findViewById(R.id.lstPalabrasClave);
        registerForContextMenu(lstKeysAli);

        txtPalabrasClaveAlojamiento = (EditText) findViewById(R.id.txtPalabrasClaveAlojamiento);
        lstKeysAlo = (ListView) findViewById(R.id.lstPalabrasClaveAlojamiento);
        registerForContextMenu(lstKeysAlo);

        txtPalabrasClaveOtros = (EditText) findViewById(R.id.txtPalabrasClaveOtros);
        lstKeysOtr = (ListView) findViewById(R.id.lstPalabrasClaveOtros);
        registerForContextMenu(lstKeysOtr);

        spValoraciones = (Spinner)findViewById(R.id.spValoracion);
        final Integer[] valoraciones = new Integer[]{0,1,2,3,4};
        ArrayAdapter<Integer> adaptador = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, valoraciones);
        adaptador.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spValoraciones.setAdapter(adaptador);

        new getPreferences().execute();


        //Inicializar el Navigation Drawer
        DrawerLayout = (android.support.v4.widget.DrawerLayout) findViewById(R.id.DrawerLayout);
        drawerToggle = setupDrawerToggle();

        DrawerLayout.setDrawerListener(drawerToggle);

        DrawerLayout = (android.support.v4.widget.DrawerLayout)findViewById(R.id.DrawerLayout);
        NavigationDrawer = (NavigationView)findViewById(R.id.nvView);

        View navView =  NavigationDrawer.getHeaderView(0);
        TextView nav_user = (TextView)navView.findViewById(R.id.name);
        nav_user.setText(username);

        NavigationDrawer.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        switch (menuItem.getItemId()) {
                            case R.id.nav_profile:
                                Intent intentPerfil = new Intent(UserProfileActivity.this, UserProfileActivity.class);
                                intentPerfil.putExtra("username", username);
                                intentPerfil.putExtra("idUser", idUser);
                                startActivity(intentPerfil);
                                break;
                            case R.id.nav_busqueda:
                                Intent intentBusqueda = new Intent(UserProfileActivity.this, SelectSearchActivity.class);
                                intentBusqueda.putExtra("idUser", idUser);
                                intentBusqueda.putExtra("username", username);
                                startActivity(intentBusqueda);
                                break;
                            case R.id.nav_listapois:
                                Intent intentListaPois = new Intent(UserProfileActivity.this, PoiListActivity.class);
                                intentListaPois.putExtra("idUser", idUser);
                                intentListaPois.putExtra("username", username);
                                startActivity(intentListaPois);
                                break;
                            case R.id.nav_nuevo:
                                Intent intentNuevoPoi = new Intent(UserProfileActivity.this, SelectNewPoiActivity.class);
                                intentNuevoPoi.putExtra("idUser", idUser);
                                intentNuevoPoi.putExtra("username", username);
                                startActivity(intentNuevoPoi);
                                break;
                            case R.id.nav_off:
                                cerrarSesion();
                                break;
                            case R.id.nav_help:
                                Intent intentAyuda = new Intent(UserProfileActivity.this, HelpActivity.class);
                                intentAyuda.putExtra("idUser", idUser);
                                intentAyuda.putExtra("username", username);
                                startActivity(intentAyuda);
                                break;
                            default:
                                Log.i("NavigationView", "Ninguna opción pulsada");
                        }
                        menuItem.setChecked(true);
                        DrawerLayout.closeDrawers();
                        return true;
                    }
                });
        //Fin Navigation Drawer

        spValoraciones.setOnItemSelectedListener(
                new AdapterView.OnItemSelectedListener() {
                    public void onItemSelected(AdapterView<?> parent, android.view.View v, int position, long id) {
                        valoracion= (Integer) parent.getItemAtPosition(position);
                    }

                    public void onNothingSelected(AdapterView<?> parent) {
                        Log.i("Perfil", "Select: nada");
                    }
                });
    }

    public void cerrarSesion(){
        finish();
        Intent intentLogin = new Intent(UserProfileActivity.this, LoginActivity.class);
        intentLogin.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intentLogin);
        Toast.makeText(UserProfileActivity.this, "Te has desconectado", Toast.LENGTH_LONG).show();
    }

    //Navigation Drawer
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        drawerToggle.syncState();
    }

    //Navigation Drawer
    private ActionBarDrawerToggle setupDrawerToggle() {
        return new ActionBarDrawerToggle(this, DrawerLayout, toolbar, R.string.drawer_open,  R.string.drawer_close);
    }

    //Navigation Drawer
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        drawerToggle.onConfigurationChanged(newConfig);
    }


    public void btnGuardarPreferenciasOnClick(View view) {
        String sufijoAli="";
        String sufijoAlo="";
        String sufijoOtr="";
        for(int i=0; i<listaKeysAli.size();i++){
            if(i==0){
                sufijoAli=listaKeysAli.get(i);
            }else{
                sufijoAli=sufijoAli+"," +listaKeysAli.get(i);
            }
        }
        for(int i=0; i<listaKeysAlo.size();i++){
            if(i==0){
                sufijoAlo=listaKeysAlo.get(i);
            }else{
                sufijoAlo=sufijoAlo+"," +listaKeysAlo.get(i);
            }
        }
        for(int i=0; i<listaKeysOtr.size();i++){
            if(i==0){
                sufijoOtr=listaKeysOtr.get(i);
            }else{
                sufijoOtr=sufijoOtr+"," +listaKeysOtr.get(i);
            }
        }
        sufijoURL = "?idusername="+ idUser+"&nombre="+txtNombre.getText().toString()+"&isopen="+isOpen+"&valoracion="+valoracion;
        sufijoURL2="?idpreference="+preferencias.get(0).getId();
        sufijoURL3="?idpreference="+preferencias.get(0).getId()+"&keysAli=" + sufijoAli +"&keysAlo="+ sufijoAlo+"&keysOtr="+ sufijoOtr;
        Log.e("status", "excepcion= " + sufijoURL3);
        new updatePreferences().execute();
    }


    public void btnAddKeyOnClick(View view) {
        listaKeysAli.add(txtPalabrasClaveAlimentacion.getText().toString());
        adaptadorAli = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, listaKeysAli);
        lstKeysAli.setAdapter(adaptadorAli);
        txtPalabrasClaveAlimentacion.setText("");
    }

    public void btnAddKey3OnClick(View view) {
        listaKeysAlo.add(txtPalabrasClaveAlojamiento.getText().toString());
        adaptadorAlo = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, listaKeysAlo);
        lstKeysAlo.setAdapter(adaptadorAlo);
        txtPalabrasClaveAlojamiento.setText("");
    }

    public void btnAddKey4OnClick(View view) {
        listaKeysOtr.add(txtPalabrasClaveOtros.getText().toString());
        adaptadorOtr = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, listaKeysOtr);
        lstKeysOtr.setAdapter(adaptadorOtr);
        txtPalabrasClaveOtros.setText("");
    }


    public void cbOpenOnClick(View view) {
        if(cbIsOpen.isChecked()){
            isOpen="True";
        }else{
            isOpen="False";
        }
    }

    //Clase para realizar la conexión de la petición de las preferencias del usuario
    private class getPreferences extends AsyncTask<Integer, Integer, Integer> {
        @Override
        protected Integer doInBackground(Integer... arg0) {
            preferencias = gestorUsuario.getPreferences(idUser);
            return null;
        }

        @Override
        protected void onPostExecute(Integer result) {
            if(preferencias != null ){
                mostrarPreferencias(preferencias);
            }else{
                Log.e("status", "No hay preferecias");
            }
        }
    }

    //Clase para realizar la conexión de la petición de crear una nueva preferencia
    private class updatePreferences extends AsyncTask<Integer, Integer, Integer> {
        private boolean creado=false;
        @Override
        protected Integer doInBackground(Integer... arg0) {
            respuesta = gestorUsuario.updatePreferences(sufijoURL, sufijoURL2, sufijoURL3);
            if (respuesta!=null && respuesta.contains("ok")) {
                creado=true;
            }else{
                creado=false;
            }
            return null;
        }

        @Override
        protected void onPostExecute(Integer result) {
            if(creado){
                Toast.makeText(getBaseContext(), "Preferencias actualizadas", Toast.LENGTH_LONG).show();
                username=txtNombre.getText().toString();
                View navView =  NavigationDrawer.getHeaderView(0);
                TextView nav_user = (TextView)navView.findViewById(R.id.name);
                nav_user.setText(username);
            }else{
                Toast.makeText(getBaseContext(), "Error al actualizar las preferencias", Toast.LENGTH_LONG).show();
            }
        }
    }//Fin clase updatePreferences


    public void mostrarPreferencias(ArrayList<Preferences> preferencias){
        txtNombre.setText(preferencias.get(0).getName());
        if(preferencias.get(0).getIsOpen().equals("True")){
            isOpen="True";
            cbIsOpen.setChecked(true);
        }else{
            isOpen="False";
            cbIsOpen.setChecked(false);
        }
        spValoraciones.setSelection(preferencias.get(0).getValoracion());

        if(preferencias.get(0).getAlimentacion()!=null || preferencias.get(0).getAlojamiento()!=null || preferencias.get(0).getOtros()!=null){
            listaKeysAli=preferencias.get(0).getAlimentacion();
            listaKeysAlo=preferencias.get(0).getAlojamiento();
            listaKeysOtr=preferencias.get(0).getOtros();

            adaptadorAli = new ArrayAdapter<>(context, android.R.layout.simple_list_item_1, listaKeysAli);
            adaptadorAlo = new ArrayAdapter<>(context, android.R.layout.simple_list_item_1, listaKeysAlo);
            adaptadorOtr = new ArrayAdapter<>(context, android.R.layout.simple_list_item_1, listaKeysOtr);
            lstKeysAli.setAdapter(adaptadorAli);
            lstKeysAlo.setAdapter(adaptadorAlo);
            lstKeysOtr.setAdapter(adaptadorOtr);
        }
    }

    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        if(v.getId() == R.id.lstPalabrasClave){
            MenuInflater inflater = getMenuInflater();
            inflater.inflate(R.menu.menu_contextual_alimentacion, menu);

            AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
            keySeleccionado = info.position;
        }else if(v.getId() == R.id.lstPalabrasClaveAlojamiento){
            MenuInflater inflater = getMenuInflater();
            inflater.inflate(R.menu.menu_contextual_alojamiento, menu);

            AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
            keySeleccionado = info.position;
        }else if(v.getId() == R.id.lstPalabrasClaveOtros){
            MenuInflater inflater = getMenuInflater();
            inflater.inflate(R.menu.menu_contextual_otros, menu);

            AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
            keySeleccionado = info.position;
        }
    }

    public boolean onContextItemSelected (MenuItem item) {
        switch (item.getItemId()) {
            case R.id.borrarKeyAli:
                listaKeysAli.remove(keySeleccionado);
                adaptadorAli.notifyDataSetChanged();
                break;
            case R.id.borrarKeyAlo:
                listaKeysAlo.remove(keySeleccionado);
                adaptadorAlo.notifyDataSetChanged();
                break;
            case R.id.borrarKeyOtr:
                listaKeysOtr.remove(keySeleccionado);
                adaptadorOtr.notifyDataSetChanged();
                break;
        }
        return true;
    }

    //Método para crear el menú
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search_cercanos, menu);
        return true;
    }

    //Método para lanzar los eventos de las opciones del menú
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (drawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        switch (item.getItemId()) {
            case R.id.search:
                Intent intentBusqueda = new Intent(this, SelectSearchActivity.class);
                intentBusqueda.putExtra("idUser", idUser);
                intentBusqueda.putExtra("username", username);
                startActivity(intentBusqueda);
                return true;
            case R.id.home:
                Intent intentHome = new Intent(UserProfileActivity.this, SelectionActivity.class);
                intentHome.putExtra("idUser", idUser);
                intentHome.putExtra("username", username);
                intentHome.putExtra("actividadOrigen","UserProfileActivity");
                startActivity(intentHome);
                return true;
            case R.id.info:
                AlertDialog alertDialog = new AlertDialog.Builder(this).create();
                alertDialog.setTitle("Información");
                alertDialog.setMessage("Esta aplicación es el resultado del TFG - Aplicación móvil " +
                        "de localización de POIs basada en preferencias de usuario y tecnologías " +
                        "de interacción por contacto. Desarrollada por María del Pilar Moreno Duque");
                alertDialog.setButton(DialogInterface.BUTTON_POSITIVE,"OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {}});
                alertDialog.setIcon(R.drawable.ic_info);
                alertDialog.show();
                return true;
            case R.id.salir:
                if (hayConexion()) {
                    cerrarSesion();
                } else {
                    Toast.makeText(this, "No hay conexión a internet", Toast.LENGTH_LONG).show();
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    //Método para comprobar si hay conexión a internet
    public boolean hayConexion() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getBaseContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnectedOrConnecting()) {
            return true;
        } else {
            return false;
        }
    }
}
