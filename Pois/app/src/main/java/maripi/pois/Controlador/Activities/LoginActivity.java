package maripi.pois.Controlador.Activities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import maripi.pois.Modelo.GestorUsuario;
import maripi.pois.R;

public class LoginActivity extends AppCompatActivity {
    public static final String TAG = "LoginActivity";
    private EditText txtUser;
    private EditText txtPass;
    private CheckBox cbRecordar;
    private ProgressDialog progressDialog;
    private ArrayList<NameValuePair> datos;
    private GestorUsuario gestorUsuario;
    public String respuesta;
    private SharedPreferences.Editor preferenciasLogin;
    private static String idUser;
    private String username="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);

        //Añadir toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //Inicializar gestor para acceder
        gestorUsuario = new GestorUsuario();

        //Shared preferences para recordar los datos del login si el usuario lo desea
        SharedPreferences loginPreferences = getSharedPreferences("loginPrefs", MODE_PRIVATE);
        preferenciasLogin = loginPreferences.edit();

        //Elementos del layout
        txtUser = (EditText) findViewById(R.id.txtUsuario);
        txtPass = (EditText) findViewById(R.id.txtPass);
        Button btnAcceder = (Button) findViewById(R.id.btnLogin);
        cbRecordar = (CheckBox) findViewById(R.id.cbRecordarDatos);

        //Si el usuario desea recordar los datos, se guardan
        Boolean saveLogin = loginPreferences.getBoolean("saveLogin", false);
        if (saveLogin) {
            txtUser.setText(loginPreferences.getString("username", ""));
            txtPass.setText(loginPreferences.getString("password", ""));
            cbRecordar.setChecked(true);
        }

        //Evento del botón Acceder, si hay conexión y el usuario ha rellenado los datos se comprueba si son correctos
        btnAcceder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (hayConexion()) {
                    if (txtUser.getText().length() > 0 && txtPass.getText().length() > 0) {
                        datos = new ArrayList<>(2);
                        datos.add(new BasicNameValuePair("usuario", txtUser.getText().toString()));
                        datos.add(new BasicNameValuePair("password", txtPass.getText().toString()));
                        progressDialog = ProgressDialog.show(LoginActivity.this, null, "Comprobando datos...", true, false);
                        new checkLogin().execute();
                    } else {
                        Toast.makeText(getBaseContext(), "Por favor, escriba usuario y contraseña", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getBaseContext(), "No hay conexión a internet", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }//Fin onCreate

    public void lblRegistroOnClick(View view) {
        Intent intentRegistro = new Intent(LoginActivity.this, RegistroActivity.class);
        startActivity(intentRegistro);
    }

    //Clase para realizar la conexión de la petición de login al servicio web
    private class checkLogin extends AsyncTask<Integer, Integer, Integer> {
        @Override
        protected Integer doInBackground(Integer... arg0) {
            //Se comprueban los datos en segundo plano
            respuesta = gestorUsuario.check(datos);
            return null;
        }
        @Override
        protected void onPostExecute(Integer result) {
            progressDialog.dismiss();
            if (respuesta != null && respuesta.indexOf("success") == 4) {
                //Se esconde el teclado
                InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(txtUser.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                // Se recogen los términos
                String user = txtUser.getText().toString();
                String pass = txtPass.getText().toString();
                //Se guardan las preferencias si así lo desea el usuario
                if (cbRecordar.isChecked()) {
                    preferenciasLogin.putBoolean("saveLogin", true);
                    preferenciasLogin.putString("username", user);
                    preferenciasLogin.putString("password", pass);
                    preferenciasLogin.commit();
                } else {
                    preferenciasLogin.clear();
                    preferenciasLogin.commit();
                }
                setId(respuesta);
                //Se accede al resto de la aplicación
                Toast.makeText(getApplicationContext(), "Conectado: " + user, Toast.LENGTH_LONG).show();
                Intent intentLogin = new Intent(LoginActivity.this, SelectionActivity.class);
                intentLogin.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intentLogin.putExtra("idUser", idUser);
                intentLogin.putExtra("username", user);
                intentLogin.putExtra("actividadOrigen","LoginActivity");
                Log.i("Log idUser", "LoginActivity " + idUser);
                startActivity(intentLogin);
            } else {
                Toast.makeText(getBaseContext(), "Datos incorrectos", Toast.LENGTH_SHORT).show();
            }
        }
    }//Fin clase checkLogin

    //Método para guardar el Id del usuario
    public void setId(String respuesta) {
        try {
            JSONObject jObj = new JSONObject(respuesta);
            String user = jObj.optString("user");
            jObj = new JSONObject(user);
            idUser = jObj.optString("uid");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    //Método para crear el menú
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    //Método para lanzar los eventos de las opciones del menú
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.info:
                AlertDialog alertDialog = new AlertDialog.Builder(this).create();
                alertDialog.setTitle("Información");
                alertDialog.setMessage("Esta aplicación es el resultado del TFG Aplicación móvil " +
                        "de localización de POIs basada en preferencias de usuario y tecnologías " +
                        "de interacción por contacto. Desarrollada por María del Pilar Moreno Duque");
                alertDialog.setButton(DialogInterface.BUTTON_POSITIVE,"OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {}});
                alertDialog.setIcon(R.drawable.ic_info);
                alertDialog.show();
                return true;
            case R.id.ayuda:
                Intent intentAyuda = new Intent(LoginActivity.this, HelpActivity.class);
                intentAyuda.putExtra("idUser", idUser);
                intentAyuda.putExtra("username", username);
                startActivity(intentAyuda);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    //Método para comprobar si hay conexión a internet
    public boolean hayConexion() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getBaseContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnectedOrConnecting()) {
            return true;
        } else {
            return false;
        }
    }
}

