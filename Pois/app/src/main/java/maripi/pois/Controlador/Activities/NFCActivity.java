package maripi.pois.Controlador.Activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.Ndef;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.AddPlaceRequest;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.Collections;

import maripi.pois.Modelo.GestorPois;
import maripi.pois.Modelo.Poi;
import maripi.pois.R;

public class NFCActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener{
    public static final String TAG = "NFCActivity";
    private static String idUser;
    private LatLng latitudLongitud;
    private TextView txtviewTag;
    private TextView txtviewInfoTag;
    private NfcAdapter mNfcAdapter;
    public static final String MIME_TEXT_PLAIN = "text/plain";
    private GestorPois gestorPois;
    private String respuesta;
    String sufijoURL;
    private EditText txtNombre;
    private EditText txtTelefono;
    private Button btnAgregar;
    private TextView txtDireccion;
    private TextView txtTipo;
    private TextView txtDescripcion;
    private TextView txtWeb;
    private TextView txtKeywords;
    private TextView lblInstrucciones;
    private String txtTag="";
    private Poi poiNFC;
    private String username="";
    private Button btnNFC;
    private GoogleApiClient mGoogleApiClient;


    //Atributos para el Navigation Drawer
    private DrawerLayout DrawerLayout;
    private Toolbar toolbar;
    private NavigationView NavigationDrawer;
    private ActionBarDrawerToggle drawerToggle;
    //Fin Atributos para el Navigation Drawer

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.newpoinfc);

        Bundle bundle=getIntent().getExtras();
        idUser= bundle.getString("idUser");
        username= bundle.getString("username");

        //Elementos del layout
        btnNFC = (Button) findViewById(R.id.btnNewNFC);
        txtviewTag = (TextView) findViewById(R.id.txtviewTag);
        txtviewInfoTag = (TextView) findViewById(R.id.txtviewInfoTag);

        //Inicializar gestor para acceder
        gestorPois = new GestorPois();

        //Añadir toolbar
        toolbar = (Toolbar) findViewById(R.id.tool);
        setSupportActionBar(toolbar);

        //Crear Cliente de la API de Google
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                //.addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();


        //Inicializar el Navigation Drawer
        DrawerLayout = (DrawerLayout) findViewById(R.id.DrawerLayout);
        drawerToggle = setupDrawerToggle();

        DrawerLayout.setDrawerListener(drawerToggle);

        DrawerLayout = (DrawerLayout)findViewById(R.id.DrawerLayout);
        NavigationDrawer = (NavigationView)findViewById(R.id.nvView);

        View navView =  NavigationDrawer.getHeaderView(0);
        TextView nav_user = (TextView)navView.findViewById(R.id.name);
        nav_user.setText(username);

        NavigationDrawer.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        switch (menuItem.getItemId()) {
                            case R.id.nav_profile:
                                Intent intentPerfil = new Intent(NFCActivity.this, UserProfileActivity.class);
                                intentPerfil.putExtra("idUser", idUser);
                                intentPerfil.putExtra("username", username);
                                startActivity(intentPerfil);
                                break;
                            case R.id.nav_busqueda:
                                Intent intentBusqueda = new Intent(NFCActivity.this, SelectSearchActivity.class);
                                intentBusqueda.putExtra("idUser", idUser);
                                intentBusqueda.putExtra("username", username);
                                startActivity(intentBusqueda);
                                break;
                            case R.id.nav_listapois:
                                Intent intentListaPois = new Intent(NFCActivity.this, PoiListActivity.class);
                                intentListaPois.putExtra("idUser", idUser);
                                intentListaPois.putExtra("username", username);
                                startActivity(intentListaPois);
                                break;
                            case R.id.nav_nuevo:
                                Intent intentNuevoPoi = new Intent(NFCActivity.this, SelectNewPoiActivity.class);
                                intentNuevoPoi.putExtra("idUser", idUser);
                                intentNuevoPoi.putExtra("username", username);
                                startActivity(intentNuevoPoi);
                                break;
                            case R.id.nav_off:
                                cerrarSesion();
                                break;
                            case R.id.nav_help:
                                Intent intentAyuda = new Intent(NFCActivity.this, HelpActivity.class);
                                intentAyuda.putExtra("idUser", idUser);
                                intentAyuda.putExtra("username", username);
                                startActivity(intentAyuda);
                                break;
                            default:
                                Log.i("NavigationView", "Ninguna opción pulsada");
                        }
                        menuItem.setChecked(true);
                        //setTitle(menuItem.getTitle());
                        DrawerLayout.closeDrawers();
                        return true;
                    }
                });
        //Fin Navigation Drawer

        //Iniciar el lector NFC si el dispositivo lo tiene
        mNfcAdapter = NfcAdapter.getDefaultAdapter(this);
        if (mNfcAdapter == null) {
            Toast.makeText(getBaseContext(), "El dispositivo no soporta NFC", Toast.LENGTH_LONG).show();
            txtviewTag.setText(R.string.noNFC);
        }else{
            if (!mNfcAdapter.isEnabled()) {
                txtviewTag.setText(R.string.noNFC);
            } else {
                handleIntent(getIntent());
            }
        }


        //Evento del botón de creación de POIs mediante NFC
        btnNFC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mNfcAdapter == null) {
                    Toast.makeText(getBaseContext(), "El dispositivo no soporta NFC", Toast.LENGTH_LONG).show();
                }else{
                    setContentView(R.layout.newpoi);

                    //Elementos del layout
                    txtNombre = (EditText) findViewById(R.id.txtNombre);
                    txtDireccion = (TextView) findViewById(R.id.txtDireccion);
                    txtTipo = (TextView) findViewById(R.id.txtTipo);
                    txtTelefono = (EditText) findViewById(R.id.txtTelefono);
                    txtDescripcion = (TextView) findViewById(R.id.txtDescrip);
                    txtWeb = (TextView) findViewById(R.id.txtWeb);
                    txtKeywords = (TextView) findViewById(R.id.txtKeywords);
                    btnAgregar = (Button) findViewById(R.id.btnAgregarPOI);
                    lblInstrucciones = (TextView) findViewById(R.id.lblInstrucciones);


                    lblInstrucciones.setText(R.string.InstruccionesNewNFC);
                    txtNombre.setText(poiNFC.getName());
                    txtDireccion.setText(poiNFC.getAddress());
                    txtTipo.setText(poiNFC.getType());
                    txtTelefono.setText(poiNFC.getPhone());
                    txtWeb.setText(poiNFC.getUrlWeb());
                    txtDescripcion.setText(poiNFC.getDescription());
                    txtKeywords.setText(poiNFC.getKeywords());

                    latitudLongitud=new LatLng(poiNFC.getLatitud(), poiNFC.getLongitud());

                    String urlMapita= "http://maps.googleapis.com/maps/api/staticmap?center=" + poiNFC.getLatitud() + "," + poiNFC.getLongitud() + "&zoom=17&scale=false&size=600x300&maptype=roadmap&format=png&visual_refresh=true&markers=size:mid%7Ccolor:0xff0000%7Clabel:1%7C" + poiNFC.getLatitud() + "," + poiNFC.getLongitud();
                    new DownloadImageTask((ImageView) findViewById(R.id.imageMap)).execute(urlMapita);

                    btnAgregar.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (txtNombre.getText().length() > 0 && txtTelefono.getText().length() > 0) {
                                poiNFC.setName(txtNombre.getText().toString());
                                poiNFC.setPhone(txtTelefono.getText().toString());
                                AddPlaceRequest requestPoi = new AddPlaceRequest(
                                        poiNFC.getName(), // Name
                                        new LatLng(poiNFC.getLatitud(), poiNFC.getLongitud()), // Latitude and longitude
                                        poiNFC.getAddress(), // Address
                                        Collections.singletonList(Place.TYPE_POINT_OF_INTEREST), // Place types
                                        poiNFC.getPhone() // Phone number
                                );

                                Places.GeoDataApi.addPlace(mGoogleApiClient, requestPoi)
                                        .setResultCallback(new ResultCallback<PlaceBuffer>() {
                                            @Override
                                            public void onResult(PlaceBuffer places) {
                                                Log.i("Prueba definitiva", "Resultado de añadir lugar: " + places.getStatus().toString());
                                                Log.i("Prueba definitiva", "Lugar añadido: " + places.get(0).getName().toString());
                                                Log.i("Prueba definitiva", "Lugar añadido, ID: " + places.get(0).getId());
                                                guardarPOIenBBDD(places.get(0).getId(), places.get(0).getName().toString(),
                                                        places.get(0).getAddress().toString(),places.get(0).getLatLng().latitude,
                                                        places.get(0).getLatLng().longitude, poiNFC.getType(), poiNFC.getUrlWeb(), poiNFC.getUrlIcon(),
                                                        poiNFC.getDescription(), places.get(0).getPhoneNumber().toString(), "false");
                                                places.release();
                                            }
                                        });
                                try {
                                    int PLACE_PICKER_REQUEST = 1;
                                    PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
                                    startActivityForResult(builder.build(NFCActivity.this), PLACE_PICKER_REQUEST);
                                    builder.setLatLngBounds(getLatLngBounds(latitudLongitud));
                                } catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                Toast.makeText(getBaseContext(), "Por favor, escriba al menos el nombre y el teléfono.", Toast.LENGTH_SHORT).show();
                            }
                        }

                    });
                }
            }
        });

    }//Fin onCreate

    protected void onResume() {
        super.onResume();
        if (mNfcAdapter != null) {
            setupForegroundDispatch(this, mNfcAdapter);
        }
    }

    @Override
    protected void onPause() {
        if (mNfcAdapter != null) {
            stopForegroundDispatch(this, mNfcAdapter);
        }
        super.onPause();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        handleIntent(intent);
    }

    private void handleIntent(Intent intent) {
        String action = intent.getAction();
        if (NfcAdapter.ACTION_NDEF_DISCOVERED.equals(action)) {

            String type = intent.getType();
            if (MIME_TEXT_PLAIN.equals(type)) {

                Tag tag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
                new NdefReaderTask().execute(tag);

            } else {
                Log.d(TAG, "Wrong mime type: " + type);
            }
        } else if (NfcAdapter.ACTION_TECH_DISCOVERED.equals(action)) {
            Tag tag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
            String[] techList = tag.getTechList();
            String searchedTech = Ndef.class.getName();

            for (String tech : techList) {
                if (searchedTech.equals(tech)) {
                    new NdefReaderTask().execute(tag);
                    break;
                }
            }
        }
    }

    public static void setupForegroundDispatch(final Activity activity, NfcAdapter adapter) {
        final Intent intent = new Intent(activity.getApplicationContext(), activity.getClass());
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);

        final PendingIntent pendingIntent = PendingIntent.getActivity(activity.getApplicationContext(), 0, intent, 0);
        IntentFilter[] filters = new IntentFilter[1];
        String[][] techList = new String[][]{};

        //Comprobar que es el mismo filtro que el indicado en el manifest
        filters[0] = new IntentFilter();
        filters[0].addAction(NfcAdapter.ACTION_NDEF_DISCOVERED);
        filters[0].addCategory(Intent.CATEGORY_DEFAULT);
        try {
            filters[0].addDataType(MIME_TEXT_PLAIN);
        } catch (IntentFilter.MalformedMimeTypeException e) {
            throw new RuntimeException("Check your mime type.");
        }
        adapter.enableForegroundDispatch(activity, pendingIntent, filters, techList);
    }

    public static void stopForegroundDispatch(final Activity activity, NfcAdapter adapter) {
        adapter.disableForegroundDispatch(activity);
    }

    public void btnNewNFCTagOnClick(View view) {
        if (mNfcAdapter == null) {
            Toast.makeText(getBaseContext(), "El dispositivo no soporta NFC", Toast.LENGTH_LONG).show();
        }else {
            Intent intentTag = new Intent(NFCActivity.this, WriteNFCActivity.class);
            intentTag.putExtra("idUser", idUser);
            intentTag.putExtra("username", username);
            startActivity(intentTag);
        }
    }

    //CLASES ASÍNCRONAS NFC

    //Clase para la lectura de NFC
    private class NdefReaderTask extends AsyncTask<Tag, Void, String> {
        @Override
        protected String doInBackground(Tag... params) {
            Tag tag = params[0];
            Ndef ndef = Ndef.get(tag);
            if (ndef == null) { //El tag no soporta NDEF
                return null;
            }

            NdefMessage ndefMessage = ndef.getCachedNdefMessage();
            NdefRecord[] records = ndefMessage.getRecords();
            for (NdefRecord ndefRecord : records) {
                if (ndefRecord.getTnf() == NdefRecord.TNF_WELL_KNOWN && Arrays.equals(ndefRecord.getType(), NdefRecord.RTD_TEXT)) {
                    try {
                        return readText(ndefRecord);
                    } catch (UnsupportedEncodingException e) {
                        Log.e(TAG, "Unsupported Encoding", e);
                    }
                }
            }
            return null;
        }

        private String readText(NdefRecord record) throws UnsupportedEncodingException {
            byte[] payload = record.getPayload();
            String textEncoding = ((payload[0] & 128) == 0) ? "UTF-8" : "UTF-8"; // Text Encoding
            int languageCodeLength = payload[0] & 0063; //Language Code
            return new String(payload, languageCodeLength + 1, payload.length - languageCodeLength - 1, textEncoding);
        }

        @Override
        protected void onPostExecute(String tag) {
            if (tag != null) {
                txtviewInfoTag.setText(tag);
                txtTag = txtviewInfoTag.getText().toString();
                Log.i("NFC", "txtTag: " + txtTag);
                new getNFC().execute();
            }
            else{
                txtviewInfoTag.setText(R.string.NFCVacio);
            }
        }
    }//Fin clase NdefReaderTask

    //Clase para realizar la conexión de la petición de los datos de un POI por NFC
    private class getNFC extends AsyncTask<Integer, Integer, Integer> {
        @Override
        protected Integer doInBackground(Integer... arg0) {
            poiNFC = gestorPois.getNFC(txtTag, idUser);
            Log.i("NFC", "poiNFC: " + poiNFC);
            return null;
        }

        @Override
        protected void onPostExecute(Integer result) {
            if(poiNFC != null ){
                Log.i("NFC", "poiNFC onPost: " + poiNFC);
                btnNFC.setVisibility(View.VISIBLE);
            }else{
                Log.i("NFC", "No hay POI por NFC");
                btnNFC.setVisibility(View.INVISIBLE);
            }
        }
    }//Fin clase getNFC



    //CLASES ASÍNCRONAS EXTRAS

    //Clase para realizar la conexión de la petición de crear un POI al servicio web
    private class setPoi extends AsyncTask<Integer, Integer, Integer> {
        private boolean creado=false;
        @Override
        protected Integer doInBackground(Integer... arg0) {
            respuesta = gestorPois.setPoi(sufijoURL);
            if (respuesta!=null && respuesta.contains("ok")) {
                creado=true;
            }else{
                creado=false;
            }
            return null;
        }

        @Override
        protected void onPostExecute(Integer result) {
            if(creado){
                Toast.makeText(getBaseContext(), "POI creado", Toast.LENGTH_LONG).show();
            }else{
                if (respuesta.contains("Duplicate")) {
                    Toast.makeText(getBaseContext(), "El POI ya estaba en tu lista", Toast.LENGTH_LONG).show();
                }
                else{
                    Toast.makeText(getBaseContext(), "Error al crear POI", Toast.LENGTH_LONG).show();
                }
            }
        }
    }//Fin clase setPoi

    //Clase para rellenar la imagen del mapa al crear un POI manualmente o por NFC
    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }

    //MENUS
    //Método para crear el menú
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search_cercanos, menu);
        return true;
    }

    //Método para lanzar los eventos de las opciones del menú
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (drawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        switch (item.getItemId()) {
            case R.id.search:
                Intent intentBusqueda = new Intent(this, SelectSearchActivity.class);
                intentBusqueda.putExtra("idUser", idUser);
                intentBusqueda.putExtra("username", username);
                startActivity(intentBusqueda);
                return true;
            case R.id.home:
                Intent intentHome = new Intent(NFCActivity.this, SelectionActivity.class);
                intentHome.putExtra("idUser", idUser);
                intentHome.putExtra("username", username);
                startActivity(intentHome);
                return true;
            case R.id.info:
                AlertDialog alertDialog = new AlertDialog.Builder(this).create();
                alertDialog.setTitle("Información");
                alertDialog.setMessage("Esta aplicación es el resultado del TFG Aplicación móvil " +
                        "de localización de POIs basada en preferencias de usuario y tecnologías " +
                        "de interacción por contacto. Desarrollada por María del Pilar Moreno Duque");
                alertDialog.setButton(DialogInterface.BUTTON_POSITIVE,"OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {}});
                alertDialog.setIcon(R.drawable.ic_info);
                alertDialog.show();
                return true;
            case R.id.salir:
                if (hayConexion()) {
                    cerrarSesion();
                } else {
                    Toast.makeText(this, "No hay conexión a internet", Toast.LENGTH_LONG).show();
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    //Navigation Drawer
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        drawerToggle.syncState();
    }

    //Navigation Drawer
    private ActionBarDrawerToggle setupDrawerToggle() {
        return new ActionBarDrawerToggle(this, DrawerLayout, toolbar, R.string.drawer_open,  R.string.drawer_close);
    }

    //Navigation Drawer
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        drawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mGoogleApiClient.disconnect();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.e(TAG, "La conexión con la API de Google Places falló con código de error: " +
                connectionResult.getErrorCode());
        Toast.makeText(this, "La conexión con la API de Google Places falló con código de error:" +
                connectionResult.getErrorCode(), Toast.LENGTH_LONG).show();
    }

    //UTIL
    public void guardarPOIenBBDD(String id, String name ,String address, Double latitud, Double longitud, String type, String web, String urlIcon, String description, String phone, String opening){
        if (hayConexion()) {
            Poi poiNuevo = new Poi(id, idUser, name, address, type, urlIcon, description, phone, opening);

            poiNuevo.setScope("userContent");
            Toast.makeText(this, "Nuevo POI guardado", Toast.LENGTH_LONG).show();

            poiNuevo.setUsername(idUser);
            poiNuevo.setLatitud(latitud);
            poiNuevo.setLongitud(longitud);
            poiNuevo.setUrlWeb(web);
            sufijoURL = "?placeid="+ poiNuevo.getPlaceId()+"&username="+poiNuevo.getUsername()
                    +"&name="+poiNuevo.getName()+"&address="+poiNuevo.getAddress()+"&latitud="+
                    poiNuevo.getLatitud()+"&longitud="+poiNuevo.getLongitud()+"&type="+poiNuevo.getType()
                    +"&description="+poiNuevo.getDescription()+"&phone="+poiNuevo.getPhone()
                    +"&web="+poiNuevo.getUrlWeb()+"&icon="+poiNuevo.getUrlIcon()+"&scope="+poiNuevo.getScope();
            Log.i("MAP", "sufijo: " + sufijoURL);
            new setPoi().execute();
        } else {
            Toast.makeText(this, "No hay conexión a internet", Toast.LENGTH_LONG).show();
        }
    }

    //Método para comprobar si hay conexión a internet
    public boolean hayConexion() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getBaseContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnectedOrConnecting()) {
            return true;
        } else {
            return false;
        }
    }

    public LatLngBounds getLatLngBounds(LatLng center) {
        LatLngBounds.Builder builder = LatLngBounds.builder();
        builder.include(center);
        return builder.build();
    }

    public void cerrarSesion(){
        finish();
        Intent intentLogin = new Intent(NFCActivity.this, LoginActivity.class);
        intentLogin.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intentLogin);
        Toast.makeText(NFCActivity.this, "Te has desconectado", Toast.LENGTH_LONG).show();
    }

}