package maripi.pois.Controlador.Activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import maripi.pois.Modelo.GestorUsuario;
import maripi.pois.R;

public class RegistroActivity extends AppCompatActivity {
    private EditText txtUsuario;
    private EditText txtPass;
    private ProgressDialog progressDialog;
    private ArrayList<NameValuePair> datos;
    private GestorUsuario gestorUsuario;
    public String respuesta;
    public String respuesta2;
    String sufijoURL;
    private static String idUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registro);

        //Añadir toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        txtUsuario = (EditText) findViewById(R.id.txtUsuarioRegistro);
        txtPass = (EditText) findViewById(R.id.txtPassRegistro);

        //Inicializar gestor para acceder
        gestorUsuario = new GestorUsuario();
    }

    public void lblLoginOnClick(View view) {
        Intent intentLogin = new Intent(RegistroActivity.this, LoginActivity.class);
        startActivity(intentLogin);
    }

    public void btnRegistroOnClick(View view) {
        //Evento del botón Registrar, si hay conexión y el usuario ha rellenado los datos se añade a la BBDD
        if (hayConexion()) {
            if ((txtUsuario.getText().length() > 0 && txtPass.getText().length() > 0)) {
                datos = new ArrayList<NameValuePair>(2);
                datos.add(new BasicNameValuePair("usuario", txtUsuario.getText().toString()));
                datos.add(new BasicNameValuePair("password", txtPass.getText().toString()));
                progressDialog = ProgressDialog.show(RegistroActivity.this, null, "Comprobando datos...", true, false);
                new setUsuario().execute();
            } else {
                Toast.makeText(getBaseContext(), "Por favor, escriba usuario y contraseña", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(getBaseContext(), "No hay conexión a internet", Toast.LENGTH_SHORT).show();
        }
    }

    //CLASES ASÍNCRONAS
    private class setUsuario extends AsyncTask<Integer, Integer, Integer> {

        @Override
        protected Integer doInBackground(Integer... arg0) {
            respuesta = gestorUsuario.setUsuario(datos);
            return null;
        }

        @Override
        protected void onPostExecute(Integer result) {
            progressDialog.dismiss();
            if (respuesta != null && respuesta.indexOf("success") == 3) {
                //Se esconde el teclado
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(txtUsuario.getWindowToken(), 0);

                //Se informa del éxito
                Toast.makeText(getApplicationContext(), "Registrado: " + txtUsuario.getText().toString(), Toast.LENGTH_LONG).show();

                //Se crean sus preferencias
                new setPreferences().execute();

                //Se accede al login
                Intent intentLogin = new Intent(RegistroActivity.this, LoginActivity.class);
                intentLogin.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentLogin);
            } else {
                Toast.makeText(getBaseContext(), "Ha habido un error al registrar. Inténtelo de nuevo", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private class setPreferences extends AsyncTask<Integer, Integer, Integer> {
        @Override
        protected Integer doInBackground(Integer... arg0) {
            try {
                JSONObject jObj = new JSONObject(respuesta);
                String user = jObj.optString("user");
                jObj = new JSONObject(user);
                idUser = jObj.optString("uid");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            sufijoURL= "?idusername="+ idUser;
            respuesta2 = gestorUsuario.setPreferences(sufijoURL);
            return null;
        }

        @Override
        protected void onPostExecute(Integer result) {
            if (respuesta2 != null) {
                Log.i("Registro", "Preferencias: " + respuesta2);
            } else {
                Toast.makeText(getBaseContext(), "Ha habido un error al agregar las preferencias. Inténtelo de nuevo", Toast.LENGTH_SHORT).show();
            }
        }
    }

    //UTIL
    //Método para comprobar si hay conexión a internet
    public boolean hayConexion() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getBaseContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnectedOrConnecting()) {
            return true;
        } else {
            return false;
        }
    }
}
