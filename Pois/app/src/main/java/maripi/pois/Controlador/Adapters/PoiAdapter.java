package maripi.pois.Controlador.Adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.InputStream;
import java.net.URL;
import java.util.List;

import maripi.pois.Modelo.Poi;
import maripi.pois.R;

public class PoiAdapter extends ArrayAdapter<Poi> {
    public static final String TAG = "PoiAdapter";

    public PoiAdapter(Context context, List<Poi> pois) {
        super(context, R.layout.poi, pois);
    }

    //Método llamado internamente por el GridView para rellenar la vista
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater theInflater = LayoutInflater.from(getContext());
        View theView = theInflater.inflate(R.layout.poi, parent, false);
        Poi poi = getItem(position);
        TextView poiName = (TextView) theView.findViewById(R.id.lblNombre);
        poiName.setText(poi.getName());
        TextView poiAddress = (TextView) theView.findViewById(R.id.lblDireccion);
        poiAddress.setText(poi.getAddress());
        Log.e("status", "poi= " + poiName.getText().toString() + " y icon "+ poi.getIcon());
        if (poi.getIcon() != null) {
            ImageView poiIcon = (ImageView) theView.findViewById(R.id.ivTipo);
            poiIcon.setImageBitmap(poi.getIcon());
        } else {
            getIcono icono = new getIcono();
            icono.execute(new PoiViewContainer(poi, theView));
        }
        return theView;
    }

    public class PoiViewContainer {
        public Poi poi;
        public View view;

        public PoiViewContainer(Poi poi, View view) {
            this.poi = poi;
            this.view = view;
        }
    }

    private class getIcono extends AsyncTask<PoiViewContainer, String, PoiViewContainer> {
        @Override
        protected PoiViewContainer doInBackground(PoiViewContainer... params) {
            PoiViewContainer container = params[0];
            if (container != null) {
                container = getIcons(container);
                return container;
            }
            return null;
        }

        private PoiViewContainer getIcons(PoiViewContainer container) {
            try {
                String urlIcon = container.poi.getUrlIcon();
                InputStream inputStream = (InputStream) new URL(urlIcon).getContent();
                Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
                container.poi.setIcon(bitmap);
                inputStream.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return container;
        }
        @Override
        protected void onPostExecute(PoiViewContainer container) {
            ImageView poiIcon = (ImageView) container.view.findViewById(R.id.ivTipo);
            poiIcon.setImageBitmap(container.poi.getIcon());
        }
    }

}
