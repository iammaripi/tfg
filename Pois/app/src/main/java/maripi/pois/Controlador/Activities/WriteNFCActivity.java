package maripi.pois.Controlador.Activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.nfc.FormatException;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.Ndef;
import android.nfc.tech.NdefFormatable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.model.LatLng;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import maripi.pois.Modelo.GestorPois;
import maripi.pois.Modelo.Poi;
import maripi.pois.R;

@SuppressLint("NewApi")
public class WriteNFCActivity extends AppCompatActivity implements PlaceSelectionListener, GoogleApiClient.OnConnectionFailedListener{
    public static final String TAG = "WriteNFCActivity";
    private static final int PLACE_PICKER_REQUEST = 1;
    private String username="";
    private static String idUser;
    private static final String urlIcono = "https://maps.gstatic.com/mapfiles/place_api/icons/geocode-71.png";
    private GestorPois gestorPois;
    private GoogleApiClient mGoogleApiClient;
    private Place place;
    private LatLng latitudLongitud;
    private Button btnWrite;
    private EditText txtNombre;
    private EditText txtTelefono;
    private TextView txtDireccion;
    private TextView txtTipo;
    private TextView txtDescripcion;
    private TextView txtWeb;
    String nombre, direccion, tlfn;
    private String respuesta;
    String sufijoURL;
    String id;
    NfcAdapter adapter;
    PendingIntent pendingIntent;
    IntentFilter writeTagFilters[];
    boolean writeMode;
    Tag miTag;
    Context context;

    //Atributos para el Navigation Drawer
    private android.support.v4.widget.DrawerLayout DrawerLayout;
    private Toolbar toolbar;
    private NavigationView NavigationDrawer;
    private ActionBarDrawerToggle drawerToggle;
    //Fin Atributos para el Navigation Drawer

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.write_nfc);
        context = this;

        //Añadir toolbar
        toolbar = (Toolbar) findViewById(R.id.tool);
        setSupportActionBar(toolbar);

        Bundle bundle=getIntent().getExtras();
        idUser= bundle.getString("idUser");
        username= bundle.getString("username");

        btnWrite = (Button) findViewById(R.id.btnWriteNFC);

        //Inicializar gestor para acceder
        gestorPois = new GestorPois();

        //Inicializar el Navigation Drawer
        DrawerLayout = (DrawerLayout) findViewById(R.id.DrawerLayout);
        drawerToggle = setupDrawerToggle();

        DrawerLayout.setDrawerListener(drawerToggle);

        DrawerLayout = (DrawerLayout)findViewById(R.id.DrawerLayout);
        NavigationDrawer = (NavigationView)findViewById(R.id.nvView);

        View navView =  NavigationDrawer.getHeaderView(0);
        TextView nav_user = (TextView)navView.findViewById(R.id.name);
        nav_user.setText(username);

        NavigationDrawer.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        switch (menuItem.getItemId()) {
                            case R.id.nav_profile:
                                Intent intentPerfil = new Intent(WriteNFCActivity.this, UserProfileActivity.class);
                                intentPerfil.putExtra("idUser", idUser);
                                intentPerfil.putExtra("username", username);
                                startActivity(intentPerfil);
                                break;
                            case R.id.nav_busqueda:
                                Intent intentBusqueda = new Intent(WriteNFCActivity.this, SelectSearchActivity.class);
                                intentBusqueda.putExtra("idUser", idUser);
                                intentBusqueda.putExtra("username", username);
                                startActivity(intentBusqueda);
                                break;
                            case R.id.nav_listapois:
                                Intent intentListaPois = new Intent(WriteNFCActivity.this, PoiListActivity.class);
                                intentListaPois.putExtra("idUser", idUser);
                                intentListaPois.putExtra("username", username);
                                startActivity(intentListaPois);
                                break;
                            case R.id.nav_nuevo:
                                Intent intentNuevoPoi = new Intent(WriteNFCActivity.this, SelectNewPoiActivity.class);
                                intentNuevoPoi.putExtra("idUser", idUser);
                                intentNuevoPoi.putExtra("username", username);
                                startActivity(intentNuevoPoi);
                                break;
                            case R.id.nav_off:
                                cerrarSesion();
                                break;
                            case R.id.nav_help:
                                Intent intentAyuda = new Intent(WriteNFCActivity.this, HelpActivity.class);
                                intentAyuda.putExtra("idUser", idUser);
                                intentAyuda.putExtra("username", username);
                                startActivity(intentAyuda);
                                break;
                            default:
                                Log.i("NavigationView", "Ninguna opción pulsada");
                        }
                        menuItem.setChecked(true);
                        //setTitle(menuItem.getTitle());
                        DrawerLayout.closeDrawers();
                        return true;
                    }
                });
        //Fin Navigation Drawer

        //Escritura NFC
        adapter = NfcAdapter.getDefaultAdapter(this);
        pendingIntent = PendingIntent.getActivity(this, 0, new Intent(this,getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
        IntentFilter tagDetected = new IntentFilter(NfcAdapter.ACTION_TAG_DISCOVERED);
        tagDetected.addCategory(Intent.CATEGORY_DEFAULT);
        writeTagFilters = new IntentFilter[]{tagDetected};

        //Crear Cliente de la API de Google
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                //.addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        try {
            int PLACE_PICKER_REQUEST = 1;
            PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
            startActivityForResult(builder.build(WriteNFCActivity.this), PLACE_PICKER_REQUEST);
        } catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }

    }


    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.e(TAG, "La conexión con la API de Google Places falló con código de error: " +
                connectionResult.getErrorCode());
        Toast.makeText(this, "La conexión con la API de Google Places falló con código de error:" +
                connectionResult.getErrorCode(), Toast.LENGTH_LONG).show();
    }

    @Override
    public void onPlaceSelected(Place place) {
        //Log
    }

    //Método que se llama al pulsar un lugar
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_PICKER_REQUEST && resultCode == Activity.RESULT_OK) {
            place = PlacePicker.getPlace(this,data);
            final CharSequence name = place.getName();
            final CharSequence address = place.getAddress();
            final CharSequence phone = place.getPhoneNumber();

            latitudLongitud=place.getLatLng();

            //Elementos del layout
            txtNombre = (EditText) findViewById(R.id.txtNombreNFC);
            txtDireccion = (TextView) findViewById(R.id.txtDireccionNFC);
            txtTipo = (TextView) findViewById(R.id.txtTipoNFC);
            txtTelefono = (EditText) findViewById(R.id.txtTelefonoNFC);
            txtDescripcion = (TextView) findViewById(R.id.txtDescripNFC);
            txtWeb = (TextView) findViewById(R.id.txtWebNFC);

            txtNombre.setText("");
            txtDireccion.setText(address);
            txtTipo.setText(R.string.type);
            txtTelefono.setText(phone);
            if(place.getWebsiteUri() != null){
                final String web = place.getWebsiteUri().toString();
                txtWeb.setText(web);
            }

            btnWrite.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    nombre=txtNombre.getText().toString();
                    direccion=txtDireccion.getText().toString();
                    tlfn=txtTelefono.getText().toString();
                    guardarPOIenBBDDNFC(nombre,direccion, latitudLongitud.latitude, latitudLongitud.longitude, "TYPE_POINT_OF_INTEREST",
                            txtWeb.getText().toString(),urlIcono, txtDescripcion.getText().toString(), tlfn, "false");
                }
            });
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    public void guardarPOIenBBDDNFC(String name ,String address, Double latitud, Double longitud, String type, String web, String urlIcon, String description, String phone, String opening){
        if (hayConexion()) {
            id= generarId(name, address, phone);
            Poi poiNuevo = new Poi(id, idUser, name, address, type, urlIcon, description, phone, opening);

            poiNuevo.setScope("userContent");

            poiNuevo.setUsername(idUser);
            poiNuevo.setLatitud(latitud);
            poiNuevo.setLongitud(longitud);
            poiNuevo.setUrlWeb(web);
            sufijoURL = "?placeidnfc="+ poiNuevo.getPlaceId()+ "&name="+poiNuevo.getName()+"&address="+
                    poiNuevo.getAddress()+"&type="+poiNuevo.getType() + "&description="+poiNuevo.getDescription() +
                    "&phone="+poiNuevo.getPhone() +"&icon="+poiNuevo.getUrlIcon()+ "&scope="+poiNuevo.getScope() +
                    "&latitud="+ poiNuevo.getLatitud()+"&longitud="+poiNuevo.getLongitud()+"&urlWeb="+poiNuevo.getUrlWeb();
            Log.i("NFC", "sufijo: " + sufijoURL);
            try{//Escritura NFC
                NdefMessage msg = new NdefMessage(
                        new NdefRecord[] {
                                NdefRecord.createApplicationRecord("maripi.pois")});
                Log.i(TAG, "Sali");
                if(miTag == null){
                    Toast.makeText(context, "No hay tag", Toast.LENGTH_LONG).show();
                }else{
                    Log.i(TAG, "Tag: " + miTag);
                    write(id,miTag);
                    Toast.makeText(context,"Escrito correctamente", Toast.LENGTH_LONG).show();
                }
            }catch(IOException e){
                Toast.makeText(context,"Error de I/O",Toast.LENGTH_LONG).show();
                e.printStackTrace();
            }catch(FormatException e){
                Toast.makeText(context,"Error de formato", Toast.LENGTH_LONG).show();
                e.printStackTrace();
            }
            new setPoiNFC().execute();
        } else {
            Toast.makeText(this, "No hay conexión a internet", Toast.LENGTH_LONG).show();
        }
    }

    public String generarId(String name, String address, String phone){
        return "" + name.hashCode() + address.hashCode() + phone.hashCode();
    }

    @Override
    public void onError(Status status) {
        Log.e(TAG, "onError: Status = " + status.toString());
        Toast.makeText(this, "Poi selection failed: " + status.getStatusMessage(),
                Toast.LENGTH_SHORT).show();
    }

    private void write(String text, Tag tag) throws IOException, FormatException{
        //Crear array de elementos NdefRecord que es un registro del mensaje NDEF
        NdefRecord[] records = {createRecord(text)};
        //NdefMessage encapsula un mensaje Ndef(NFC Data Exchange Format)
        //Compuesto por varios registros encapsulados por la clase NdefRecord
        NdefMessage message = new NdefMessage(records);
        int size = message.toByteArray().length;
        try {
            Ndef ndef = Ndef.get(tag);
            if (ndef != null) {
                ndef.connect();
                if (!ndef.isWritable()) {
                    Toast.makeText(this, "Error: no se puede escribir en el tag", Toast.LENGTH_SHORT).show();
                }else {
                    if (ndef.getMaxSize() < size) {
                        Toast.makeText(getApplicationContext(), "Error: tag sin espacio suficiente", Toast.LENGTH_SHORT).show();
                    }
                    else {
                        ndef.writeNdefMessage(message);
                        ndef.close();
                    }
                }
            } else {
                Log.i(TAG, "Formato");
                NdefFormatable format = NdefFormatable.get(tag);
                if (format != null) {
                    try {
                        format.connect();
                        format.format(message);
                    } catch (IOException e) {
                        Log.e("nfcio", "excepcion"+  e);
                    }
                } else {
                    Log.i(TAG, "Error en el formato");
                }
            }
        } catch (Exception e) {
            Log.e("nfcwrite", "excepcion"+  e);
        }
    }
    //Codificar el mensaje para crear un NdefRecord
    @SuppressLint("NewApi")
    private NdefRecord createRecord(String text) throws UnsupportedEncodingException {
        String lang = "us";
        byte[] textBytes = text.getBytes();
        byte[] langBytes = lang.getBytes("US-ASCII");
        int langLength = langBytes.length;
        int textLength = textBytes.length;
        byte[] payLoad = new byte[1 + langLength + textLength];

        payLoad[0] = (byte) langLength;

        System.arraycopy(langBytes, 0, payLoad, 1, langLength);
        System.arraycopy(textBytes, 0, payLoad, 1 + langLength, textLength);

        return new NdefRecord(NdefRecord.TNF_WELL_KNOWN, NdefRecord.RTD_TEXT, new byte[0], payLoad);

    }
    //Intent para encontrar el Tag
    @SuppressLint("NewApi")
    protected void onNewIntent(Intent intent){
        if(NfcAdapter.ACTION_TAG_DISCOVERED.equals(intent.getAction())){
            miTag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
            Toast.makeText(this, "Tag encontrado: " + miTag.toString(), Toast.LENGTH_LONG).show();
        }
    }

    public void onPause(){
        super.onPause();
        WriteModeOff();
    }
    public void onResume(){
        super.onResume();
        WriteModeOn();
    }

    @SuppressLint("NewApi")
    private void WriteModeOn(){
        writeMode = true;
        adapter.enableForegroundDispatch(this, pendingIntent, writeTagFilters, null);
    }

    @SuppressLint("NewApi")
    private void WriteModeOff(){
        writeMode = false;
        adapter.disableForegroundDispatch(this);
    }

    //MENUS
    //Método para crear el menú
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search_cercanos, menu);
        return true;
    }

    //Método para lanzar los eventos de las opciones del menú
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (drawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        switch (item.getItemId()) {
            case R.id.search:
                Intent intentBusqueda = new Intent(this, SelectSearchActivity.class);
                MapActivity.sCurrentIntent = intentBusqueda;
                intentBusqueda.putExtra("idUser", idUser);
                intentBusqueda.putExtra("username", username);
                startActivity(intentBusqueda);
                return true;
            case R.id.home:
                Intent intentHome = new Intent(WriteNFCActivity.this, SelectionActivity.class);
                intentHome.putExtra("idUser", idUser);
                intentHome.putExtra("username", username);
                intentHome.putExtra("actividadOrigen","MapActivity");
                startActivity(intentHome);
                return true;
            case R.id.info:
                AlertDialog alertDialog = new AlertDialog.Builder(this).create();
                alertDialog.setTitle("Información");
                alertDialog.setMessage("Esta aplicación es el resultado del TFG Aplicación móvil " +
                        "de localización de POIs basada en preferencias de usuario y tecnologías " +
                        "de interacción por contacto. Desarrollada por María del Pilar Moreno Duque");
                alertDialog.setButton(DialogInterface.BUTTON_POSITIVE,"OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {}});
                alertDialog.setIcon(R.drawable.ic_info);
                alertDialog.show();
                return true;
            case R.id.salir:
                if (hayConexion()) {
                    cerrarSesion();
                } else {
                    Toast.makeText(this, "No hay conexión a internet", Toast.LENGTH_LONG).show();
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    //Navigation Drawer
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        drawerToggle.syncState();
    }

    //Navigation Drawer
    private ActionBarDrawerToggle setupDrawerToggle() {
        return new ActionBarDrawerToggle(this, DrawerLayout, toolbar, R.string.drawer_open,  R.string.drawer_close);
    }

    //Navigation Drawer
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        drawerToggle.onConfigurationChanged(newConfig);
    }


    @Override
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    protected void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();
    }

    //Método para comprobar si hay conexión a internet
    public boolean hayConexion() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getBaseContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnectedOrConnecting()) {
            return true;
        } else {
            return false;
        }
    }


    public void cerrarSesion(){
        finish();
        Intent intentLogin = new Intent(WriteNFCActivity.this, LoginActivity.class);
        intentLogin.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intentLogin);
        Toast.makeText(WriteNFCActivity.this, "Te has desconectado", Toast.LENGTH_LONG).show();
    }

    //CLASES ASÍNCRONAS EXTRAS

    //Clase para realizar la conexión de la petición de crear un POI NFC al servicio web
    private class setPoiNFC extends AsyncTask<Integer, Integer, Integer> {
        private boolean creado=false;
        @Override
        protected Integer doInBackground(Integer... arg0) {
            respuesta = gestorPois.setPoiNFC(sufijoURL);
            if (respuesta!=null && respuesta.contains("ok")) {
                creado=true;
            }else{
                creado=false;
            }
            return null;
        }

        @Override
        protected void onPostExecute(Integer result) {
            if(creado){
                Toast.makeText(getBaseContext(), "POI guardado en etiqueta NFC con id: " + id, Toast.LENGTH_LONG).show();
            }else{
                if (respuesta.contains("Duplicate")) {
                    Toast.makeText(getBaseContext(), "El POI ya estaba en la BBDD", Toast.LENGTH_LONG).show();
                }
                else{
                    Toast.makeText(getBaseContext(), "Error al crear la etiqueta", Toast.LENGTH_LONG).show();
                }
            }
        }
    }//Fin clase setPoi
}
