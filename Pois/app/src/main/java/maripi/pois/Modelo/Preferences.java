package maripi.pois.Modelo;

import java.util.ArrayList;

public class Preferences {

    private int id;
    private String username;
    private String name;
    private int valoracion;
    private String isOpen;
    private ArrayList alimentacion;
    private ArrayList alojamiento;
    private ArrayList otros;

    public Preferences() {
        this.id = 1000;
        this.username = "";
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValoracion() {
        return valoracion;
    }

    public void setValoracion(int valoracion) {
        this.valoracion = valoracion;
    }

    public String getIsOpen() {
        return isOpen;
    }

    public void setIsOpen(String isOpen) {
        this.isOpen = isOpen;
    }

    public ArrayList getAlimentacion() {
        return alimentacion;
    }

    public void setAlimentacion(ArrayList alimentacion) {
        this.alimentacion = alimentacion;
    }

    public ArrayList getAlojamiento() {
        return alojamiento;
    }

    public void setAlojamiento(ArrayList alojamiento) {
        this.alojamiento = alojamiento;
    }

    public ArrayList getOtros() {
        return otros;
    }

    public void setOtros(ArrayList otros) {
        this.otros = otros;
    }

    @Override
    public String toString() {
        return id + "\n" + username;
    }

}
