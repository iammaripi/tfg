package maripi.pois.Modelo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ParserJson {

    //Método que recoge el JSON recibido del servicio web que pide una lista de lugares concretos
    // y lo trabaja para mostrarlo
    public List<Poi> parserBusquedaPoi(String contenido) {
        List<Poi> pois = new ArrayList<>();
        try {
            JSONObject jso = new JSONObject(contenido);
            JSONArray jsa = jso.getJSONArray("results");
            if (jsa.length() > 0) {
                for (int i = 0; i < jsa.length(); i++) {
                    Poi lugar = new Poi();
                    JSONObject jso1 = jsa.getJSONObject(i);
                    lugar.setPlaceId(jso1.getString("place_id"));
                    lugar.setName(jso1.getString("name"));
                    lugar.setAddress(jso1.getString("formatted_address"));

                    JSONObject ubicacion = jso1.getJSONObject("geometry").getJSONObject("location");
                    lugar.setLatitud(ubicacion.getDouble("lat"));
                    lugar.setLongitud(ubicacion.getDouble("lng"));
                    lugar.setUrlIcon(jso1.getString("icon"));
                    lugar.setType(jso1.getJSONArray("types").getString(0).replace("_", " "));
                    lugar.setScope("onlineContent");
                    pois.add(lugar);
                }
                return pois;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    //Método que recoge el JSON recibido del servicio web que pide una lista de lugares sugeridos
    // según palabras clave y cercanía y lo trabaja para mostrarlo
    public List<Poi> parserBusquedaSugeridos(String contenido) {
        List<Poi> lugares = new ArrayList<>();
        try {
            JSONObject jso = new JSONObject(contenido);
            JSONArray jsa = jso.getJSONArray("results");
            if (jsa.length() > 0) {
                for (int i = 0; i < jsa.length(); i++) {
                    Poi lugar = new Poi();
                    JSONObject jso1 = jsa.getJSONObject(i);
                    lugar.setPlaceId(jso1.getString("place_id"));
                    lugar.setName(jso1.getString("name"));
                    lugar.setAddress(jso1.getString("formatted_address"));

                    JSONObject ubicacion = jso1.getJSONObject("geometry").getJSONObject("location");
                    lugar.setLatitud(ubicacion.getDouble("lat"));
                    lugar.setLongitud(ubicacion.getDouble("lng"));
                    lugar.setUrlIcon(jso1.getString("icon"));
                    lugar.setType(jso1.getJSONArray("types").getString(0).replace("_", " "));
                    lugar.setScope("onlineContent");
                    lugares.add(lugar);
                }
                return lugares;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    //Método que recoge el JSON recibido del servicio web que pide toda la información de un
    // lugar concreto y lo trabaja para mostrarlo
    public Poi parserLugar(String jsonContent) {
        Poi poi = new Poi();
        try {
            JSONObject jso = new JSONObject(jsonContent);
            JSONObject jso1 = jso.getJSONObject("result");
            poi.setPlaceId(jso1.getString("place_id"));
            poi.setName(jso1.getString("name"));
            poi.setAddress(jso1.getString("formatted_address"));
            JSONObject location = jso1.getJSONObject("geometry").getJSONObject("location");
            poi.setLatitud(location.getDouble("lat"));
            poi.setLongitud(location.getDouble("lng"));
            poi.setUrlIcon(jso1.getString("icon"));
            String type = jso1.getJSONArray("types").getString(0).replace("_", " ");
            type = type.substring(0, 1).toUpperCase() + type.substring(1);
            poi.setType(type);

            if (jso1.has("photos")) {
                poi.setPhotoReferences(new ArrayList<String>());
                JSONArray photos = jso1.getJSONArray("photos");
                for (int i = 0; i < photos.length(); i++) {
                    JSONObject photo = photos.getJSONObject(i);
                    poi.getPhotoReferences().add(photo.getString("photo_reference"));
                }
            }

            if (jso1.has("website")) {
                String urlWeb = jso1.getString("website");
                poi.setUrlWeb(urlWeb);
            }

            if (jso1.has("opening_hours")) {
                JSONObject openingHours = jso1.getJSONObject("opening_hours");
                String opening = Boolean.toString(openingHours.getBoolean("open_now"));
                poi.setOpening(opening);
                JSONArray weekdayText = openingHours.getJSONArray("weekday_text");
                String weekdayHours = "";
                for (int i = 0; i < weekdayText.length(); i++) {
                    weekdayHours += weekdayText.getString(i) + "<br/>";
                }
                poi.setOpeningHours(weekdayHours);
            }

            if (jso1.has("reviews")) {
                JSONArray reviews = jso1.getJSONArray("reviews");
                String reviewsText = "";
                // for each review, retrieve the review text and concatenate
                // them into a string of reviews with a number in front of
                // each review.
                for (int i = 0; i < reviews.length(); i++) {
                    JSONObject review = reviews.getJSONObject(i);
                    reviewsText += (i + 1) + ".&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                            + review.getString("text") + "<br/><br/>";
                }
                poi.setReviews(reviewsText);
            }

            if (jso1.has("formatted_phone_number")) {
                String phone = jso1.getString("formatted_phone_number");
                poi.setPhone(phone);
            }
            poi.setScope("onlineContent");
            return poi;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
}
