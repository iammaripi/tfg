package maripi.pois.Modelo;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class Conexion {

    public String retrieveData(String URL) throws IOException {
        BufferedReader read = null;
        URL url;
        HttpURLConnection conexion;
        StringBuilder stringBuilder;
        String l;
        try {
            url = new URL(URL);
            conexion = (HttpURLConnection) url.openConnection();
            conexion.setReadTimeout(10000);
            conexion.setConnectTimeout(15000);
            conexion.setRequestMethod("GET");
            conexion.connect();
            stringBuilder = new StringBuilder();

            read = new BufferedReader(new InputStreamReader(conexion.getInputStream()));
            while ((l = read.readLine()) != null) {
                stringBuilder.append(l + "\n");
            }

            return stringBuilder.toString();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            if (read != null) {
                read.close();
            }
        }
    }
}
