package maripi.pois.Modelo;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class GestorPois {

    public ArrayList<Poi> getPoi(String idUser) {
        String urlGetPoi="http://pois.esy.es/getPoi.php";
        ArrayList<Poi> pois = new ArrayList<>();
        String respuesta = null;
        BufferedReader bufferedReader = null;
        try {
            URL url = new URL(urlGetPoi +"?username=" + idUser);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            bufferedReader = new BufferedReader(new InputStreamReader(con.getInputStream()));
            respuesta = bufferedReader.readLine();

            JSONArray jsa = new JSONArray(respuesta);
            for (int i = 0; i < jsa.length(); i++) {

                JSONObject jso = jsa.getJSONObject(i);
                Poi poi = new Poi();

                Log.e("status", "json= " + jso);

                poi.setId(jso.getInt("id"));
                poi.setPlaceId(jso.getString("placeid"));
                poi.setUsername(jso.getString("idusername"));
                poi.setName(jso.getString("name"));
                poi.setAddress(jso.getString("address"));
                poi.setType(jso.getString("type"));
                poi.setDescription(jso.getString("description"));
                poi.setPhone(jso.getString("phone"));
                poi.setUrlIcon(jso.getString("icon"));
                poi.setScope(jso.getString("scope"));
                poi.setUrlWeb(jso.getString("web"));
                poi.setLongitud(jso.getDouble("longitud"));
                poi.setLatitud(jso.getDouble("latitud"));

                pois.add(poi);
            }
        } catch (Exception e) {
            return null;
        }
        return pois;
    }

    public String setPoi(String sufijo) {
        String urlSetPoi="http://pois.esy.es/setPoi.php";
        BufferedReader bufferedReader = null;
        String respuesta = null;
        try {
            sufijo = reemplazar(sufijo);
            URL url = new URL(urlSetPoi + sufijo);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            bufferedReader = new BufferedReader(new InputStreamReader(con.getInputStream()));
            respuesta = bufferedReader.readLine();
        } catch (Exception e) {
            return null;
        }
        return respuesta;
    }

    public String deletePoi(String sufijo) {
        String urlDeletePoi="http://pois.esy.es/deletePoi.php";
        BufferedReader bufferedReader = null;
        String respuesta = null;
        try {
            URL url = new URL(urlDeletePoi + sufijo);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            bufferedReader = new BufferedReader(new InputStreamReader(con.getInputStream()));
            respuesta = bufferedReader.readLine();
        } catch (Exception e) {
            return null;
        }
        return respuesta;
    }

    public Poi getNFC(String idNFC, String idUser) {
        String urlGetNFC="http://pois.esy.es/getNFC.php";
        Poi  poiNFC = new Poi();
        String respuesta = null;
        BufferedReader bufferedReader = null;
        try {
            URL url = new URL(urlGetNFC +"?idnfc=" + idNFC);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            bufferedReader = new BufferedReader(new InputStreamReader(con.getInputStream()));
            respuesta = bufferedReader.readLine();

            JSONArray jsa = new JSONArray(respuesta);
            for (int i = 0; i < jsa.length(); i++) {

                JSONObject jso = jsa.getJSONObject(i);
                Log.e("status", "json= " + jso);

                poiNFC.setId(jso.getInt("id"));
                poiNFC.setPlaceId(jso.getString("placeidnfc"));
                poiNFC.setUsername(idUser);
                poiNFC.setName(jso.getString("name"));
                poiNFC.setAddress(jso.getString("address"));
                poiNFC.setType(jso.getString("type"));
                poiNFC.setDescription(jso.getString("description"));
                poiNFC.setPhone(jso.getString("phone"));
                poiNFC.setUrlIcon(jso.getString("icon"));
                poiNFC.setScope(jso.getString("scope"));
                poiNFC.setKeywords(jso.getString("keywords"));
                poiNFC.setLongitud(jso.getDouble("longitud"));
                poiNFC.setLatitud(jso.getDouble("latitud"));
                poiNFC.setUrlWeb(jso.getString("urlWeb"));

                Log.e("nfc", "json= " + jso);
                Log.e("nfc", "poi= " + poiNFC);
            }

        } catch (Exception e) {
            Log.e("nfc", "excepcion"+  e);
            return null;
        }
        return poiNFC;
    }

    public String setPoiNFC(String sufijo) {
        String urlSetPoiNFC="http://pois.esy.es/setPoiNFC.php";
        BufferedReader bufferedReader = null;
        String respuesta = null;
        try {
            sufijo = reemplazar(sufijo);
            URL url = new URL(urlSetPoiNFC + sufijo);
            Log.i("NFC", "url: " + url);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            bufferedReader = new BufferedReader(new InputStreamReader(con.getInputStream()));
            respuesta = bufferedReader.readLine();
            Log.i("NFC", "respuesta: " + respuesta);
        } catch (Exception e) {
            return null;
        }
        return respuesta;
    }

    public String reemplazar(String sufijo){
        sufijo = sufijo.replaceAll(" ", "%20");
        return sufijo;
    }


}
