package maripi.pois.Modelo;

import android.util.Log;

import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class GestorUsuario {
    private JSONObject jObj;

    //Método para realizar la conexión de la petición de comprobación de usuario al servicio web
    public String check(List<NameValuePair> nameValuePairs) {
        String urlGetUser = "http://pois.esy.es/access.php";
        Log.i("Login", "Entro");
        String respuesta = null;
        HttpClient httpclient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost(urlGetUser);
        Log.i("Login", "tengo la url");
        try {
            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
            ResponseHandler<String> responseHandler = new BasicResponseHandler();
            respuesta = httpclient.execute(httppost, responseHandler);
            Log.i("Login", "He ejecutado la conexion");
            jObj = new JSONObject(respuesta);

        } catch (Exception e) {
            e.printStackTrace();
        }
        Log.i("Login", "Petición: " + urlGetUser);
        Log.i("Login", "Respuesta JSON:\n" + jObj);
        return respuesta;
    }

    //Método para crear un usuario
    public String setUsuario(List<NameValuePair> nameValuePairs) {
        String urlSetUser = "http://pois.esy.es/setuser.php";
        String respuesta = null;
        HttpClient httpclient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost(urlSetUser);
        try {
            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
            ResponseHandler<String> responseHandler = new BasicResponseHandler();
            respuesta = httpclient.execute(httppost, responseHandler);
            jObj = new JSONObject(respuesta);

        } catch (Exception e) {
            e.printStackTrace();
        }
        Log.e("Registro", "json= " + jObj);
        Log.e("Registro", "respuesta= " + respuesta);
        Log.e("Registro", "respuesta index= " + respuesta.indexOf("success"));
        return respuesta;
    }

    //Método para conseguir las preferencias de un usuario
    public ArrayList<Preferences> getPreferences(String idUser) {
        String urlGetPreferences = "http://pois.esy.es/getPreferences.php";
        String urlGetKeys = "http://pois.esy.es/getKeys.php";
        ArrayList<Preferences> preferencias = new ArrayList<>();
        ArrayList<String> alimentacion = new ArrayList<>();
        ArrayList<String> alojamiento = new ArrayList<>();
        ArrayList<String> otros = new ArrayList<>();
        Preferences preferencia = new Preferences();
        String respuesta = null;
        BufferedReader bufferedReader = null;
        try {
            URL url = new URL(urlGetPreferences +"?username=" + idUser);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            bufferedReader = new BufferedReader(new InputStreamReader(con.getInputStream()));
            respuesta = bufferedReader.readLine();
            Log.i("status", "respuesta= " + respuesta);

            JSONArray jsa = new JSONArray(respuesta);
            for (int i = 0; i < jsa.length(); i++) {
                JSONObject jso = jsa.getJSONObject(i);
                Log.e("status", "json= " + jso);

                preferencia.setId(jso.getInt("id"));
                preferencia.setUsername(jso.getString("idusername"));
                preferencia.setName(jso.getString("nombre"));
                preferencia.setValoracion(jso.getInt("valoracion"));
                preferencia.setIsOpen(jso.getString("isopen"));
            }
            URL url2 = new URL(urlGetKeys +"?idpreference=" + preferencia.getId());
            HttpURLConnection con2 = (HttpURLConnection) url2.openConnection();
            bufferedReader = new BufferedReader(new InputStreamReader(con2.getInputStream()));
            respuesta = bufferedReader.readLine();
            Log.i("status", "respuesta keys= " + respuesta);

            if(respuesta!=null){
                JSONArray jsa2 = new JSONArray(respuesta);

                for (int i = 0; i < jsa2.length(); i++) {
                    JSONObject jso = jsa2.getJSONObject(i);
                    Log.e("status", "json= " + jso);

                    if(jso.getString("categoria").equals("alimentacion")){
                        alimentacion.add(jso.getString("key"));
                    }else{
                        if(jso.getString("categoria").equals("alojamiento")){
                            alojamiento.add(jso.getString("key"));
                        }else{
                            if(jso.getString("categoria").equals("otros")){
                                otros.add(jso.getString("key"));
                            }
                        }
                    }
                }
                preferencia.setAlimentacion(alimentacion);
                preferencia.setAlojamiento(alojamiento);
                preferencia.setOtros(otros);
            }
            preferencias.add(preferencia);
        } catch (Exception e) {
            Log.e("status", "excepcion= " + e);
            return null;
        }
        return preferencias;
    }

    //Método para modificar las preferencias
    public String updatePreferences(String sufijo, String sufijo2, String sufijo3) {
        String urlUpdatePreferences = "http://pois.esy.es/updatePreferences.php";
        String urlSetKeys = "http://pois.esy.es/setKeys.php";
        String urlUpdateKeys = "http://pois.esy.es/updateKeys.php";
        BufferedReader bufferedReader = null;
        String respuesta = null;
        String respuesta2 = null;
        String respuesta3 = null;
        try {
            sufijo = reemplazar(sufijo);
            URL url = new URL(urlUpdatePreferences + sufijo);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            bufferedReader = new BufferedReader(new InputStreamReader(con.getInputStream()));
            respuesta = bufferedReader.readLine();

            sufijo2 = reemplazar(sufijo2);
            URL url2 = new URL(urlUpdateKeys + sufijo2);
            HttpURLConnection con2 = (HttpURLConnection) url2.openConnection();
            bufferedReader = new BufferedReader(new InputStreamReader(con2.getInputStream()));
            respuesta2 = bufferedReader.readLine();

            sufijo3 = reemplazar(sufijo3);
            URL url3 = new URL(urlSetKeys + sufijo3);
            HttpURLConnection con3 = (HttpURLConnection) url3.openConnection();
            bufferedReader = new BufferedReader(new InputStreamReader(con3.getInputStream()));
            respuesta3 = bufferedReader.readLine();

        } catch (Exception e) {
            Log.e("status", "excepcion= " + e);
            return null;
        }
        return respuesta3;
    }

    //Método para crear las preferencias cuando un usuario se registre
    public String setPreferences(String sufijo) {
        String urlSetPreferences = "http://pois.esy.es/setPreferences.php";
        BufferedReader bufferedReader = null;
        String respuesta = null;
        try {
            sufijo = reemplazar(sufijo);
            URL url = new URL(urlSetPreferences + sufijo);
            Log.i("Registro", "URL: " + url);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            bufferedReader = new BufferedReader(new InputStreamReader(con.getInputStream()));
            respuesta = bufferedReader.readLine();
        } catch (Exception e) {
            Log.i("Registro", "Excepcion: " + e);
            return null;
        }
        return respuesta;
    }

    public String reemplazar(String sufijo){
        sufijo = sufijo.replaceAll(" ", "%20");
        return sufijo;
    }

}
