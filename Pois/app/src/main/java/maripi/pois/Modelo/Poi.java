package maripi.pois.Modelo;

import android.graphics.Bitmap;

import java.util.List;

public class Poi {

    private int id;
    private String username;
    private String placeid;
    private String name;
    private String address;
    private String type;
    private String phone;
    private String urlIcon;
    private String description;
    private String opening;
    private String scope;
    private Bitmap icon;
    private double latitud;
    private double longitud;
    private List<Bitmap> photos;
    private List<String> photoReferences;
    private String urlWeb;
    private String openingHours;
    private String reviews;
    private String keywords;

    public Poi(String placeid, String username, String name, String address, String type, String urlIcon, String description, String phone, String opening) {
        this.placeid = placeid;
        this.username = username;
        this.name = name;
        this.address = address;
        this.type = type;
        this.urlIcon = urlIcon;
        this.phone = phone;
        this.description = description;
        this.opening = opening;
        this.urlWeb = "Sin web";
    }

    public Poi() {
        this.phone = "Sin teléfono";
        this.description = "Sin descripción";
        this.urlWeb = "Sin web";
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getOpening(){
        return opening;
    }

    public void setOpening(String opening){
        this.opening = opening;
    }

    public String getUrlWeb() {
        return urlWeb;
    }

    public void setUrlWeb(String urlWeb) {
        this.urlWeb = urlWeb;
    }

    public String getPlaceId() {
        return placeid;
    }

    public void setPlaceId(String placeid) {
        this.placeid = placeid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
                this.name = name;
    }

    public String getAddress() {
                return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public double getLatitud() {
        return latitud;
    }

    public void setLatitud(double latitud) {
        this.latitud = latitud;
    }

    public double getLongitud() {
        return longitud;
    }

    public void setLongitud(double longitud) {
        this.longitud = longitud;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getReviews() {
        return reviews;
    }

    public void setReviews(String reviews) {
        this.reviews = reviews;
    }

    public List<Bitmap> getPhotos() {
        return photos;
    }

    public void setPhotos(List<Bitmap> photos) {
        this.photos = photos;
    }

    public List<String> getPhotoReferences() {
        return photoReferences;
    }

    public void setPhotoReferences(List<String> photoReferences) {
        this.photoReferences = photoReferences;
    }

    public Bitmap getIcon() {
        return icon;
    }

    public void setIcon(Bitmap icon) {
        this.icon = icon;
    }

    public String getUrlIcon() {
        return urlIcon;
    }

    public void setUrlIcon(String urlIcon) {
        this.urlIcon = urlIcon;
    }

    public String getOpeningHours() {
        return openingHours;
    }

    public void setOpeningHours(String openingHours) {
        this.openingHours = openingHours;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    @Override
    public String toString() {
        return name + "\n" + address + "\n" + type + "\n" + urlWeb;
    }

}
