# **TFG: Aplicación móvil de localización de POIs basada en preferencias de usuario y tecnologías de interacción por contacto**

Este es el Trabajo Fin de Grado realizado por María del Pilar Moreno Duque para el Grado en Ingeniería Informática de la Escuela Superior de Informática de Ciudad Real [ESI](http://webpub.esi.uclm.es/).

### **¿Qué es este TFG?** ###
Este Trabajo Fin de Grado tiene como objetivo ofrecer a los usuarios una herramienta
de búsqueda personalizada de Puntos de Interés que integre dos de las tecnologías actuales
que poseen los smartphones en la actualidad, la geolocalización y la tecnología NFC de
interacción por contacto.

La aplicación ofrece a los usuarios la posibilidad de buscar lugares según sus necesidades
específicas, consultar sus detalles y almacenarlos para su posterior gestión.

Además, ofrece la posibilidad de integrarse en un entorno de Smart City, facilitando
la inclusión de Puntos de Interés mediante etiquetas NFC. Así, el usuario minimizará su
interacción con la aplicación y podrá aportar nueva información de lugares que no existen en otras aplicaciones similares.

### **Documentación** ###

Puede encontrar toda la documentación necesaria en el siguiente enlace [Documentación](https://bitbucket.org/iammaripi/tfg/src/d0804593d6a9f2fc27e71907991bb515a327b008/Doc/).