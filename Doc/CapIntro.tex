\chapter{Introducción}
\label{ch:Introduccion}

\section{Contexto}
En la actualidad, 3500 millones de personas tienen acceso a internet \cite{Acceso}, y más de 2000 millones lo hacen desde dispositivos móviles\cite{AccesoMovil}. Eso representa un 45\% de la población mundial.

Además, el progreso en las tecnologías de geolocalización\index{geolocalización} y, en concreto, el GPS (Global Positioning System)\index{geolocalización!GPS} \footnote{\url{http://www.gps.gov/systems/gps/spanish.php}} ha supuesto que estas tecnologías se hayan convertido en imprescindibles para muchos de estos usuarios. Las aplicaciones que se sirven del GPS se utilizan en ámbitos como la agricultura, cartografía, topografía, cronometría...Todo smartphone tiene una o varias aplicaciones que hacen uso del GPS. Por tanto, esos 2000 millones tienen acceso y podrían utilizar en alguna ocasión su smartphone para abrir una de estas aplicaciones.

Estos dispositivos también cuentan con otras tecnologías, las cuales pueden complementarse con el GPS. En este sentido, NFC (Near Field Communication) es una tecnología de comunicación inalámbrica, de corto alcance y alta frecuencia. Permite el almacenamiento y posterior intercambio de datos entre smartphones que posean dicha tecnología, o elementos pasivos (tales como etiquetas NFC) y smartphones. Aunque no es nueva, en los últimos años se están fabricando cada vez más dispositivos que hacen uso de ella, desde smartphones hasta consolas portátiles.

Es importante resaltar la importancia de este tipo de aplicaciones y sistemas en un entorno de Ciudad Inteligente (conocido a nivel global como Smart City)\index{Smart City}. Una Smart City es una ciudad que usa las TIC (Tecnologías de la Información y la Comunicación) para hacer que su infraestructura, componentes y servicios públicos ofrecidos sean más interactivos, eficientes y los ciudadanos puedan ser más conscientes de ellos. Las Smart Cities conllevan una reducción del gasto público y una mejora de la calidad de los servicios que prestan, mejorando la información aportada a los ciudadanos y la toma de decisiones \cite{SmartCities}. Constituyen una vía para la innovación, favoreciendo la creación de nuevos negocios e ideas.

La idea de este TFG es desarrollar una aplicación que permita realizar búsquedas de Puntos de Interés (en adelante POI, de Point of Interest), según sus preferencias. Los POIs serán lugares de interés para el usuario que se encuentran en una ubicación específica. Esta aplicación se integrará en un contexto de Smart City mediante la interacción con etiquetas NFC que contendrán información de lugares de la ciudad.


\section{Motivación}

Existe un potencial en el desarrollo de aplicaciones que integren tecnologías de geolocalización y de interacción por contacto, como se demuestra en el estudio del Estado del Arte del capítulo ~\ref{ch:Antecedentes}.

Las aplicaciones incluidas en los sistemas operativos móviles que usan tecnologías de geolocalización son generales y están pensadas para dar soporte al máximo número de usuarios, sin atender a sus preferencias, que en ocasiones, como es el ámbito de la salud, será necesario tener en cuenta. Además, estas aplicaciones no integran tecnologías de interacción por contacto. 

Moverse por ciudades desconocidas o encontrar puntos de interés en ellas puede suponer un problema para los usuarios, que además quieren limitar el tiempo que pasan explorando este tipo de aplicaciones, pues lo importante es disfrutar de la ciudad. Desde siempre se han solicitado recomendaciones antes de viajar a la ciudad en cuestión o se ha preguntado a los residentes de la zona, pero eso no asegura que vayan a cumplir las necesidades del usuario final.

Integrando la tecnologías GPS y NFC se puede ofrecer al usuario una experiencia de búsqueda de Puntos de Interés de acuerdo a sus necesidades específicas, mientras que se está en continua interacción con la ciudad y sus lugares.


\section{Solución propuesta}
Aprovechando el asentamiento de la tecnología GPS y el auge de la tecnología NFC surge este TFG, donde se pretende desarrollar una aplicación Android que permita al usuario realizar búsquedas personalizadas de POIs. Ésto se logrará mediante la gestión de preferencias de usuario.

Las preferencias\index{preferencias} se especificarán y se incluirán  como filtros en las búsquedas. Se podrá filtrar una búsqueda para que sólo se muestren lugares que se encuentren abiertos en ese momento, requisito indispensable si se quiere acudir inmediatamente al establecimiento que se pretende buscar o, en el caso de lugares que ofrezcan servicios de salud, como farmacias o centros médicos, que estén disponibles para ofrecer esos servicios.

La aplicación también ofrecerá soporte a usuarios con necesidades específicas. Por ejemplo, los usuarios que sean intolerantes pueden filtrar los resultados de una búsqueda de restaurantes o establecimientos a aquellos que satisfacen sus necesidades.

Además, los usuarios podrán guardar los resultados de las búsquedas para su posterior visualización y gestión. Se podrán consultar detalles de los sitios, tales como sus horarios, fotos y valoraciones de otros usuarios.

También se ofrecerá la posibilidad de incluir POIs mediante etiquetas NFC que incluyan información de lugares que no existen en la base de datos del servicio que proporciona las búsquedas (Google). La integración de la tecnología NFC en este TFG minimizará la interacción del usuario, que no tendrá que incluir manualmente el POI si éste se encuentra en una etiqueta. La aplicación también dará soporte a la escritura de las etiquetas con la información de un lugar.

\section{Estructura del documento}
La documentación de este TFG se ha elaborado conforme a la normativa de TFG de la Escuela Superior de Informática de la Universidad de Castilla-La Mancha. Está estructurado en capítulos, resumidos a continuación.

\subsection*{Capítulo 1: Introducción}
Este capítulo introduce el tema del TFG, su contexto, su justificación y motivación para el desarrollo del mismo, describiendo de forma general la solución propuesta.

\subsection*{Capítulo 2: Objetivos y herramientas de desarrollo}
Se exponen los objetivos para resolver el problema y los requisitos necesarios del proyecto para abordar la solución. Se definen, además, el entorno de trabajo y las herramientas y medios hardware y software empleados. 

\subsection*{Capítulo 3: Fundamentos tecnológicos y Estado del Arte}
En este capítulo se explican con detalle las tecnologías que integrará la aplicación resultante. 

Además, se analizarán las principales aplicaciones existentes que dan solución al problema y, mediante un estudio de sus ventajas y debilidades, se obtendrán conclusiones de las que se recogerá información relevante para la obtención de requisitos y definición de funcionalidades del proyecto.

También se realizará un estudio de trabajos de investigación existentes en base a las tecnologías que utiliza la propuesta de este TFG.


\subsection*{Capítulo 4: Método de trabajo}
Se explica la metodología de trabajo que se ha seleccionado para abordar el desarrollo de este TFG. En base a ella, se mostrarán los procesos que la caracterizan.

\subsection*{Capítulo 5: Resultados}
Este capítulo mostrará los resultados del desarrollo del TFG, organizado en iteraciones. Además, se detallarán los resultados fruto de la evaluación de la usabilidad de la aplicación y los resultados de las pruebas realizadas. 

En este capítulo también se mostrará con detalle el esfuerzo dedicado al proyecto, con los costes del mismo y las horas empleadas en su desarrollo.

\subsection*{Capítulo 6: Conclusiones}
Este último capítulo resumirá las conclusiones obtenidas, los problemas encontrados y los objetivos alcanzados al término del desarrollo del proyecto.
Se especificarán también las posibles mejoras futuras que se podrían realizar.

\subsection*{Anexo A: Manual de la aplicación}
Incluye un manual de uso de la aplicación, así como de su instalación. 

\subsection*{Anexo B: Mapa de navegación de la aplicación}
Incluye un mapa con las diferentes pantallas de la aplicación y la relación entre ellas. 

\subsection*{Anexo C: Formulario de test de usabilidad}
Incluye los formularios completados por los usuarios que realizaron el test de usabilidad.

\subsection*{Anexo D: Scripts de creación de tablas y consultas}
Incluye el código necesario para la creación de las tablas de la base de datos, así como las consultas SQL necesarias para la aplicación.