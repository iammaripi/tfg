\chapter{Inclusión de ecuaciones y listados de código}
\label{ch:ecsprogs}

\section{Fórmulas matemáticas}
Para que \LaTeX{} pueda incluir muchos símbolos matemáticos es preciso incluir algunos paquetes que ayudan en dicha tarea: \texttt{amsmath}, \texttt{amsfonts}, \texttt{amssymb}. También hay que tener en cuenta que si el tipo principal empleado en el texto es Times y se desea utilizar un tipo coherente en las fórmulas es conveniente emplear el paquete \emph{mathptmx} en vez de \emph{Times}. Pero en este caso es recomendable incluir siempre paquetes adicionales para suministrar las otras dos familias de fuentes escalables (p.~ej.\ \texttt{helvet} para familia \textsf{palo seco} y \texttt{couriers} para \texttt{monoespaciada}). Si no se hace esta última inclusión pueden obtenerse errores de difícil diagnóstico.

\subsection{Fórmulas creadas en línea y con entorno \texttt{equation}}
Es muy sencillo incluir fórmulas matemáticas sencillas en el mismo texto en el que se escribe. Por ejemplo, $c^{2}=a^{2}+b^{2}$ que podría ser la ecuación representativa del teorema de Pitágoras.

Las fórmulas también se pueden separar del texto para que aparezcan destacadas, así:

% Ejemplo: Ecuación no numerada realizada desde MathType
% ============
% Este ejemplo contiene el código para MathType (TeXaide) y por tanto puede ser copiado en dicho programa para modificar la f´romula sin necesidad de utilizar un fichero auxiliar.
% MathType!MTEF!2!1!+-
% feaafaart1ev1aaatCvAUfeBSjuyZL2yd9gzLbvyNv2CaerbuLwBLn
% hiov2DGi1BTfMBaeXatLxBI9gBaerbd9wDYLwzYbItLDharqqr1ngB
% PrgifHhDYfgasaacH8srps0lbbf9q8WrFfeuY-Hhbbf9v8qqaqFr0x
% c9pk0xbba9q8WqFfea0-yr0RYxir-Jbba9q8aq0-yq-He9q8qqQ8fr
% Fve9Fve9Ff0dmeaabaqaciGacaGaaeqabaWaaeaaeaaakeaacaWGJb
% WaaWbaaSqabeaacaaIYaaaaOGaeyypa0Zaa8qaaeaadaqadaqaaiaa
% dggadaahaaWcbeqaaiaaikdaaaGccqGHRaWkcaWGIbWaaWbaaSqabe
% aacaaIYaaaaaGccaGLOaGaayzkaaaaleqabeqdcqGHRiI8aOGaeyyX
% ICTaamizaiaadIhaaaa!46D4!
\[
c^2  = \int {\left( {a^2  + b^2 } \right)}  \cdot dx
\]




Pero si se desea, las ecuaciones pueden ser numeradas de forma automática e incluso utilizar referencias cruzadas a ellas:

% Ejemplo: Ec. numerada.
% ============
\begin{equation} \label{eq:pitagoras}
	c^{2}=a^{2}+b^{2}
\end{equation}

No hay que preocuparse demasiado por los tipos de fundición empleados en las fórmulas pues \LaTeX{} hace por nosotros <<casi>> todo el trabajo.\footnote{Los matemáticos son muy exquisitos y no se conforman con cualquier cosa, pero nosotros debemos ser mucho menos pretenciosos si queremos resultados rápidos.}

Los ejemplos que aquí se muestran son muy sencillos pero \LaTeX{} proporciona entornos específicos más potentes. Para mostrar algo <<más sofisticado>> añado dos ejemplos más. La ec.~\ref{eq:integral} que es un poquito más compleja y la ec.~\ref{eq:cuadro} que está recuadrada.

% Ejemplo:
% ============
\begin{equation}\label{eq:integral}
	I = \! \int_{-\infty}^\infty f(x)\,dx
\end{equation}



% Ejemplo: Ec. con recuadro (numeración exterior).
% ============
{\fboxsep 8pt \fboxrule 0.5pt 
%\fboxsep ajusta la separación entre la caja y el elemento recuadrado
%\fboxrule ajusta el espesor de la línea del recuadro
\begin{equation}\label{eq:cuadro}
\fbox{$\displaystyle 
R = \frac{L}{2} \cdot \frac{{\left( {v_d  + v_i } \right)}}{{\left( {v_d  - v_i } \right)}}
$}
\end{equation}
}

Algunos otros cuadros en ecuaciones son p.~ej.\ $x + y = \fbox{$\Omega$}$ o incluso el que se muestra a continuación (ec.~\ref{eq:cuadrogrande}) y que abarca todo el ancho de la línea:\footnote{Adaptado del manual \emph{Documentation for fancybox.sty:
Box tips and tricks for \LaTeX{}} de Timothy Van Zandt (2010).}

% Ejemplo: Ec. con recuadro (numeración interior).
% ============
\newlength{\milong}
\[
	\setlength{\fboxsep}{15pt}
	\setlength{\milong}{\linewidth}
	\addtolength{\milong}{-2\fboxsep}
	\addtolength{\milong}{-2\fboxrule}
	\fbox{%
		\parbox{\milong}{
		\setlength{\abovedisplayskip}{0pt}
		\setlength{\belowdisplayskip}{0pt}
		\begin{equation}\label{eq:cuadrogrande}
		\sqrt[n]{1+x+x^2+x^3+\ldots}
		\end{equation}}}
\]


\subsection{Ecuaciones en varias líneas con entornos \texttt{eqnarray} y \texttt{align}}
A continuación se muestra un ejemplo de ecuación muy larga dividida en varias líneas:

% Ejemplo: Ec. en varias líneas con alineación y sin numeración.
% ============
\begin{eqnarray*}
  \lefteqn{\left(1+x\right)^n = } \\
  & & 1 + nx + \frac{n\left(n-1\right)}{2!}x^2 + \\
  & & \frac{n\left(n-1\right)\left(n-2\right)}{3!}x^3 + \\
  & & \frac{n\left(n-1\right)\left(n-2\right)\left(n-3\right)}{4!}x^4 + \\
  & & \ldots
\end{eqnarray*}
% Apreciar el asterisco en la ecuación anterior para evitar que todas las líneas de la ecuación aparezcan numeradas.

También se puede escribir varias ecuaciones en líneas sucesivas alineadas por algún elemento como se hace en el siguiente ejemplo de uso del entorno \texttt{align}:

% Ejemplo: Ecs. en varias líneas con alineación y manejo de la numeración.
% ============
\begin{align}
f(x) & = \cos x \\
f'(x) & = -\sin x \\
\int_{0}^{x} f(y)dy & = \sin x \nonumber
\end{align}

\noindent En este último ejemplo se observa también cómo es posible suprimir la numeración de una de las ecuaciones con el comando (\texttt{\textbackslash nonumber}).

Para terminar, un ejemplo más del control del espaciado horizontal empleando el entorno \texttt{array}:

% Ejemplo:
% ============
\[f(n) = \left\{ 
\begin{array}{l l}
  n/2 & \quad \mbox{si $n$ es par}\\
  -(n+1)/2 & \quad \mbox{si $n$ es impar}\\ \end{array} \right. \]









\section{Listados de programas}
En el caso de las titulaciones técnicas es muy habitual tener que explicar en el texto en preparación (p.~ej.\ un TFG/PFC o una Tesis, etc.) alguna porción de código fuente (p.~ej.\ algoritmo, función, etc.).\footnote{La inclusión de código en el texto debe estar justificada pues los listados exhaustivos deben dejarse para un CD que acompañe a la documentación, nunca deben incluirse <<tal cual>> en un documento.} Para facilitar la tarea de escribir código fuente \LaTeX{} proporciona el entorno \texttt{verbatim} para imprimir texto <<tal cual>> se escribe en el fichero de entrada. Sin embargo, este entorno es muy limitado y para ello se proporcionan paquetes que aumentan las posibilidades a la hora de tratar con el texto <<tal cual>> para que su aspecto final sea más profesional y flexible. Los dos paquetes que conviene mencionar aquí son: \texttt{listings} y \texttt{fancyvrb}.


\subsection{Listados de código con el paquete \texttt{listings}}
El paquete \texttt{listings} está pensado para tratar especialmente con código fuente. En este caso se reconoce el lenguaje\footnote{Se reconoce un número muy amplio de lenguajes.} en que está escrito el código y ésto condiciona el modo de impresión del código (véase el listado~\ref{lst:java}). Este paquete tiene mucha flexibilidad y permite tratar con los listados de código como si fueran objetos deslizantes, de modo similar a como se tratan las figuras y las tablas. Una consecuencia de esto es que los listados no quedan divididos entre páginas. Por supuesto se admiten muchas de las opciones disponibles para los objetos deslizantes en \LaTeX{} como referencias cruzadas índice de elementos, etc. El número de opciones es tan numeroso que su comentario excede el propósito del curso por lo que se recomienda la consulta de la documentación del paquete a aquellos que estén más interesados.


% Ejemplo:
% ============
\begin{lstlisting}[style=Java,float=ht,caption={[Un ejemplo de código]Ejemplo de código fuente en lenguaje Java},label=lst:java]
// @author www.javadb.com
public class Main {    
  // Este método convierte un String a
  // un vector de bytes

   public void convertStringToByteArray() {
        
     String stringToConvert = "This String is 15";      
     byte[] theByteArray = stringToConvert.getBytes();        
     System.out.println(theByteArray.length);        
   }
    
   // argumentos de línea de comandos 
   public static void main(String[] args) {
     new Main().convertStringToByteArray();
   }
}
\end{lstlisting}

Un aspecto a tener presente cuando se trabaja con el paquete \texttt{listings} es que no funciona completamente bien cuando se emplea codificación unicode (UTF8) y el texto del entorno \texttt{lstlisting} tiene caracteres especiales (acentos, interrogación, etc.). En estos casos se pueden adoptar dos opciones: 

\begin{enumerate}
	\item Trabajar con codificación UTF8 (opción \texttt{utf8} para el paquete \texttt{inputenx}) e incluir en la definición \texttt{lstset} la opción \texttt{extendedchars=\textbackslash true} y \texttt{texcl=true}. De este modo se reconocen los caracteres extendidos pero aún así se presentan errores con algunos caracteres.
	\item Trabajar con codificación ANSI (opción \texttt{ansinew} o \texttt{latin1}) y emplear los caracteres que se desee.
\end{enumerate}

En los siguientes ejemplos veremos varios listados generados empleando el paquete \texttt{listings}. El listado~\ref{lst:java} muestra el resultado cuando se emplean unas opciones asociadas a un estilo creado específicamente para código fuente Java. También ilustra cómo es manejado el código como un elemento deslizante \emph{(float)} y cómo se puede incluir incluso referencias cruzadas.

En los ejemplos siguientes se muestra el código fuente de un pequeño programa C para el que se emplea un ajuste diferente de los atributos del entorno \texttt{lstlisting}.

\begin{lstlisting}[style=C,float=ht,caption={Ejemplo de código C},label=lst:codC]
// Este código se ha incluido tal cual está 
// en el fichero \LaTeX{}
#include <stdio.h>
int main(int argc, char* argv[]) {
  puts("¡Hola mundo!");
}
\end{lstlisting}

\noindent Por el contrario este otro se ha generado incluyendo el código desde un fichero externo \texttt{``HolaMundo.c''}.

%\lstinputlisting[style=C,firstline=3,float=ht]{HolaMundo.c} % La opción firstline indica la primera línea incluida
\lstinputlisting[style=C,float=ht,caption={Ejemplo de código C desde un fichero externo},label=lst:codCfile]{./code/HolaMundo.c}


También es posible configurar un estilo específico para indicar comandos de consola del computador, como en el siguiente ejemplo dónde se señala cómo compilar un fichero usando \texttt{gcc} (detener pulsando \tecla{Ctrl+d}): %Aquí se muestra cómo incluir en un manual la pulsación de teclas

\begin{lstlisting}[style=consola, numbers=none]
$ gcc -o Hola HolaMundo.c
\end{lstlisting}





\subsection{Listados con el paquete \texttt{fancyvrb}}
El paquete \texttt{fancyvrb} suministra un nuevo entorno \texttt{Verbatim} que añade gran flexibilidad al entorno \texttt{verbatim}. Todos los detalles y posibilidades de este nuevo entorno exceden el propósito de este curso básico por lo que para profundizar se recomienda la consulta de la documentación del paquete. En el ejemplo más abajo se muestra algunas de las posibilidades del paquete mencionado. Se debe tener en cuenta que con este entorno, a diferencia de \texttt{lstlisting}, los listados pueden ser divididos entre páginas y que si se desea evitar dicha división habrá que intentarlo mediante ensayo y error, o bien creando un objeto nuevo deslizante para estos nuevos elementos y así ser tratados como las imágenes o las tablas.

% Ejemplo:
% ============
%Inclusión del entorno propio configurado para Verbatim
\begin{listado}
/* @author www.javadb.com */
public class Main {    
/* Este método convierte un String a
   un vector de bytes */

   public void convertStringToByteArray() {
        
     String stringToConvert = "This String is 15";      
     byte[] theByteArray = stringToConvert.getBytes();        
     System.out.println(theByteArray.length);        
   }
    
   /* argumentos de línea de comandos */
   public static void main(String[] args) {#label[vrb:main]
     new Main().convertStringToByteArray();
   }
}
\end{listado}

\noindent Este tipo de entorno permite incluso la inclusión de referencias cruzadas a las líneas de código (p.~ej.\ en línea~\ref{vrb:main} aparece el constructor \texttt{main}).




\subsection{Algoritmos con el paquete \texttt{algorithm2e}}
Como ya se ha comentado en los textos científicos relacionados con las TIC\footnote{Por supuesto en un TFG o tesis de una Escuela de Informática.} (Tecnologías de la Información y Comunicaciones) suelen aparecer porciones de código en los que se explica alguna función o característica relevante del trabajo que se expone. Muchas veces lo que se quiere ilustrar es un algoritmo o método en que se ha resuelto un problema abstrayéndose del lenguaje de programación concreto en que se realiza la implementación. El paquete \texttt{algorithm2e} proporciona un entorno \texttt{algorithm} para la impresión apropiada de algoritmos tratándolos como objetos flotantes y con muchas flexibilidad de personalización. En el algoritmo \ref{alg:como} se muestra cómo puede emplearse dicho paquete. En este curso no se explican las posibilidades del paquete más en profundidad ya que excede el propósito del curso. A todos los interesados se les remite a la documentación del mismo.


% Ejemplo:
% ============
\IncMargin{1em}
\begin{algorithm}
\SetKwInOut{Input}{Datos}\SetKwInOut{Output}{Resultado}
\LinesNumbered
\SetAlgoLined
\Input{este texto}
\Output{como escribir algoritmos con \LaTeX2e}
inicialización\;
\While{no es el fin del documento}{
leer actual\;
\eIf{comprendido}{
ir a la siguiente sección\;
la sección actual es esta\;
}{
ir al principio de la sección actual\;
}
}
\caption{Cómo escribir algoritmos}\label{alg:como}
\end{algorithm}

