\chapter{Inclusión de figuras en documentos}
\label{ch:figuras}



\section{Primeros pasos}
La inclusión de figuras y archivos de imagen en un documento elaborado con \LaTeX{} es muy sencilla y versátil (en la Fig.~\ref{fig:plazaCR} se muestra una fotografía en formato \texttt{.jpg} en color).\footnote{El título de la imagen también muestra cómo debe darse créditos al autor de la imagen si ésta no es de libre uso y tenemos permiso para usarla.} Hay que tener presente que el tipo de ficheros permitidos depende de si empleamos \texttt{latex} o \texttt{pdflatex},\footnote{En todo este curso asumimos que trabajaremos con \texttt{pdfltex} pues es más cómodo y siempre es posible utilizar la herramienta \texttt{epstopdf} para realizar la conversión de formatos.} ya que el primero sólo permite la inclusión de ficheros gráficos \texttt{.eps} mientras el segundo admite \texttt{.pdf}, \texttt{.png} y \texttt{.jpg}. Cuando la figura original presente un formato diferentes a los soportados será preciso emplear algún programa para realizar la conversión apropiada de formatos.



% Ejemplo:
% ============
\begin{figure}[htb] % Véase los tags de ubicación (h)ere, (t)op, (b)ottom
	\centering % Fig. centrada en la página
	\includegraphics[height=6cm]{plazaCR} % Alto de la figura en la pág.
	\caption[Ejemplo de foto en formato jpg]{La Plaza Mayor (cortesía de J.~Salido)} % Título y el título para la lista de figuras
	\label{fig:plazaCR} % Etiqueta para las refs. cruzadas
\end{figure}


La inclusión de figuras requiere al menos el empleo del paquete \texttt{graphicx} con el que ya se pueden obtener resultados muy aceptables, aunque existen otros paquetes más especializados que facilitan hacer cosas más exóticas, como el paquete \texttt{subfigure} para presentar figuras compuestas de varias subfiguras (ver Figs.~\ref{fig:clock} y \ref{fig:lion}).

\LaTeX{} puede procesar las figuras como \emph{objetos deslizantes o <<flotantes>>}\footnote{Estos elementos se denominan \textit{float} y por ello a veces en español utilizamos como traducción <<flotante>>.} (o sin ubicación prefijada). De este modo \LaTeX{} emplea algoritmos para encontrar la mejor ubicación de todos los objetos flotantes que contenga el texto. El usuario siempre tiene a su disposición opciones para sugerir la ubicación deseada dejando a \LaTeX{} la reubicación del texto y párrafos en la página. Con todo, en ocasiones el usuario debe hacer algunos ajustes para conseguir la ubicación deseada, aunque sigue siendo un procedimiento mucho más cómodo que en algunos de los procesadores de texto WYSIWYG populares. Siempre que existan muchas figuras en el texto el ajuste del resultado final va a ser más complejo y requerirá mayor intervención humana.


Otra de las ventajas de \LaTeX{} es que las imágenes no están incrustadas en nuestro fichero fuente sino que son ficheros aparte incluidos durante la compilación. De este modo la modificación de la figuras no requiere la modificación del fichero fuente sino únicamente una nueva compilación.

% Ejemplo:
% ============
%Aquí se muestra cómo se emplean subfiguras. Se pueden añadir cuantas subfiguras se desee, de modo que si no caben a la par LaTeX las ubicará en varias filas. Esta colocación en varias filas se puede provocar añadiendo \\ (saltos de línea). Las subfiguras también se pueen emplear en las refs. cruzadas. El título de la figura también admite uno alternativo para la lista de figuras.
\begin{figure}[htb]
	\centering
	\subfigure[Imagen jpg en color]{
		\includegraphics[width=5.5cm]{clockCR}
		\label{fig:clockCR}
	}
	\subfigure[Imagen jpg en niveles de gris]{
		\includegraphics[width=5.5cm]{clockCRbw}
		\label{fig:clockCRbw}
	}
	\caption[Comparación jpg color y niveles de gris]{El reloj de la Plaza Mayor (cortesía de J.~Salido)}
	\label{fig:clock}
\end{figure}




\section{Formatos gráficos}
A la hora de incluir figuras se debe decidir qué formato es el más apropiado entre los disponibles, siguiendo las siguientes reglas:
\begin{itemize}
	\item Siempre que sea posible es preferible el formato \texttt{.pfd} ya que es vectorial (escalable), aunque por supuesto si el fichero contiene alguna imagen de mapa de bits, esta no será escalable (véase Fig.~\ref{fig:4004arch}).
	\item Las fotografías se incluirán en formato \texttt{.jpg} (véase Fig.~\ref{fig:plazaCR})
	\item Las capturas de pantalla o gráficos de gran contraste se deben incluir en formato \texttt{.png} (véase Fig.~\ref{fig:inkscape}). 
	\item Siempre que se tenga un fichero de imagen (mapa de bits) con un fondo blanco u otro color plano, debería intentarse transformar en una imagen con fondo transparente convirtiéndola al formato \texttt{.png} (véase ejemplo en la Fig.~\ref{fig:lion}).
\end{itemize}

% Ejemplo:
% ============
\begin{figure}[htb]
  \centering
	\subfigure[Imagen como jpg]{
		\includegraphics[width=6cm]{lionL.jpg}
		\label{fig:lionLjpg}
	}
	\subfigure[Imagen como png con fondo transparente]{
		\includegraphics[width=6cm]{lionL.png}
		\label{fig:lionpng}
	}
	\caption[Comparación jpg y png con transparencia]{Comparativa de formatos bitmap (cortesía de D.~Wright)}
	\label{fig:lion}
\end{figure}

Si se desea se puede usar imágenes en color. Esto es muy conveniente para documentos electrónicos que se va a visualizar en la pantalla de un computador. Sin embargo, para documentos que serán impresos hay que tener presente algunos aspectos del color. 

Sólo se debería emplear gráficos en color si es imprescindible para la comprensión del documento final. En el caso de Proyectos Fin de Carrera y Tesis podemos emplear color pues el número de ejemplares a imprimir es pequeño (habitualmente inferior a 10) y generalmente se imprimirán con impresora láser, aunque el coste será muy superior al de una impresión en niveles de gris. Cuando se trate de artículos científicos o libros para una editorial, el color debería ser descartado pues en el 99,99\% de los casos las editoriales imprimirán el documento en niveles de gris y el color será irrelevante. Si a priori sabemos que el documento será impreso en niveles de gris deberíamos preparar las figuras para que sean legibles cuando se impriman de este modo, no para que tengan unos colores vistosos.

En la Fig.~\ref{fig:clock} se muestra un ejemplo de empleo de subfiguras. En dicho ejemplo se muestra dos versiones de la misma imagen, la subfigura~\ref{fig:clockCR} es una versión en color (tal cual se tomó la fotografía original) y la sufigura~\ref{fig:clockCRbw} muestra la misma imagen transformada en niveles de gris.

La Fig.~\ref{fig:lion} muestra un ejemplo de subfiguras en las que se muestra la misma figura en formato \texttt{.jpg} y en formato \texttt{.png} en la que el fondo blanco se ha convertido en transparente. Para apreciar la diferencia debe imprimirse el documento para observar como la figura con el fondo transparente en más nítida.

% Ejemplo:
% ============
\begin{figure}[ht]
	\centering
	\includegraphics[width=0.4\textwidth]{inkscape} 
	\caption[Ejemplo de captura en png]{Captura de pantalla de \textsf{Inkscape}}
	\label{fig:inkscape}
\end{figure}

Las capturas de pantalla de programas deberían incluirse como ficheros en formato \texttt{.png}. En la Fig.~\ref{fig:inkscape} se muestra una captura del programa de libre distribución \textsf{InkScape}. Dicho programa es apropiado para la elaboración de gráficos vectoriales. 

Siempre que sea posible emplear formato \texttt{.pdf} es preferible ya que se trata de un formato vectorial con posibilidad de escalado sin pérdida de resolución. En la Fig.~\ref{fig:4004arch} se muestra el resultado de la inclusión de un gráfico en formato PDF. 

% Ejemplo:
% ============
\begin{figure}[htb]
	\centering 
	\includegraphics[width=0.5\textwidth]{4004arch} 
	\caption[Ejemplo de gráfico vectorial PDF]{Figura vectorial de arquitectura Intel 4004 (Wikimedia Commons)}
	\label{fig:4004arch}
\end{figure}


\subsection{Ventajas del formato PDF}
Una de las dificultades de trabajar con ficheros PDF es que se trata de un formato difícil de editar de modo que habitualmente se genera a partir de otros formatos. Por ejemplo, el gráfico de la Fig.~\ref{fig:4004arch}, cuyo formato original es \texttt{.svg}, ha sido realizado con el programa \textsf{InkScape} y convertido al formato PDF mediante la herramienta de exportación (a través de \textsf{Cairo}) incluida en el programa. En los programas que no incorporan dicha herramienta el fichero PDF se puede generar imprimiendo a un archivo con un driver de impresión PDF (si no se dispone del driver de Adobe se recomienda el uso de \textsf{PDFCreator}). 

% Ejemplo:
% ============
\begin{figure}[htb]
	\centering
	\subfigure[Gráfico vectorial PDF]{
		\includegraphics[width=3.5cm]{escudoInfBW.pdf}
		\label{fig:escudoPDF}
	} 
	\subfigure[Gráfico png]{
		\includegraphics[width=3.8cm]{escudoInfBW.png}
		\label{fig:escudoPNG}
	}
	\caption[Comparación PDF y png]{Comparando distintos formatos para el escudo de Informática (cortesía de CRySoL)}
	\label{fig:escudo}
\end{figure}


En la Fig.~\ref{fig:escudo} se pueden comparar los resultados obtenidos cuando la figura se inserta en formato vectorial (escalable) y cuando se hace como mapa de bits (no escalable). En este ejemplo se muestran dos versiones para el escudo de la Ingeniería Informática.\footnote{Fuente: CRySoL. Para un discusión muy interesante sobre la historia de dicho emblema, su significado y su elaboración se puede consultar la URL: \url{http://crysol.org/es/node/1100}.}

Cuando se trabaja con figuras hay que tener mucho cuidado con emplear imágenes de Internet sin tener la seguridad de los términos de uso de las mismas. Con mucha frecuencia, de forma inadvertida, se violan los derechos de uso incluso cometiendo un delito. Por este motivo recomiendo recurrir a librerías de dominio público que permiten el uso de las imágenes y \emph{clip arts} sin restricciones, como por ejemplo Open ClipArt,\footnote{\url{http://openclipart.org/}} la página de galerías en el sitio de Inkscape\footnote{\url{http://wiki.inkscape.org/wiki/index.php/Galleries}} y Wikimedia Commons.\footnote{\url{http://commons.wikimedia.org/}}

\newpage
\subsection{Gráficas matemáticas}
En muchas ocasiones es preciso recurrir a una gráfica matemática para reflejar o apoyar una idea. Existen numerosos programas que ayudan a realizar estas gráficas, en cualquiera de los casos siempre se empleará un formato vectorial (PDF) para incluir la gráfica en nuestro documento. Nunca debe hacerse mediante una captura de pantalla pues en este caso estaremos perdiendo mucha información al tratar como un mapa de bits algo que es un objeto matemático y por tanto independiente de su escala de representación.

\subsubsection{Gráficas generadas con Excel}
Uno de los programas más universales para la creación de gráficas matemáticas son las hojas de cálculo como Excel. La Fig.~\ref{fig:excel} muestra un ejemplo de figura vectorial generada con ayuda de dicho programa.

% Ejemplo:
% ============
\begin{figure}[htb]
	\centering
	\includegraphics[width=0.5\textwidth]{EjFigsExcel} 
	\caption[Gráfico de Excel]{Figura vectorial generada con Excel}
	\label{fig:excel}
\end{figure}




\newpage
\subsubsection{Gráficas generadas con Matlab}
La Fig.~\ref{fig:matlabGrafs} muestra un ejemplo de figura vectorial generada con ayuda del programa Matlab. En este caso se observa cómo una única figura contiene distintos gráficos, siendo este un método alternativo al empleo de subfiguras.

% Ejemplo:
% ============
\begin{figure}[htb]
	\centering
	\includegraphics[width=0.8\textwidth]{matlabGrafs} 
	\caption[Gráfico de Matlab]{Figura vectorial generada con Matlab}
	\label{fig:matlabGrafs}
\end{figure}



\newpage
\subsubsection{Gráficas generadas con Matplotlib (Python)}
La Fig.~\ref{fig:python} muestra un ejemplo de figura vectorial (PDF) generada con Python y la librería \textsf{Matplotlib}. En este caso se observa cómo una única figura contiene distintos gráficos, siendo este un método alternativo al empleo de subfiguras.

% Ejemplo:
% ============
\begin{figure}[htb]
	\centering
	\includegraphics[width=0.8\linewidth]{figure_2}
	\caption[Figura hecha desde Python]{Figura vectorial generada con Matplotlib (Python)}
	\label{fig:python}
\end{figure}


\newpage

\subsection{Inclusión de páginas individuales de un PDF multipágina}

La Fig.~\ref{fig:visio_mp} nos muestra el procedimiento para usar los ficheros PDF multipágina para incluir el gráfico de una página concreta.

% Ejemplo:
% ============
\begin{figure}[htb]
	\centering
	\includegraphics[page=2,width=0.8\textwidth]{visio_mp} 
	\caption[Gráfico de Visio multipágina]{Figura vectorial generada con Microsoft Visio en un fichero multipágina}
	\label{fig:visio_mp}
\end{figure}


\newpage

\subsection{Imágenes a las que se añaden gráficos}
En ocasiones es necesario recurrir a capturas de pantalla sobre las que es preciso realizar alguna anotación gráfica, por ejemplo añadiendo flechas y bloques de texto. Este es un caso que puede aparecer cuando se explica el funcionamiento de un programa informático (p.~ej.\ un manual). La captura siempre se debe realizar al mayor tamaño posible sobre la pantalla y se debe salvar en formato \texttt{.png}. Generalmente las herramientas de captura permiten la edición de la captura añadiéndole elementos gráficos e incluso texto. En este caso los elementos añadidos 
forman parte del fichero \texttt{.png} y por tanto son definidos como mapa de bits. Si se desea mantener las características escalables de los elementos gráficos, éstos deben ser añadidos mediante algún programa de edición vectorial (p.~ej.\ 
\textsf{Inkscape}, \textsf{Dia}, \textsf{Visio}, etc.) y salvar el fichero resultante en formato \texttt{.pdf}. Las figs.~\ref{fig:texmk02} y \ref{fig:texmk03} muestran las diferencias en los dos procesos mencionados.

% Ejemplo:
% ============
\begin{figure}[htb]
	\centering
	\includegraphics[width=0.5\textwidth]{texmk02} 
	\caption[Captura con gráfico en \texttt{png}]{Captura de pantalla con añadido gráfico en formato \texttt{png}}
	\label{fig:texmk02}
\end{figure}

% Ejemplo:
% ============
\begin{figure}[htb]
	\centering
	\includegraphics[width=0.5\textwidth]{texmk03} 
	\caption[Captura con gráfico en \texttt{pdf}]{Captura de pantalla con añadido gráfico en formato \texttt{pdf}}
	\label{fig:texmk03}
\end{figure}



\newpage
\section{Gráficos directos con instrucciones \TeX}
Además de los aspectos comentados aquí sobre la inclusión de imágenes y gráficos, \LaTeX{} dispone de una infinidad de recursos que pueden por sí mismo ser objeto de un curso. La figura~\ref{fig:carrito} se muestra la capacidad de realización de gráficos mediante instrucciones directas de \TeX{}. Un paquete muy poderoso en la generación de gráficos dentro de entornos \LaTeX{} es \texttt{tikz} (ver Fig.~\ref{fig:tikz}). La cantidad y variedad de gráficos que puede realizar es muy numerosa\footnote{\url{http://www.texample.net/tikz/examples/}} aunque su uso requiere un conocimiento muy profundo y no es recomendable para principiantes.

% Ejemplo:
% ============
% Gráfico realizado con instrucciones directas de TeX, en este caso además el gráfico se ha incluido en un entorno figure para que sea tratado como un objeto flotante.
\begin{figure}[htb]
\centering
\newcounter{cms}
\setlength{\unitlength}{1mm}
\begin{picture}(50,39)
	\put(15,20){\circle{6}} \put(30,20){\circle{6}}
	\put(15,20){\circle*{2}}\put(30,20){\circle*{2}}
	\put(10,24){\framebox(25,8){
	$I = \! \int_{-\infty}^\infty f(x)\,dx$
}}
	\put(10,32){\vector(-2,1){10}}
	\put(0,7){\makebox(0,0)[bl]{cm}}
	\multiput(10,7)(10,0){5}{%
	\addtocounter{cms}{1}%
	\makebox(0,0)[b]{\thecms}}
	\multiput(1,0)(1,0){49}{\line(0,1){2.5}}
	\multiput(5,0)(10,0){5}{\line(0,1){3.5}}
	\thicklines
	\multiput(0,0)(10,0){6}{\line(0,1){5}}
	\put(0,0){\line(1,0){50}}
\end{picture}
\caption[Ejemplo de gráfico \LaTeX{}]{Figura realizada directamente con instrucciones \TeX{}}\label{fig:carrito}
\end{figure}


% Ejemplo:
% ============
% Gráfico realizado con paquete tikZ, en este caso además el gráfico se ha incluido en un entorno figure para que sea tratado como un objeto flotante.
\begin{figure}[htb]
\centering
{\shorthandoff{>}
\begin{tikzpicture}[>=stealth]
\draw [->] (-1.5,0) -- (1.5,0);
\draw [->] (0,-1.5) -- (0,1.5);
\shadedraw (0.5,0.5) circle (0.5cm);
%Relleno
\filldraw[fill=red,even odd rule]
(-1,-1) rectangle (0,0)
(-0.5,-0.5) circle (0.4cm);
\draw[->] (-0.9,-0.2) -- +(0,1)
[above] node{Relleno};
\end{tikzpicture}
}
\caption[Ejemplo de gráfico con Ti\textit{K}Z]{Figura realizada con paquete \texttt{tikz}}\label{fig:tikz}
\end{figure}



\newpage
\section{Gráficos de grandes dimensiones}
Cuando se presenta la necesidad de incluir un gráfico demasiado grande para el tamaño de la página una opción muy apropiada es la impresión del gráfico en modo apaisado en una página aparte. Este efecto se consigue con el entorno \texttt{sidewaysfigure} proporcionado por el paquete \texttt{rotating}. La Fig.~\ref{fig:sideways} muestra un ejemplo del entorno citado con un gráfico PDF.

% Ejemplo:
% ============
\begin{sidewaysfigure}[htb]
	\centering
	\includegraphics[width=19cm]{network} 
	\caption[Gráfico de apaisado Visio]{Figura vectorial con impresión apaisada}
	\label{fig:sideways}
\end{sidewaysfigure}
