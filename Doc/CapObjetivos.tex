\chapter{Objetivos y herramientas de desarrollo}
\label{ch:Objetivos}

En este capítulo se va a exponer el objetivo principal de este TFG y se detallarán los subobjetivos propuestos para conseguir el objetivo principal. Por último se detallará el entorno de trabajo, las herramientas y los medios hardware y software empleados.


\section{Objetivos}

\subsection{Objetivo principal}
Este TFG tiene como objetivo\index{objetivo} principal el desarrollo de una aplicación de geolocalización basada en POIs que sea capaz de ofrecer al usuario resultados de búsqueda basados en sus preferencias y necesidades concretas, minimizando la interacción cuanto sea posible, mejorando así la experiencia de usuario. Ésto se consigue almacenando las preferencias del usuario y ofreciendo la posibilidad de agregar POIs mediante NFC.


\subsection{Subobjetivos}

Para lograr el objetivo principal de este TFG será necesario completar unos subobjetivos:

\subsubsection{Estudiar las tecnologías y el estado del arte}
\label{sec:Estudio}
Antes de comenzar el desarrollo de cualquier proyecto es necesario estudiar las tecnologías que va a integrar la aplicación antes de realizar el desarrollo de la misma. Estas tecnologías serán la geolocalización y el NFC. 
Además, trabajar con una API ya existente y predefinida ayuda al desarrollador a recoger datos que se presuponen útiles para servir al usuario, pero a su vez limita las operaciones que se pueden realizar. Será necesario estudiar las posibilidades de las API, ya que ésto influirá en el diseño de la estructura de la aplicación.
Por último, habrá que analizar las aplicaciones ya existentes en el mercado y los trabajos de investigación con objetivos similares a este TFG.

\subsubsection{Definir las búsquedas de POIs basadas en preferencias}
\label{sec:Busquedas}
Para conocer cuáles son las preferencias de usuario que éste más podría demandar en este tipo de aplicaciones será necesario estudiar qué buscan los usuarios, cómo realizan las búsquedas y si sus necesidades son atendidas. La API de Google Places Web Service recoge datos de igual forma que los muestra si se busca un lugar en su buscador, por lo que la información que proporciona es la que usualmente busca el usuario.

Entre toda la información obtenida mediante el API anterior, se recogerá la más valiosa para los objetivos de este TFG. Así, se definirán las búsquedas para que el usuario pueda realizarlas según sus preferencias. Las preferencias generales estarán recogidas en su perfil de usuario, donde éste podrá decidir entre filtros tales como que los lugares mostrados deban estar abiertos, seleccionar un rango de valoraciones de otros usuarios o aplicar filtros personalizados según categorías.


\subsubsection{Diseñar y desarrollar la aplicación móvil}
El desarrollo de la aplicación Android se realizará en base a los conocimiento adquiridos en los subobjetivos ~\ref{sec:Estudio} y ~\ref{sec:Busquedas}. Esta aplicación permitirá a los usuarios:
\begin{itemize}
	\item Crear su perfil de usuario con preferencias.
	\item Realizar búsquedas de POIs en base a sus preferencias y mediante técnicas de geolocalización.
	\item Visualizar y consultar la información de los lugares resultantes de las búsquedas.
	\item Gestionar POIs mediante su creación, modificación y borrado.
\end{itemize}

También será necesario crear un modelo de la aplicación que incluya una base de datos con los usuarios, sus preferencias y sus POIs. Este modelo será accesible mediante servicios web, que serán consultados por la aplicación.


\subsubsection{Permitir la creación de nuevos Puntos de Interés asociados a etiquetas NFC}
Será necesario definir cómo se va a realizar el proceso de crear nuevos POIs mediante NFC, aprovechando la compatibilidad con los dispositivos. Si un dispositivo no es compatible, deberá poder realizar la misma función de forma manual.

\subsubsection{Evaluar y validar la aplicación}
Las preferencias de usuario hacen referencia a la información necesaria y relevante para un usuario en un momento y contexto determinado, por lo que será necesario realizar evaluaciones tanto desde el punto de vista funcional, como del usuario.


\begin{table}[H]
   \centering
   \caption{Subobjetivos del TFG}
	 \label{tab:subobjetivos}
   \rowcolors{1}{white}{sombra}
   \begin{tabular}{p{.15\textwidth} p{.70\textwidth}}
   		\textbf{Subobjetivo} & \textbf{Descripción} \\
		\hline
          Subobjetivo 1 & Estudiar las tecnologías y el estado del arte\\
          Subobjetivo 2 & Definir las búsquedas de POIs basadas en preferencias\\
          Subobjetivo 3 & Diseñar y desarrollar la aplicación móvil\\
          Subobjetivo 4 & Permitir la creación de nuevos puntos de interés asociados a etiquetas NFC\\
          Subobjetivo 5 & Evaluar y validar la aplicación\\
		\hline
   \end{tabular}
\end{table}


\section{Herramientas y medios de trabajo}
En esta sección se procede a especificar los medios hardware y software que se han utilizado durante el desarrollo del Trabajo Fin de Grado. Una vista general de los medios de trabajo se puede ver en las figuras ~\ref{fig:Hardware} y ~\ref{fig:Software} .

\subsection{Medios hardware}
\index{hardware}
La aplicación final debe poder estar soportada por teléfonos que puedan hacer el uso de NFC o que no dispongan de él, pues el usuario siempre puede introducir sus Puntos de Interés manualmente. Por tanto, han sido necesarios tanto dispositivos o medios que den soporte como que no cuenten con NFC:

\subsubsection*{Asus K53SV}
Ordenador portátil necesario para desarrollar el proyecto. Tiene un procesador Intel Core i7-2670QM 2.20 GHz, 6 GB de memoria RAM, 500 GB de capacidad de disco duro, una tarjeta gráfica NVIDIA GeForce GT 540M 2GB y cuenta con los Sistemas Operativos Windows 8.1 y Ubuntu 14.04.3 LTS. Contiene todo el software mencionado en la sección ~\ref{sec:software}.

\begin{figure}[htb] % Véase los tags de ubicación (h)ere, (t)op, (b)ottom
	\centering % Fig. centrada en la página
	\includegraphics[width=\textwidth]{hardware} % Alto de la figura en la pág.
	\caption[Medios hardware utilizados]{Medios hardware utilizados} % Título y el título para la lista de figuras
	\label{fig:Hardware} % Etiqueta para las refs. cruzadas
\end{figure}

\subsubsection*{Smarthphone Sony Xperia M4 Aqua Dual}
Este smartphone ha sido el dispositivo principal para la realización de las pruebas. Con Android 5.0. Su procesador es un Octa Core de 64 bits 1,5 GHz Qualcomm Snapdragon. Tiene 2 GB de memoria RAM y 8 GB de memoria interna. En cuanto a su conectividad, cuenta con tecnologías Wi-Fi, Bluetooth, 4G, GPS y NFC.

\subsubsection*{BQ Aquaris E4.5}
Smartphone sin tecnología NFC para la realización de las pruebas en dispositivos que no integren esta tecnología de interacción por contacto. Su procesador es un Quad Core Cortex A7 1.3 GHz MediaTek. Cuenta con 1 GB de memoria RAM y 8 GB de memoria interna, además de conectividad Wi-Fi, Bluetooth, 3G y GPS. Tiene el SO Android 5.0.

\subsubsection*{Etiquetas NFC}
Para almacenar la información sobre los POIs y transmitir esa información a los smartphones. Cuentan con chip de tipo NTAG203, que son compatibles con todos los modelos que integren la tecnología NFC y tienen una memoria interna útil de 144 bytes.


\subsection{Lenguajes}
\index{lenguaje}
\subsubsection*{Java}
\index{lenguaje!Java}
La aplicación resultante de este proyecto se ejecuta en un entorno con Sistema Operativo Android\footnote{\url{https://developer.android.com/intl/es/develop/index.html}}. Las aplicaciones para este SO se pueden programar en diferentes lenguajes según el tipo de herramienta de desarrollo. En este caso se ha programado en lenguaje Java, ya que es el que se utiliza en la plataforma de desarrollo elegida ~\ref{subsec:plataforma}. Los scripts Java de la aplicación serán el código fuente de la misma, clases auxiliares, clases de dominio, de persistencia, etc.

\subsubsection*{PHP}
\index{lenguaje!PHP}
Los servicios web son componentes software que facilitan la interoperabilidad entre varios sistemas independientes entre sí, con lenguajes o plataformas diferentes. Un servicio web actúa de intermediario para enviar y recibir datos entre el cliente, en este caso la aplicación Android, y la base de datos MySQL ~\ref{subsec:bbdd} alojada remotamente.

Los scripts que corresponden a los servicios web estarán implementados en lenguaje PHP. La aplicación realizará llamadas a los scripts, que serán interpretados por el servidor y ejecutados antes de servir la respuesta al cliente, que recogerá los resultados producidos. En este TFG, será necesario acceder a los datos de los usuarios y los POIs mediante este tipo de scripts. La figura ~\ref{fig:PHP} refleja el tratamiento de la petición por parte del servidor, y el envío de la respuesta al cliente.


\begin{figure}[htb] % Véase los tags de ubicación (h)ere, (t)op, (b)ottom
	\centering % Fig. centrada en la página
	\includegraphics[width=\textwidth]{php} % Alto de la figura en la pág.
	\caption[Funcionamiento de PHP]{Funcionamiento de PHP} % Título y el título para la lista de figuras
	\label{fig:PHP} % Etiqueta para las refs. cruzadas
\end{figure}
 
\subsection{Medios software}
\label{sec:software}
\index{software}
Los medios software son entornos de desarrollo, herramientas de gestión y planificación, herramientas de creación y tratamiento de imágenes, y cualquier otra herramienta necesaria durante el desarrollo del proyecto. En este TFG se han utilizado los siguientes medios:

\subsection*{Plataforma de Desarrollo}
\label{subsec:plataforma}
Android Studio con ADT (Android Development Tools) \footnote{\url{http://developer.android.com/intl/es/sdk/index.html}}. Entorno de desarrollo integrado para la plataforma Android. Fue publicado de forma gratuita con Licencia Apache 2.0. Se puede utilizar en las plataformas Microsoft Windows, Mac OS X y GNU/Linux. Provee renderización en tiempo real, soporte para la optimización del código, traducción y estadísticas de uso. Además, ofrece plantillas de diseños y componentes y soporta la programación para Android Wear.

\subsection*{Base de Datos}
\label{subsec:bbdd}
\index{BBDD}
MySQL\footnote{\url{https://www.mysql.com/}} es un sistema para gestionar bases de datos relacionales. Considerada como la base de datos open source más popular del mundo\footnote{\url{http://www.oracle.com/us/products/mysql/overview/index.html}}, es conocida por ser usada para entornos de desarrollo web. Forma un gran equipo junto a phpMyAdmin\footnote{\url{https://www.phpmyadmin.net/}}, una herramienta en PHP que permite administrar MySQL a través de páginas web. 

Hostinger\footnote{\url{http://www.hostinger.es/}} proporciona hosting web gratuito con 2Gb de espacio y 100 Gb de tráfico e integra MySQL, phpMyAdmin y otras herramientas necesarias para la creación de webs. Permite almacenar información persistente para acceder a ella remotamente desde cualquier sitio a través de servicios web. 
	
\subsection*{Gestión y planificación}
Trello\footnote{\url{https://trello.com/}} es una herramienta de gestión de proyectos colaborativa que contiene listas de tareas, imágenes, archivos adjuntos, fechas de entrega, etiquetas de colores y comentarios. Tiene un gran potencial como apoyo a las metodologías de desarrollo ágil, ya que todos los miembros del equipo pueden consultarlo estén donde estén. Las clásicas notas utilizadas en Scrum para marcar las tareas pendientes, en progreso y hechas se convierten con Trello en elementos organizados en listas donde se pueden hacer comentarios o adjuntar archivos. La figura ~\ref{fig:Trello} muestra un ejemplo del uso de trello en pleno desarrollo.

\begin{figure}[H] % Véase los tags de ubicación (h)ere, (t)op, (b)ottom
	\centering % Fig. centrada en la página
	\includegraphics[width=\textwidth]{trello} % Alto de la figura en la pág.
	\caption[Captura de Trello en un instante del desarrollo del TFG]{Captura de Trello en un instante del desarrollo del TFG} % Título y el título para la lista de figuras
	\label{fig:Trello} % Etiqueta para las refs. cruzadas
\end{figure}

Además, se utilizará Toggl\footnote{\url{https://toggl.com/}}, que permitirá registrar el tiempo invertido en las diferentes tareas que componen el proyecto. Un ejemplo de cómo se ven las tareas en la aplicación se puede ver en la figura ~\ref{fig:Toggl}.
\label{sec:Toggl}
	 
\begin{figure}[H] % Véase los tags de ubicación (h)ere, (t)op, (b)ottom
	\centering % Fig. centrada en la página
	\includegraphics[width=\textwidth]{toggl} % Alto de la figura en la pág.
	\caption[Captura de Toggl con una lista de tareas]{Captura de Toggl con una lista de tareas} % Título y el título para la lista de figuras
	\label{fig:Toggl} % Etiqueta para las refs. cruzadas
\end{figure}
	 
	 
	 
\subsection*{Modelado y diseño de software}
\label{sec:Balsamiq}
Balsamiq Mockups\footnote{\url{https://balsamiq.com/products/mockups/}} es una aplicación para diseñar bocetos de lo que será la futura aplicación. Dispone de elementos prediseñados que se pueden añadir al lienzo, con soporte para varios tipos de aplicaciones.

Esta herramienta se ha utilizado para realizar los bocetos del prototipo de la aplicación, aunque no tienen por qué coincidir con el diseño de la aplicación final, ya que algunas partes del diseño de Android están impuestas de forma estándar.

Para el modelado de software se ha utilizado Visual Paradigm\footnote{\url{Community Edition https://www.visual-paradigm.com/download/community.jsp }} en su versión gratuita Community Edition, para uso no comercial. Ofrece al usuario la posibilidad de crear todo tipo de diagramas UML del ciclo de vida de un proyecto. Permite generar código a partir de esos diagramas y realizar ingeniería inversa. En este TFG se incluyen diagramas de casos de uso y de secuencia. 


\subsection*{Control de versiones}
Bitbucket\footnote{\url{https://bitbucket.org/}} es un servicio de alojamiento en la web para proyectos que utilizan el sistema de control de versiones Git. La utilización de este servicio facilita la gestión de los cambios realizados en el proyecto.

\subsection*{Documentación}
La documentación se ha realizado siguiendo las pautas de estructura y formato para TFG de la ESI. Se utilizará \LaTeX{}\footnote{\url{https://www.latex-project.org/}} como sistema de preparación de documentos, valiéndose del editor Texmaker\footnote{\url{http://www.xm1math.net/texmaker/}}. Además, para la gestión de la bibliografía se ha utilizado JabRef\footnote{\url{https://www.jabref.org/}}, que se integra con \LaTeX{}.

\subsection*{Otros}
Se ha utilizado software de edición, como Inkscape\footnote{\url{https://inkscape.org/es/}}, para la realización de imágenes de la aplicación y la documentación. 


\begin{figure}[H] % Véase los tags de ubicación (h)ere, (t)op, (b)ottom
	\centering % Fig. centrada en la página
	\includegraphics[width=\textwidth]{mediossoftware} % Alto de la figura en la pág.
	\caption[Medios software utilizados]{Medios software utilizados} % Título y el título para la lista de figuras
	\label{fig:Software} % Etiqueta para las refs. cruzadas
\end{figure}
